** What's New **
** The developer is in the process of developing the Jetpack Compose version of this library The old library had two long term behaviour bugs which will be fixed in the Compose version. After the Compose version is complete, these bug fixes will be migrated to this library. The two bugs are: If you put more touches than required the behaviour will glitch. Also the gesture and canvas are updating on seperate process; causing the gesture to rotate faster than it should relative to the rotation of the canvas. The causes of these bugs have been identified and have already been fixed in the Compose version **

## About HGDialV2 ##

**Overview:**

**This dial widget is an advanced rotation control that you can easily include into any Android project in the form of an AAR file. You can think of this Dial control as a gesture library that is just concerned with rotation; having all of the conceivable behaviours you could possibly want in a rotation control. Supports API 16+**

**You can download the demo app directly from the Google Play Store: https://play.google.com/store/apps/details?id=com.WarwickWestonWright.HGDialV2Demo**

**or view this great library in action here: https://www.youtube.com/watch?v=Cm1LJyG_50I**

**Features of the HGDialV2 Library: The Features include:**

1. The ability to record the direction of rotation.
1. Allows precision rotation settings causing the dial to rotate at a different rate than the gesture (including the ability to rotate in the opposite direction of the gesture).
1. It records the number of gesture rotations.
1. It records the number of image rotations.
1. It has a cumulative dial setting. When enabled the rotation will occur relative to the touch; and disabled the rotation will start from the point where the gesture starts.
1. It has an advanced angle snap feature with an angle snap tolerance setting. The tolerance causes the dial to rotate freely until the snap tolerance is met. It is also possible to add an array of irregular snap points also with snap proximity.
1. The dial can operate in single or dual finger mode.
1. With this dial it is possible to set a minimum/maximum rotation constraint.
1. It has a variable dial behaviour causing the rotation rate to change depending on how close the gesture is the centre of the dial.
1. This library comes with a 'fling-to-spin' behaviour; having configurable fling tolerance, spin start/end speed and spin animation duration.
1. A key feature is that the dial controls are designed to interact with each other and any other widgets/layouts that implement touch listeners.
1. All of the above features play together in perfect harmony.
1. Includes state management object.
1. Comes with a demo app complete with source code to help developers get a head start. Though the library is protected by an open source licence, the code for the demo app is free source.

**Important Note:**

Slow spin on curve behaviour has been removed and replaced with a 'Slow Factor' behaviour. See Commit Note (https://bitbucket.org/warwick/hg_dial_v2/commits/97822252e0a1299fae94a2c84da70884a743e50f) for potential beaks in your code when updating an existing application that uses this library.

**Developer Note:**

Over time the developer will be adding more and more sections to the demo app. These addendums will be based upon the most useful and common usage scenarios for the end developer. The developer will also be taking addendum requests. To submit an idea for the demo app you may email the developer at: warwickwestonwright@gmail.com. You may also use this same email address for developer support or if you wish to commission the services of the developer.

**What could it be used for with all of these features?**

It could be used for making an analogue clock for reading and/or setting the time, It could be used as a volume control in your music or video app, it could be used as an alternative way of selecting numbers, it could be used to make a simulation of an old fashioned telephone dial, it could be used as an alternative star rating selector. Have an app that controls things remotely like a thermostat, you could use it as a custom progress dialog. The usages are only limited by your imagination.

***You can make a donation the HG Widgets for Android here: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=DWT2TT7X83PE8***

**Here is a list of apps that use this library:**

(Used by HGDialV2) Metronome app https://play.google.com/store/apps/details?id=peoplesfeelingscode.com.samplemetronomerebuild

(Used by HGDialV2) Cricket Clicker - Umpire Counter https://play.google.com/store/apps/details?id=com.hammerheadinnovations.cricketclickerumpirecounter

(Used by HGDialV2) Awesome playlist app (Demo Version) https://play.google.com/store/apps/details?id=com.WarwickWestonWright.ABPlayListDemo

(Used by HGDialV2) Awesome playlist app (Premium Version) https://play.google.com/store/apps/details?id=com.WarwickWestonWright.ABPlayList

**See docs in this deployment for developer usage.**

**Other libraries include:**

HacerGestoV3 (https://bitbucket.org/warwick/hacergestov3) Gesture library allowing the simultaneous usage of the 4 classic gestures (Rotate, Scale/Pinch, Move and Fling)

**ToDo: Add dynamic centre point behaviour for dual touch mode. This behaviour will cause canvas to rotate from centre point of two touches rather than centre of canvas.**

## Roadmap for this control. ##
**The developer is considering adding and two finger behaviour that will set the rotation centre point to the centre point of the two touches rather than rotating from the centre of the texture. This will include the ability to programatically set the centre point for both single and dual touch. This is at present a low priority.**

**The developer will be making other libraries based upon this codebase.**

**There will be improvements to the HGFastList library to include fling-to-spin behaviour along with a variety of metric behaviours.**

**Plans on improvements to the date picker dial which will be based upon the current 'Date Picker Demo' seen in this demo app ('HGDialV2Demo'). The modifications will include the ability to mix manual text date entry with dial date entry.**

**The developer will be making an out of the box VideoView (demonstrated in the 'AB PlayList Demo' app on Google Play). The playlist app uses the variable dial behaviour on a transparent dial to fast forward and rewind videos and default behaviour to control the volume.**

**The developer will also be making an out of the box library for the 'Text Select Demo' (also seen in this app 'HGDialV2Demo'). This will include extra behavioural flags for angle snapping to letters, words, sentences or paragraphs along with fling-to-spin behaviour for large text objects. May also add a behaviour to scroll text from centre of screen instead of top or bottom. All these flags will be toggles.**

**LICENSE.**

This project is licensed with the 2-clause BSD license. The BSD 2-Clause License [OSI Approved License] The BSD 2-Clause License
In the original BSD license, both occurrences of the phrase "COPYRIGHT HOLDERS AND CONTRIBUTORS" in the disclaimer read "REGENTS AND CONTRIBUTORS".
Copyright (c) 2015, Warwick Weston Wright All rights reserved.
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
1. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.