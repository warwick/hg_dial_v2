/*
Though this is an open source distribution only the files in the HGDialV2 package are protected by open source license.

All of the files in this package (HGDialV2Demo) are free source. Any developer may refactor/re-purpose or reuse the contents
of the files in the HGDialV2Demo package without any obligations to the developer of the HGDialV2 library. The files in this
package (HGDialV2Demo) are intended to give developer/users of the HGDialV2 library a head start in understanding how to use the
diverse, open usage HGDialV2 library.
*/
package com.WarwickWestonWright.HGDialV2Demo

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.DialogInterface.OnShowListener
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.Point
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.Gravity
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import com.WarwickWestonWright.HGDialV2.HGDialInfo
import com.WarwickWestonWright.HGDialV2.HGDialV2
import com.WarwickWestonWright.HGDialV2.HGGeometry
import com.WarwickWestonWright.HGDialV2.HGState
import com.WarwickWestonWright.HGDialV2.HGViewContainer
import java.util.Formatter
import java.util.Locale

class DatePickerFragment : DialogFragment(), OnShowListener {
    private var iDatePickerFragment: IDatePickerFragment? = null
    private lateinit var rootView: View
    private lateinit var fLayoutDatePickerContainer: FrameLayout
    private lateinit var imgDateMask: ImageView
    private lateinit var imgMonthMask: ImageView
    private val dialImages = arrayOfNulls<Bitmap>(6)
    private lateinit var dateDial28: Drawable
    private lateinit var dateDial29: Drawable
    private lateinit var dateDial30: Drawable
    private lateinit var dateDial31: Drawable
    private lateinit var hgDialV2Month: HGDialV2
    private lateinit var hgDialV2Date: HGDialV2
    private lateinit var hgDialV2Dummy: HGDialV2
    private lateinit var ihgDialV2Dummy: HGDialV2.IHGDial
    private lateinit var hgViewContainer: HGViewContainer
    private lateinit var dateLp: FrameLayout.LayoutParams
    private lateinit var monthLp: FrameLayout.LayoutParams
    private var monthDialRadius = 0
    private val hgGeometry: HGGeometry = HGGeometry()
    private val fLayoutCenterPoint = Point(0, 0)
    private var dateParts = IntArray(3)
    private lateinit var dateTimeUtils: DateTimeUtils
    private var date: String = ""
    private var baseYear = 0
    private lateinit var lblDatePickerTitle: TextView
    private lateinit var btnSetDate: Button
    private lateinit var txtDate: EditText
    private lateinit var chkDateSnapping: CheckBox
    private lateinit var customToast: CustomToast
    private var dateDial = 0
    private var storedDateDial = 0
    private lateinit var formatter: Formatter
    private var dateSnapping = false
    private lateinit var sp: SharedPreferences
    private var innerPrecision = 0.0
    private var outerPrecision = 0.0
    private var useVariableDial = false
    private var dialingMonth = false
    private val deviceDimens = Point(0, 0)
    private var hgStateDatePickerDemo: HGState? = null
    private var wasSpinningOnDeviceRotation = false
    private lateinit var defaultLocale: Locale
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if(iDatePickerFragment == null) {
            iDatePickerFragment = activity as IDatePickerFragment
        }
        defaultLocale = Locale.getDefault()
        formatter = Formatter(defaultLocale)
        hgStateDatePickerDemo = activity?.intent?.getParcelableExtra("hgStateDatePickerDemo")
        dialingMonth = false
        customToast = CustomToast(activity as MainActivity)
        dateTimeUtils = DateTimeUtils()
        date = dateTimeUtils.getDate()
        sp = activity?.getSharedPreferences(getString(R.string.shared_pref_filename), Context.MODE_PRIVATE)!!
        deviceDimens.x = resources.displayMetrics.widthPixels
        deviceDimens.y = resources.displayMetrics.heightPixels
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = Dialog(activity as MainActivity)
        dialog.setOnShowListener(this)
        val root = RelativeLayout(activity)
        root.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(root)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.BLACK))
        dialog.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        dialog.setOnKeyListener { dialog, keyCode, event ->
            if(event.action == MotionEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                if(dialImages[0] != null) {
                    dialImages[0]?.recycle()
                }
                if(dialImages[1] != null) {
                    dialImages[1]?.recycle()
                }
                if(dialImages[2] != null) {
                    dialImages[2]?.recycle()
                }
                if(dialImages[3] != null) {
                    dialImages[3]?.recycle()
                }
                if(dialImages[4] != null) {
                    dialImages[4]?.recycle()
                }
                if(dialImages[5] != null) {
                    dialImages[5]?.recycle()
                }
                iDatePickerFragment?.datePickerFragmentCallback()
                true
            }
            else {
                false
            }
        }
        return dialog
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        rootView = inflater.inflate(R.layout.date_picker_fragment, container, false)
        lblDatePickerTitle = rootView.findViewById(R.id.lblDatePickerTitle)
        btnSetDate = rootView.findViewById(R.id.btnSetDate)
        txtDate = rootView.findViewById(R.id.txtDate)
        chkDateSnapping = rootView.findViewById(R.id.chkDateSnapping)
        imgDateMask = rootView.findViewById(R.id.imgDateMask)
        imgMonthMask = rootView.findViewById(R.id.imgMonthMask)
        fLayoutDatePickerContainer = rootView.findViewById(R.id.fLayoutDatePickerContainer)
        val datePartsStr = date.split("-").toTypedArray()
        dateParts[0] = datePartsStr[0].toInt()
        baseYear = dateParts[0]
        dateParts[1] = datePartsStr[1].toInt()
        dateParts[2] = datePartsStr[2].toInt()

        //Developer note May be an idea to scale these bitmaps the the minimum screen size and pixel density to same memory and improve performance.
        dialImages[0] = BitmapDrawable(
            resources,
            BitmapFactory.decodeStream(resources.openRawResource(R.raw.hg_month_28))
        ).bitmap
        dialImages[1] = BitmapDrawable(
            resources,
            BitmapFactory.decodeStream(resources.openRawResource(R.raw.hg_month_29))
        ).bitmap
        dialImages[2] = BitmapDrawable(
            resources,
            BitmapFactory.decodeStream(resources.openRawResource(R.raw.hg_month_30))
        ).bitmap
        dialImages[3] = BitmapDrawable(
            resources,
            BitmapFactory.decodeStream(resources.openRawResource(R.raw.hg_month_31))
        ).bitmap
        dialImages[4] = BitmapDrawable(
            resources,
            BitmapFactory.decodeStream(resources.openRawResource(R.raw.hg_month_dial))
        ).bitmap
        dialImages[5] = BitmapDrawable(
            resources,
            BitmapFactory.decodeResource(resources, R.drawable.hg_dummy_transparent_dial)
        ).bitmap
        dateDial28 = BitmapDrawable(resources, dialImages[0]).current
        dateDial29 = BitmapDrawable(resources, dialImages[1]).current
        dateDial30 = BitmapDrawable(resources, dialImages[2]).current
        dateDial31 = BitmapDrawable(resources, dialImages[3]).current
        hgViewContainer = HGViewContainer(fLayoutDatePickerContainer)
        fLayoutDatePickerContainer.removeAllViews()
        hgViewContainer.addDial(dialImages[3])
        fLayoutDatePickerContainer.addView(imgDateMask)
        hgViewContainer.addDial(dialImages[4])
        fLayoutDatePickerContainer.addView(imgMonthMask)
        hgViewContainer.addDial(dialImages[5])
        hgDialV2Date = hgViewContainer.hgDialV2s?.get(0)!!
        hgDialV2Month = hgViewContainer.hgDialV2s?.get(1)!!
        hgDialV2Dummy = hgViewContainer.hgDialV2s?.get(2)!!
        txtDate.setText(date)
        dateSnapping = chkDateSnapping.isChecked
        btnSetDate.setOnClickListener {
            if(dateTimeUtils.isRealDatePart(txtDate.text.toString().split(" ").toTypedArray()[0])) {
                hgDialV2Dummy.cancelSpin()
                dateParts = dateTimeUtils.yearPartsYYYYMMDD
                setAngleFromParts(dateParts)
            }
            else  /* if(dateTimeUtils.isRealDatePart(txtDate.getText().toString()) == false) */ {
                customToast.showCustomToast(getString(R.string.toast_invalid_date), Toast.LENGTH_LONG)
            }
        }
        chkDateSnapping.setOnClickListener{
            dateSnapping = chkDateSnapping.isChecked
        }
        ihgDialV2Dummy = object : HGDialV2.IHGDial {
            override fun onDown(hgDialInfo: HGDialInfo) {
                updateUI(Point(hgDialInfo.firstTouchX.toInt(), hgDialInfo.firstTouchY.toInt()))
            }

            override fun onPointerDown(hgDialInfo: HGDialInfo?) {}
            override fun onMove(hgDialInfo: HGDialInfo) {
                updateUI(Point(hgDialInfo.firstTouchX.toInt(), hgDialInfo.firstTouchY.toInt()))
            }

            override fun onPointerUp(hgDialInfo: HGDialInfo?) {}
            override fun onUp(hgDialInfo: HGDialInfo) {
                updateUI(Point(hgDialInfo.firstTouchX.toInt(), hgDialInfo.firstTouchY.toInt()))
                if(!hgDialInfo.isSpinTriggered) {
                    customToast.showCustomToast(getString(R.string.toast_you_picked_date) + date, Toast.LENGTH_LONG)
                }
            }
        }
        persistValues()
        return rootView
    }

    private fun setupViews(newWidth: Int, newHeight: Int) {
        dateLp = FrameLayout.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT,
            Gravity.CENTER
        )
        monthLp = FrameLayout.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT,
            Gravity.CENTER
        )
        if (newWidth < newHeight) {
            monthDialRadius = (newWidth * 0.325f).toInt() //Pick up from dial object center point
            val dateMargin = (newWidth * 0.075f).toInt()
            dateLp.setMargins(dateMargin, dateMargin, dateMargin, dateMargin)
            val monthMargin = (newWidth * 0.175f).toInt()
            monthLp.setMargins(monthMargin, monthMargin, monthMargin, monthMargin)
        }
        else {
            monthDialRadius = (newHeight * 0.325f).toInt() //Pick up from dial object center point
            val dateMargin = (newHeight * 0.075f).toInt()
            dateLp.setMargins(dateMargin, dateMargin, dateMargin, dateMargin)
            val monthMargin = (newHeight * 0.175f).toInt()
            monthLp.setMargins(monthMargin, monthMargin, monthMargin, monthMargin)
        }
        hgDialV2Date.layoutParams = dateLp
        imgDateMask.layoutParams = dateLp
        hgDialV2Month.layoutParams = monthLp
        imgMonthMask.layoutParams = monthLp
        fLayoutCenterPoint.x = newWidth / 2
        fLayoutCenterPoint.y = newHeight / 2
        hgViewContainer.activeDial = 2
        fLayoutDatePickerContainer.requestLayout()
    }

    private fun persistValues() {
        
        if(sp.getBoolean("enableFling", false)) {
            hgDialV2Dummy.setFlingTolerance(
                sp.getInt("flingDistance", 150),
                sp.getLong("flingTime", 200L)
            )
            hgDialV2Dummy.setSpinAnimation(
                sp.getFloat("spinStartSpeed", 0f),
                sp.getFloat("spinEndSpeed", 0f),
                sp.getLong("spinDuration", 200L)
            )
            hgDialV2Dummy.slowFactor = sp.getFloat("slowFactor", 0f)
            hgDialV2Dummy.slowDown = sp.getBoolean("chkSlowDown", true)
            hgDialV2Dummy.flingAngleTolerance = sp.getFloat("flingAngle", 0f).toDouble()
        }
        else if(!sp.getBoolean("enableFling", false)) {
            hgDialV2Dummy.setFlingTolerance(0, 0L)
            hgDialV2Dummy.setSpinAnimation(0f, 0f, 0L)
            hgDialV2Dummy.slowFactor = 0f
            hgDialV2Dummy.slowDown = true
            hgDialV2Dummy.flingAngleTolerance = 0.0
        }
        
        if(sp.getBoolean("useVariableDial", false)) {
            useVariableDial = sp.getBoolean("useVariableDial", false)
            innerPrecision = 72.0
            outerPrecision = 12.0
            hgDialV2Dummy.setUseVariableDialCurve(
                sp.getBoolean("useVariableCurve", false),
                sp.getBoolean("usePositiveCurve", false)
            )
        }
        else if(!sp.getBoolean("useVariableDial", false)) {
            hgDialV2Dummy.setVariableDial(0.0, 0.0, false)
        }
    }

    private fun updateUI(touchPoint: Point) {
        val distanceFromCenter: Double = hgGeometry.getTwoFingerDistance(
            fLayoutCenterPoint.x.toDouble(),
            fLayoutCenterPoint.y.toDouble(),
            touchPoint.x.toDouble(),
            touchPoint.y.toDouble()
        )
        if(distanceFromCenter < monthDialRadius) {

            //Touching Above Month
            if(useVariableDial) {
                if(!dialingMonth) {
                    hgDialV2Dummy.setVariableDial(innerPrecision, outerPrecision, true)
                    dialingMonth = true
                }
            }
            else {
                hgDialV2Dummy.precisionRotation = 12.0
            }
        }
        else  /* if(distanceFromCenter >= monthDialRadius) */ {
            if(dialingMonth) {
                hgDialV2Dummy.setVariableDial(innerPrecision, outerPrecision, false)
                dialingMonth = false
            }
            //Touching Above Date
            hgDialV2Dummy.precisionRotation = 1.0
        }
        val fullTextureAngle: Double = hgDialV2Dummy.fullTextureAngle
        var mantissa = 0.0
        if(fullTextureAngle >= 0) {
            dateParts[0] = baseYear + (fullTextureAngle / 12.0).toInt()
            dateParts[1] = (fullTextureAngle % 12).toInt() + 1
            mantissa = fullTextureAngle - (dateParts[1] - 1)
        }
        else if (fullTextureAngle < 0) {
            dateParts[0] = baseYear + (fullTextureAngle / 12.0).toInt() - 1
            dateParts[1] = 12 + fullTextureAngle.toInt() % 12
            mantissa = fullTextureAngle - (dateParts[1] - 1)
        }
        storedDateDial = dateDial
        if(dateParts[1] == 4 || dateParts[1] == 6 || dateParts[1] == 9 || dateParts[1] == 11) {
            dateDial = 30
        }
        else if (dateParts[1] == 2) {
            if(dateParts[0] % 4 != 0) {
                dateDial = 28
            }
            else if (dateParts[0] % 4 == 0) {
                dateDial = 29
            }
        }
        else {
            dateDial = 31
        }
        if(fullTextureAngle >= 0) {
            dateParts[2] = (mantissa * dateDial).toInt() % dateDial + 1
        }
        else if(fullTextureAngle < 0) {
            dateParts[2] = dateDial + (mantissa * dateDial).toInt() % dateDial
        }
        if(storedDateDial != dateDial) {
            setDateDial(dateDial)
        }
        if(!dateSnapping) {
            hgDialV2Date.doRapidDial(hgDialV2Dummy.fullTextureAngle)
            hgDialV2Month.doRapidDial(hgDialV2Dummy.fullTextureAngle / 12.0)
        }
        else {
            setAngleSnapDate()
        }
        lblDatePickerTitle.text = dateFromParts
    }

    private fun setAngleSnapDate() {
        hgDialV2Date.doRapidDial((dateParts[2] - 1).toDouble() / dateDial.toDouble())
        if(dateParts[2] != dateDial + 1) {
            hgDialV2Month.doRapidDial((dateParts[1].toDouble() - 1) / 12.0)
        }
        else if(dateParts[2] == dateDial + 1) {
            hgDialV2Month.doRapidDial(dateParts[1].toDouble() / 12.0)
        }
    }

    private fun setDateDial(dateDial: Int) {
        when (dateDial) {
            31 -> {
                hgDialV2Date.drawable = dateDial31
            }
            30 -> {
                hgDialV2Date.drawable = dateDial30
            }
            28 -> {
                hgDialV2Date.drawable = dateDial28
            }
            29 -> {
                hgDialV2Date.drawable = dateDial29
            }
        }
    }
    
    private val dateFromParts: String
        get() {
            val DOW = dateTimeUtils.getDayOfWeek(dateParts[0], dateParts[1], dateParts[2])
            formatter = Formatter()
            val lastDate = formatter.format(
                "%1$02d-%2$02d-%3$02d",
                dateParts[0],
                dateParts[1],
                dateParts[2]
            ).toString()
            date = "$lastDate $DOW"
            return date
        }

    private fun setAngleFromParts(dateParts: IntArray) {
        var dateAngle = 0.0
        if(dateParts[1] == 4 || dateParts[1] == 6 || dateParts[1] == 9 || dateParts[1] == 11) {
            dateDial = 30
            dateAngle = (dateParts[2] - 1) * (1.0 / 30.0)
        }
        else if(dateParts[1] == 2) {
            if(dateParts[1] % 4 != 0) {
                dateDial = 28
                dateAngle = (dateParts[2] - 1) * (1.0 / 28.0)
            }
            else if(dateParts[1] % 4 == 0) {
                dateDial = 29
                dateAngle = (dateParts[2] - 1) * (1.0 / 29.0)
            }
        }
        else {
            dateDial = 31
            dateAngle = (dateParts[2] - 1) * (1.0 / 31.0)
        }
        setDateDial(dateDial)
        hgDialV2Dummy.doManualTextureDial((dateParts[0] - baseYear) * 12 + ((dateParts[1] - 1) / 12.0 + dateAngle / 12) * 12)
        hgDialV2Month.doRapidDial((dateParts[1] - 1) / 12.0 + dateAngle / 12)
        hgDialV2Date.doRapidDial(hgDialV2Dummy.fullTextureAngle)
        if(dateSnapping) {
            setAngleSnapDate()
        }
        lblDatePickerTitle.text = dateFromParts
    }

    interface IDatePickerFragment {
        fun datePickerFragmentCallback()
    }

    fun getHgStateDatePickerDemo(): HGState {
        return hgDialV2Dummy.saveState()!!
    }

    override fun onShow(dialog: DialogInterface) {
        setupViews(fLayoutDatePickerContainer.width, fLayoutDatePickerContainer.height)
        if (hgStateDatePickerDemo != null) {
            hgDialV2Dummy.doManualTextureDial(hgStateDatePickerDemo?.fullGestureAngle!!)
            if(hgStateDatePickerDemo?.spinTriggered!!) {
                if(!hgDialV2Dummy.slowDown) {
                    hgDialV2Dummy.triggerSpin(
                        hgStateDatePickerDemo?.spinCurrentSpeed!!,
                        0F,
                        Long.MAX_VALUE,
                        hgStateDatePickerDemo?.lastTextureDirection!!
                    )
                }
                else if(hgDialV2Dummy.slowDown) {
                    if(hgStateDatePickerDemo?.spinTimeRemaining!! > 0) {
                        if(hgDialV2Dummy.slowDown) {
                            hgDialV2Dummy.triggerSpin(
                                hgStateDatePickerDemo?.spinCurrentSpeed!!,
                                0F,
                                hgStateDatePickerDemo?.spinTimeRemaining!!,
                                hgStateDatePickerDemo?.lastTextureDirection!!
                            )
                        }
                    }
                }
            }
        }
    }

    override fun onStart() {
        super.onStart()
        hgDialV2Dummy.registerCallback(ihgDialV2Dummy)
    }

    override fun onStop() {
        super.onStop()
        wasSpinningOnDeviceRotation = hgViewContainer.hgDialV2Active?.spinTriggered!!
        hgViewContainer.cancelSpinThread()
        hgDialV2Dummy.unRegisterCallback()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        iDatePickerFragment = if (context is IDatePickerFragment) {
            context
        }
        else {
            throw RuntimeException("$context must implement IDatePickerFragment")
        }
    }

    override fun onDetach() {
        super.onDetach()
        iDatePickerFragment = null
    }
}