/*
Though this is an open source distribution only the files in the HGDialV2 package are protected by open source license.

All of the files in this package (HGDialV2Demo) are free source. Any developer may refactor/re-purpose or reuse the contents
of the files in the HGDialV2Demo package without any obligations to the developer of the HGDialV2 library. The files in this
package (HGDialV2Demo) are intended to give developer/users of the HGDialV2 library a head start in understanding how to use the
diverse, open usage HGDialV2 library.
*/
package com.WarwickWestonWright.HGDialV2Demo

import android.app.Dialog
import android.content.ClipData
import android.content.ClipDescription.MIMETYPE_TEXT_PLAIN
import android.content.ClipboardManager
import android.content.Context
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.ActionMode
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import com.WarwickWestonWright.HGDialV2.HGDialInfo
import com.WarwickWestonWright.HGDialV2.HGDialV2
import com.WarwickWestonWright.HGDialV2.HGDialV2.IHGDial
import com.WarwickWestonWright.HGDialV2.SnapPoint
import java.util.regex.Matcher
import java.util.regex.Pattern
import kotlin.math.roundToInt

const val ACTION_REDO_FIRST_DIAL = 0
const val ACTION_COPY = 1
const val ACTION_CUT = 2
const val ACTION_PASTE = 3
const val ACTION_SELECT_ALL = 4
const val ACTION_REDO_SECOND_DIAL = 5
const val NO_SNAPPING = 0
const val LETTER_SNAPPING = 1
const val WORD_SNAPPING = 2
const val SENTENCE_SNAPPING = 3
const val PARAGRAPH_SNAPPING = 4

class TextSelectFragment : DialogFragment(),
    IHGDial,
    TextSelectSettingsFragment.ITextSelectSettingsFragment,
    View.OnLongClickListener,
    View.OnFocusChangeListener,
    ActionMode.Callback {

    private lateinit var rootView: View
    private lateinit var txtTheSecret: EditText
    private lateinit var txtSingleLineControl: EditText
    private lateinit var txtSubject: EditText
    private lateinit var btnTextSelectSettings: Button
    private lateinit var hgDialV2: HGDialV2
    private val textSelectSettingsFragment: TextSelectSettingsFragment = TextSelectSettingsFragment()
    private var theSecret: String? = ""
    private var singleLine: String? = ""
    private var dialSelStart = 0
    private var currentPosition = 0
    private var dialCount = 0
    private lateinit var sp: SharedPreferences
    private lateinit var customToast: CustomToast
    private lateinit var imm: InputMethodManager
    private val pattern: Pattern = Pattern.compile("[\\w|\\d]{1}")
    private var actionMode: ActionMode? = null
    private val anglePerChar: Double = 1.0.div(12.0)//0.08333333333333333
    private val letterSnapProximity: Double = anglePerChar.div(4.0)//0.020833333333333332
    private var minAngle: Double = 0.0
    private var maxAngle: Double = 0.0
    private lateinit var clipboard: ClipboardManager
    private var whichMenuToShow = 1
    private var defaultTextSelect: Boolean = false
    private var multiLineControl: Boolean = true

    private var snapping: Int = SENTENCE_SNAPPING

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if(arguments != null) {
            theSecret = arguments?.getString("theSecret")?.trim()
        }
        singleLine = getString(R.string.txt_single_line_test)
        customToast = CustomToast(activity as MainActivity)
        sp = activity?.getSharedPreferences(getString(R.string.shared_pref_filename), Context.MODE_PRIVATE)!!
        imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        clipboard = activity?.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager

        setSnapping(sp.getInt("textSnapping", SENTENCE_SNAPPING))

        Thread.setDefaultUncaughtExceptionHandler { thread, throwable ->
            customToast.showCustomToast(throwable.message, Toast.LENGTH_SHORT)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        rootView = inflater.inflate(R.layout.text_select_fragment, container, false)
        hgDialV2 = rootView.findViewById(R.id.hgDialV2)
        hgDialV2.registerCallback(this)
        txtTheSecret = rootView.findViewById(R.id.txtTheSecret)
        txtSingleLineControl = rootView.findViewById(R.id.txtSingleLineControl)
        btnTextSelectSettings = rootView.findViewById(R.id.btnTextSelectSettings)
        txtTheSecret.setOnLongClickListener(this)
        txtTheSecret.onFocusChangeListener = this
        txtSingleLineControl.setOnLongClickListener(this)
        txtSingleLineControl.onFocusChangeListener = this
        multiLineControl = sp.getBoolean("multiLineControl", true)
        setControlVisibility(multiLineControl)
        setupBehaviours()
        if(theSecret != null && theSecret?.trim() != "") {
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                txtTheSecret.setText(Html.fromHtml(theSecret, Html.FROM_HTML_OPTION_USE_CSS_COLORS))
            }
            else {
                txtTheSecret.setText(Html.fromHtml(theSecret))
            }
            if(snapping == LETTER_SNAPPING) {
                hgDialV2.setAngleSnap(anglePerChar, letterSnapProximity)
                hgDialV2.setSnapPoints(null)
            }
            else if(snapping != NO_SNAPPING) {
                hgDialV2.setAngleSnap(anglePerChar, letterSnapProximity)
                hgDialV2.setSnapPoints(null)
                multiLineControl = sp.getBoolean("multiLineControl", true)
                txtSubject = if(multiLineControl) {
                    txtTheSecret
                } else {
                    txtSingleLineControl
                }
                hgDialV2.setSnapPoints(getSnapPoints(txtSubject))
            }
        }
        btnTextSelectSettings.setOnClickListener { view ->
            textSelectSettingsFragment.setTargetFragment(requireActivity().supportFragmentManager.findFragmentByTag("TextSelectFragment") as TextSelectFragment, 0)
            textSelectSettingsFragment.show(requireActivity().supportFragmentManager, "TextSelectSettingsFragment")
            hgDialV2.visibility = View.GONE
            hideMenu()
            resetVars()
        }
        return rootView
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = Dialog(requireActivity())
        val root = RelativeLayout(requireActivity())
        root.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(root)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.BLACK))
        dialog.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        dialog.setOnKeyListener { dialog, keyCode, event ->
            if(event.action == MotionEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                txtSubject.clearFocus()
                resetVars()
                hgDialV2.visibility = View.GONE
                false
            }
            else {
                false
            }
        }
        return dialog
    }

    private fun setSnapping(snapping: Int) {
        when(snapping) {
            LETTER_SNAPPING -> {this.snapping = LETTER_SNAPPING}
            WORD_SNAPPING -> {this.snapping = WORD_SNAPPING}
            SENTENCE_SNAPPING -> {this.snapping = SENTENCE_SNAPPING}
            PARAGRAPH_SNAPPING -> {this.snapping = PARAGRAPH_SNAPPING}
        }
    }

    private fun getSnapPoints(txtSubject: EditText): MutableList<SnapPoint>? {
        if(snapping == NO_SNAPPING) {return null}
        val returnVal = mutableListOf<SnapPoint>()
        val text = txtSubject.text.toString()

        var delimiterCollection: Collection<String>? = null

        when(snapping) {
            LETTER_SNAPPING -> {
                hgDialV2.setAngleSnap(anglePerChar, letterSnapProximity)
                return null
            }
            WORD_SNAPPING -> {
                delimiterCollection = arrayListOf(" ", ".", ",", "?", "!", "'", "\"", "\n", "£", "$", "%", "+", "=", ":", ";", "(", ")", "[", "]", "{", "}",
                    ">", "<", "@", "#", "€", "*", "/", "\\",
                    "|", "~", "^", "&")
            }
            SENTENCE_SNAPPING -> {
                delimiterCollection = arrayListOf(".", "?", "!", ":", "\n")
            }
            PARAGRAPH_SNAPPING -> {
                delimiterCollection = if(txtSubject.maxLines == 1) {
                    arrayListOf("\n", "")
                }
                else {
                    arrayListOf("\n")
                }
            }
        }

        var i = 0
        var lastPoint: Int
        var intStrPair: Pair<Int, String>? = text.findAnyOf(delimiterCollection!!, 0, true)
        val proximity: Double
        if(intStrPair != null) {
            proximity = (intStrPair.first + 1).times(anglePerChar).div(4.0)
            val start = 0.0
            val end = (intStrPair.first + 1).times(anglePerChar)
            returnVal.add(i, SnapPoint(start, end, start + proximity, end - proximity))
            lastPoint = intStrPair.first
            i++
        }
        else {
            proximity = (text.length - 1).times(anglePerChar).div(4.0)
            val start = 0.0
            val end = (text.length - 1).times(anglePerChar)
            returnVal.add(0, SnapPoint(start, end, start + proximity, end - proximity))
            return returnVal
        }
        while(intStrPair != null) {
            intStrPair = text.findAnyOf(delimiterCollection, lastPoint + 1, true)
            if(intStrPair != null) {
                val matcher: Matcher = pattern.matcher(text)
                if(matcher.find(lastPoint)) {
                    val proximity: Double
                    if(snapping != SENTENCE_SNAPPING) {
                        proximity = ((intStrPair .first).times(anglePerChar) - (matcher.start()).times(anglePerChar)).div(4.0)
                        val start = (matcher.start()).times(anglePerChar)
                        val end = (intStrPair .first).times(anglePerChar)
                        returnVal.add(i, SnapPoint(start, end, start + proximity, end - proximity))
                    }
                    else {
                        proximity = ((intStrPair .first + 1).times(anglePerChar) - (matcher.start()).times(anglePerChar)).div(4.0)
                        val start = (matcher.start()).times(anglePerChar)
                        val end = (intStrPair .first + 1).times(anglePerChar)
                        returnVal.add(i, SnapPoint(start, end, start + proximity, end - proximity))
                    }
                }
                lastPoint = intStrPair.first
                i++
            }
        }

        intStrPair = text.findAnyOf(delimiterCollection, text.length -1, true)
        if(intStrPair == null) {
            val proximity: Double = ((text.length).times(anglePerChar) - lastPoint.times(anglePerChar)).div(4.0)
            val start = lastPoint.times(anglePerChar)
            val end = (text.length).times(anglePerChar)
            returnVal.add(i, SnapPoint(start, end, start + proximity, end - proximity))
        }
        return returnVal
    }

    private fun setupBehaviours() {
        //Enable/Disable variable dial behaviour
        if(sp.getBoolean("useVariableDial", false)) {
            hgDialV2.setVariableDial(
                sp.getFloat("innerPrecision", 5.2f).toDouble(),
                sp.getFloat("outerPrecision", 0.2f).toDouble(),
                true
            )
            hgDialV2.setUseVariableDialCurve(
                sp.getBoolean("useVariableCurve", false),
                sp.getBoolean("usePositiveCurve", false)
            )
        }
        else if(!sp.getBoolean("useVariableDial", false)) {
            hgDialV2.setVariableDial(0.0, 0.0, false)
        }

        //Enable/Disable fling-to-spin behaviour
        if(sp.getBoolean("enableFling", false)) {
            hgDialV2.setFlingTolerance(
                sp.getInt("flingDistance", 150),
                sp.getLong("flingTime", 200L)
            )
            hgDialV2.setSpinAnimation(
                sp.getFloat("spinStartSpeed", 0f),
                sp.getFloat("spinEndSpeed", 0f),
                sp.getLong("spinDuration", 200L)
            )
            hgDialV2.slowFactor = sp.getFloat("slowFactor", 0f)
            hgDialV2.slowDown = sp.getBoolean("chkSlowDown", true)
            hgDialV2.flingAngleTolerance = sp.getFloat("flingAngle", 0f).toDouble()
        }
        else if(!sp.getBoolean("enableFling", false)) {
            hgDialV2.setFlingTolerance(0, 0L)
            hgDialV2.setSpinAnimation(0f, 0f, 0L)
            hgDialV2.slowFactor = 0f
            hgDialV2.slowDown = true
            hgDialV2.flingAngleTolerance = 0.0
        }
    }

    override fun onDown(hgDialInfo: HGDialInfo?) {}
    override fun onPointerDown(hgDialInfo: HGDialInfo?) {}

    override fun onMove(hgDialInfo: HGDialInfo?) {
        try {
            if(snapping != LETTER_SNAPPING) {
                if(dialCount == 0) {
                    currentPosition = (hgDialInfo?.textureAngle?.div(anglePerChar))!!.roundToInt()
                    dialSelStart = currentPosition
                    txtSubject.setSelection(dialSelStart, currentPosition)
                }
                else if(dialCount == 1) {
                    currentPosition = (hgDialInfo?.textureAngle?.div(anglePerChar))!!.roundToInt()
                    txtSubject.setSelection(dialSelStart, currentPosition)
                }
            }
            else {
                if(dialCount == 0) {
                    currentPosition = (hgDialInfo?.textureAngle?.div(anglePerChar))!!.roundToInt()
                    dialSelStart = currentPosition
                    txtSubject.setSelection(dialSelStart, currentPosition)
                }
                else if(dialCount == 1) {
                    currentPosition = (hgDialInfo?.textureAngle?.div(anglePerChar))!!.roundToInt()
                    txtSubject.setSelection(dialSelStart, currentPosition)
                }
            }
        }
        catch(e: IndexOutOfBoundsException) {
            customToast.showCustomToast(e.message, Toast.LENGTH_SHORT)
        }
    }

    override fun onPointerUp(hgDialInfo: HGDialInfo?) {}

    override fun onUp(hgDialInfo: HGDialInfo?) {
        if(hgDialInfo?.isSpinTriggered!!) {return}
        dialCount++
        if(dialCount == 1) {
            whichMenuToShow = 2
            popUpMenu()
        }
        else if(dialCount == 2) {
            whichMenuToShow = 3
            hgDialV2.visibility = View.GONE
            popUpMenu()
        }
    }

    //onLongClick executed before onFocusChange
    override fun onLongClick(v: View?): Boolean {
        if(defaultTextSelect) {return false}
        hgDialV2.visibility = View.VISIBLE
        txtSubject = if(multiLineControl) {
            txtTheSecret
        }
        else {
            txtSingleLineControl
        }
        hgDialV2.setSnapPoints(getSnapPoints(txtSubject))
        setMaxAngle()
        resetVars()
        txtSubject.clearFocus()
        popUpMenu()
        return true
    }

    //onFocusChange executed after onLongClick
    override fun onFocusChange(v: View?, hasFocus: Boolean) {
        if(dialCount == 0) {
            dialSelStart = txtSubject.selectionStart
            if(hgDialV2.visibility == View.VISIBLE) {
                hgDialV2.doManualGestureDial(dialSelStart * anglePerChar)
            }
        }
    }

    private fun popUpMenu() {
        actionMode = null
        when(actionMode) {
            null -> {
                val window = dialog?.window
                val topLevel = window?.decorView ?: return
                actionMode = topLevel.startActionMode(this)
                view?.isSelected = true
                true
            }
            else -> false
        }
    }

    private fun hideMenu() {
        if(actionMode != null) {
            (actionMode as ActionMode).finish()
        }
    }

    override fun onDetach() {
        super.onDetach()
        resetVars()
    }

    private fun resetVars() {
        dialSelStart = 0
        currentPosition = 0
        dialCount = 0
        whichMenuToShow = 1
        theSecret = ""
    }

    private fun setMaxAngle() {
        val textLen = txtSubject.text.toString().length - 1
        maxAngle = (anglePerChar.times(textLen))
        hgDialV2.setMinMaxDial(minAngle, maxAngle + anglePerChar, true)
    }

    override fun onActionItemClicked(mode: ActionMode?, item: MenuItem?): Boolean {
        return when(item?.itemId) {
            R.id.menuDial1 -> {
                dialCount = 0
                true
            }
            R.id.menuCopy -> {
                copyText()
                resetVars()
                txtSubject.clearFocus()
                hgDialV2.visibility = View.GONE
                hideMenu()
                mode?.finish()
                true
            }
            R.id.menuCut -> {
                copyText()
                try {
                    txtSubject.setText(txtSubject.text.replaceRange(txtSubject.selectionStart, txtSubject.selectionEnd, ""))
                    setMaxAngle()
                    hgDialV2.setSnapPoints(getSnapPoints(txtSubject))
                }
                catch(e: IndexOutOfBoundsException) {
                    resetVars()
                    customToast.showCustomToast(e.message, Toast.LENGTH_SHORT)
                }
                resetVars()
                txtSubject.clearFocus()
                hgDialV2.visibility = View.GONE
                hideMenu()
                mode?.finish()
                true
            }
            R.id.menuPaste -> {
                if(clipboard.hasPrimaryClip()) {
                    if(clipboard.primaryClipDescription!!.hasMimeType(MIMETYPE_TEXT_PLAIN)) {
                        try {
                            val item = clipboard.primaryClip!!.getItemAt(0)
                            val pasteData = item.text as String
                            txtSubject.setText(txtSubject.text.replaceRange(txtSubject.selectionStart, txtSubject.selectionEnd, pasteData))
                            hgDialV2.setSnapPoints(getSnapPoints(txtSubject))
                            setMaxAngle()
                        }
                        catch(e: IndexOutOfBoundsException) {
                            resetVars()
                        }
                    }
                    else {
                        customToast.showCustomToast(getString(R.string.toast_clipboard_not_text), Toast.LENGTH_SHORT)
                    }
                }
                else {
                    customToast.showCustomToast(getString(R.string.toast_clipboard_empty), Toast.LENGTH_SHORT)
                }
                resetVars()
                txtSubject.clearFocus()
                hgDialV2.visibility = View.GONE
                hideMenu()
                mode?.finish()
                true
            }
            R.id.menuSelectAll -> {
                txtSubject.setSelection(0, txtSubject.text.toString().length -1)
                hgDialV2.visibility = View.GONE
                whichMenuToShow = 4
                hideMenu()
                popUpMenu()
                true
            }
            R.id.menuDial2 -> {
                dialCount = 1
                hgDialV2.visibility = View.VISIBLE
                true
            }
            else -> false
        }
    }

    override fun onCreateActionMode(mode: ActionMode?, menu: Menu?): Boolean {
        val inflater: MenuInflater = mode?.menuInflater!!
        inflater.inflate(R.menu.text_action, menu)
        return true
    }

    override fun onPrepareActionMode(mode: ActionMode?, menu: Menu?): Boolean {
        when(whichMenuToShow) {
            1 -> {//Show this menu when long click first pressed
                menu?.getItem(ACTION_REDO_FIRST_DIAL)?.isVisible = false
                menu?.getItem(ACTION_COPY)?.isVisible = false
                menu?.getItem(ACTION_CUT)?.isVisible = false
                menu?.getItem(ACTION_PASTE)?.isVisible = true
                menu?.getItem(ACTION_SELECT_ALL)?.isVisible = true
                menu?.getItem(ACTION_REDO_SECOND_DIAL)?.isVisible = false
            }
            2 -> {//Show this menu when first dial is executed
                menu?.getItem(ACTION_REDO_FIRST_DIAL)?.isVisible = true//Allow redo of first dial in case user missed target
                menu?.getItem(ACTION_COPY)?.isVisible = false
                menu?.getItem(ACTION_CUT)?.isVisible = false
                menu?.getItem(ACTION_PASTE)?.isVisible = true
                menu?.getItem(ACTION_SELECT_ALL)?.isVisible = false
                menu?.getItem(ACTION_REDO_SECOND_DIAL)?.isVisible = false
            }
            3 -> {//Show this menu when second dial is done
                menu?.getItem(ACTION_REDO_FIRST_DIAL)?.isVisible = false
                menu?.getItem(ACTION_COPY)?.isVisible = true
                menu?.getItem(ACTION_CUT)?.isVisible = true
                menu?.getItem(ACTION_PASTE)?.isVisible = true
                menu?.getItem(ACTION_SELECT_ALL)?.isVisible = false
                menu?.getItem(ACTION_REDO_SECOND_DIAL)?.isVisible = true//Allow redo of second dial in case user missed target
            }
            4 -> {//Show this menu when Select All has been chosen
                menu?.getItem(ACTION_REDO_FIRST_DIAL)?.isVisible = false
                menu?.getItem(ACTION_COPY)?.isVisible = true
                menu?.getItem(ACTION_CUT)?.isVisible = true
                menu?.getItem(ACTION_PASTE)?.isVisible = true
                menu?.getItem(ACTION_SELECT_ALL)?.isVisible = false
                menu?.getItem(ACTION_REDO_SECOND_DIAL)?.isVisible = false
            }
        }
        menu?.getItem(ACTION_REDO_FIRST_DIAL)?.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM)//Redo First Dial
        menu?.getItem(ACTION_COPY)?.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM)//Copy
        menu?.getItem(ACTION_CUT)?.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM)//Cut
        menu?.getItem(ACTION_PASTE)?.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM)//Paste
        menu?.getItem(ACTION_SELECT_ALL)?.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM)//Select All
        menu?.getItem(ACTION_REDO_SECOND_DIAL)?.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM)//Redo Second Dial
        return false
    }

    override fun onDestroyActionMode(mode: ActionMode?) {
        actionMode = null
    }

    private fun copyText(): String {
        return try {
            var returnVal = ""
            if(txtSubject.selectionStart > txtSubject.selectionEnd) {
                if(txtSubject.selectionStart > txtSubject.text.length -1) {
                    txtSubject.setSelection(txtSubject.selectionEnd, txtSubject.text.length)
                }
                else if(txtSubject.selectionEnd < 1) {
                    txtSubject.setSelection(0, txtSubject.selectionStart)
                }
                else {
                    txtSubject.setSelection(txtSubject.selectionEnd, txtSubject.selectionStart)
                }
            }
            returnVal = txtSubject.text.substring(txtSubject.selectionStart, txtSubject.selectionEnd)
            val clip: ClipData = ClipData.newPlainText(resources.getResourceEntryName(R.id.menuCopy), returnVal)
            clipboard.setPrimaryClip(clip)
            returnVal
        }
        catch(e: IndexOutOfBoundsException) {
            val clip: ClipData = ClipData.newPlainText(resources.getResourceEntryName(R.id.menuCopy), "")
            clipboard.setPrimaryClip(clip)
            resetVars()
            ""
        }
    }

    override fun textSelectSettingsCb(snapping: Int, multiLineControl: Boolean, defaultTextSelect: Boolean) {
        textSelectSettingsFragment.dialog?.dismiss()
        resetVars()
        setSnapping(snapping)
        txtTheSecret.clearFocus()
        txtSingleLineControl.clearFocus()
        setControlVisibility(multiLineControl)
        this.multiLineControl = multiLineControl
        this.defaultTextSelect = defaultTextSelect
    }

    private fun setControlVisibility(singleLineControl: Boolean) {
        if(singleLineControl) {
            txtTheSecret.visibility = View.VISIBLE
            txtSingleLineControl.visibility = View.GONE
            txtSubject = txtTheSecret
        }
        else {
            txtTheSecret.visibility = View.GONE
            txtSingleLineControl.visibility = View.VISIBLE
            txtSubject = txtSingleLineControl
        }
    }
}