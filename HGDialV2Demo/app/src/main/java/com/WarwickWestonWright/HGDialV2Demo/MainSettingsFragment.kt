/*
Though this is an open source distribution only the files in the HGDialV2 package are protected by open source license.

All of the files in this package (HGDialV2Demo) are free source. Any developer may refactor/re-purpose or reuse the contents
of the files in the HGDialV2Demo package without any obligations to the developer of the HGDialV2 library. The files in this
package (HGDialV2Demo) are intended to give developer/users of the HGDialV2 library a head start in understanding how to use the
diverse, open usage HGDialV2 library.
*/
package com.WarwickWestonWright.HGDialV2Demo

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.DialogInterface.OnShowListener
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.animation.Animation
import android.view.animation.Animation.AnimationListener
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.RelativeLayout
import androidx.fragment.app.DialogFragment

class MainSettingsFragment : DialogFragment(),
    OnShowListener,
    AnimationListener {

    private lateinit var rootView: View
    private lateinit var chkPrecision: CheckBox
    private lateinit var txtPrecision: EditText
    private lateinit var lLayoutUseVariableDial: LinearLayout
    private lateinit var chkUseVariableDial: CheckBox
    private lateinit var txtInnerPrecision: EditText
    private lateinit var txtOuterPrecision: EditText
    private lateinit var chkUseVariableCurve: CheckBox
    private lateinit var chkUsePositiveCurve: CheckBox
    private lateinit var chkCumulative: CheckBox
    private lateinit var chkSingleFinger: CheckBox
    private lateinit var lLayoutSnappingContainer: LinearLayout
    private lateinit var chkAngleSnap: CheckBox
    private lateinit var txtAngleSnap: EditText
    private lateinit var txtProximity: EditText
    private lateinit var btnCloseSettings: Button
    private lateinit var lLayoutMinMaxContainer: LinearLayout
    private lateinit var chkEnableMinMax: CheckBox
    private lateinit var txtMinDial: EditText
    private lateinit var txtMaxDial: EditText
    private lateinit var lLayoutFlingContainer: LinearLayout
    private lateinit var chkEnableFling: CheckBox
    private lateinit var txtFlingDistance: EditText
    private lateinit var txtFlingTime: EditText
    private lateinit var txtFlingStartSpeed: EditText
    private lateinit var txtFlingEndSpeed: EditText
    private lateinit var txtSpinDuration: EditText
    private lateinit var chkEnableQuickTap: CheckBox
    private lateinit var txtSlowFactor: EditText
    private lateinit var chkSlowDown: CheckBox
    private lateinit var txtFlingAngle: EditText
    private lateinit var txtQuickTapTime: EditText
    private lateinit var txtPrecisionFadeIn: Animation
    private lateinit var sp: SharedPreferences
    private var precisionFadeOut: Animation? = null
    private var chkUsePositiveCurveFadeIn: Animation? = null
    private var chkUsePositiveCurveFadeOut: Animation? = null
    private var snappingContainerFadeIn: Animation? = null
    private var snappingContainerFadeOut: Animation? = null
    private var useVariableDialFadeIn: Animation? = null
    private var useVariableDialFadeOut: Animation? = null
    private var enableMinMaxFadeIn: Animation? = null
    private var enableMinMaxFadeout: Animation? = null
    private var flingContainerFadeIn: Animation? = null
    private var flingContainerFadeOut: Animation? = null
    private var txtQuickTapTimeFadeIn: Animation? = null
    private var quickTapTimeFadeOut: Animation? = null
    private var mainDemoFragment: MainDemoFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        snappingContainerFadeIn = AnimationUtils.loadAnimation(activity, R.anim.anim_expand_in)
        snappingContainerFadeOut = AnimationUtils.loadAnimation(activity, R.anim.anim_shrink_out)
        useVariableDialFadeIn = AnimationUtils.loadAnimation(activity, R.anim.anim_expand_in)
        useVariableDialFadeOut = AnimationUtils.loadAnimation(activity, R.anim.anim_shrink_out)
        txtPrecisionFadeIn = AnimationUtils.loadAnimation(activity, R.anim.anim_expand_in)
        precisionFadeOut = AnimationUtils.loadAnimation(activity, R.anim.anim_shrink_out)
        chkUsePositiveCurveFadeIn = AnimationUtils.loadAnimation(activity, R.anim.anim_expand_in)
        chkUsePositiveCurveFadeOut = AnimationUtils.loadAnimation(activity, R.anim.anim_shrink_out)
        enableMinMaxFadeIn = AnimationUtils.loadAnimation(activity, R.anim.anim_expand_in)
        enableMinMaxFadeout = AnimationUtils.loadAnimation(activity, R.anim.anim_shrink_out)
        flingContainerFadeIn = AnimationUtils.loadAnimation(activity, R.anim.anim_expand_in)
        flingContainerFadeOut = AnimationUtils.loadAnimation(activity, R.anim.anim_shrink_out)
        txtQuickTapTimeFadeIn = AnimationUtils.loadAnimation(activity, R.anim.anim_expand_in)
        quickTapTimeFadeOut = AnimationUtils.loadAnimation(activity, R.anim.anim_shrink_out)
        sp = activity?.getSharedPreferences(getString(R.string.shared_pref_filename), Context.MODE_PRIVATE)!!
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = Dialog(requireActivity())
        val root = RelativeLayout(requireActivity())
        root.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(root)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.BLACK))
        dialog.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        dialog.setOnShowListener(this)
        dialog.setOnKeyListener { dialog, keyCode, event ->
            if(event.action == MotionEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                if(mainDemoFragment != null) {
                    mainDemoFragment?.setupBehaviours()
                }
                false
            }
            else {
                false
            }
        }
        return dialog
    }

    @SuppressLint("SetTextI18n")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        rootView = inflater.inflate(R.layout.main_settings_fragment, container, false)
        txtPrecisionFadeIn.setAnimationListener(this)
        precisionFadeOut?.setAnimationListener(this)
        useVariableDialFadeIn?.setAnimationListener(this)
        useVariableDialFadeOut?.setAnimationListener(this)
        chkUsePositiveCurveFadeOut?.setAnimationListener(this)
        snappingContainerFadeOut?.setAnimationListener(this)
        flingContainerFadeOut?.setAnimationListener(this)
        quickTapTimeFadeOut?.setAnimationListener(this)
        enableMinMaxFadeout?.setAnimationListener(this)
        lLayoutSnappingContainer = rootView.findViewById(R.id.lLayoutSnappingContainer)
        chkPrecision = rootView.findViewById(R.id.chkPrecision)
        txtPrecision = rootView.findViewById(R.id.txtPrecision)
        lLayoutUseVariableDial = rootView.findViewById(R.id.lLayoutUseVariableDial)
        chkUseVariableDial = rootView.findViewById(R.id.chkUseVariableDial)
        txtInnerPrecision = rootView.findViewById(R.id.txtInnerPrecision)
        txtOuterPrecision = rootView.findViewById(R.id.txtOuterPrecision)
        chkUseVariableCurve = rootView.findViewById(R.id.chkUseVariableCurve)
        chkUsePositiveCurve = rootView.findViewById(R.id.chkUsePositiveCurve)
        chkCumulative = rootView.findViewById(R.id.chkCumulative)
        chkSingleFinger = rootView.findViewById(R.id.chkSingleFinger)
        chkAngleSnap = rootView.findViewById(R.id.chkAngleSnap)
        txtAngleSnap = rootView.findViewById(R.id.txtAngleSnap)
        txtProximity = rootView.findViewById(R.id.txtProximity)
        lLayoutMinMaxContainer = rootView.findViewById(R.id.lLayoutMinMaxContainer)
        chkEnableMinMax = rootView.findViewById(R.id.chkEnableMinMax)
        txtMinDial = rootView.findViewById(R.id.txtMinDial)
        txtMaxDial = rootView.findViewById(R.id.txtMaxDial)
        lLayoutFlingContainer = rootView.findViewById(R.id.lLayoutFlingContainer)
        chkEnableFling = rootView.findViewById(R.id.chkEnableFling)
        txtFlingDistance = rootView.findViewById(R.id.txtFlingDistance)
        txtFlingTime = rootView.findViewById(R.id.txtFlingTime)
        txtFlingStartSpeed = rootView.findViewById(R.id.txtFlingStartSpeed)
        txtFlingEndSpeed = rootView.findViewById(R.id.txtFlingEndSpeed)
        txtSpinDuration = rootView.findViewById(R.id.txtSpinDuration)
        chkEnableQuickTap = rootView.findViewById(R.id.chkEnableQuickTap)
        txtSlowFactor = rootView.findViewById(R.id.txtSlowFactor)
        chkSlowDown = rootView.findViewById(R.id.chkSlowDown)
        txtFlingAngle = rootView.findViewById(R.id.txtFlingAngle)
        txtQuickTapTime = rootView.findViewById(R.id.txtQuickTapTime)
        btnCloseSettings = rootView.findViewById(R.id.btnCloseSettings)
        chkPrecision.isChecked = sp.getBoolean("usePrecision", true)
        txtPrecision.setText(sp.getFloat("precision", 0.5f).toString())
        chkUseVariableDial.isChecked = sp.getBoolean("useVariableDial", false)
        txtInnerPrecision.setText(sp.getFloat("innerPrecision", 5.2f).toString())
        txtOuterPrecision.setText(sp.getFloat("outerPrecision", 0.2f).toString())
        chkUseVariableCurve.isChecked = sp.getBoolean("useVariableCurve", false)
        chkUsePositiveCurve.isChecked = sp.getBoolean("usePositiveCurve", false)
        chkCumulative.isChecked = sp.getBoolean("cumulativeDial", true)
        chkSingleFinger.isChecked = sp.getBoolean("isSingleFinger", true)
        chkAngleSnap.isChecked = sp.getBoolean("useAngleSnap", false)
        txtAngleSnap.setText((sp.getFloat("angleSnap", 0.125f).toString()))
        txtProximity.setText(sp.getFloat("snapProximity", 0.03125f).toString())
        chkEnableMinMax.isChecked = sp.getBoolean("useMinMax", false)
        txtMinDial.setText(sp.getFloat("minRotation", -2.45f).toString())
        txtMaxDial.setText(sp.getFloat("maxRotation", 2.88f).toString())
        chkEnableFling.isChecked = sp.getBoolean("enableFling", false)
        txtFlingDistance.setText(sp.getInt("flingDistance", 150).toString())
        txtFlingTime.setText(sp.getLong("flingTime", 200L).toString())
        txtFlingStartSpeed.setText(sp.getFloat("spinStartSpeed", 0f).toString())
        txtFlingEndSpeed.setText(sp.getFloat("spinEndSpeed", 0f).toString())
        txtSpinDuration.setText(sp.getLong("spinDuration", 8000L).toString())
        chkEnableQuickTap.isChecked = sp.getBoolean("enableQuickTap", false)
        txtSlowFactor.setText(sp.getFloat("slowFactor", 0f).toString())
        chkSlowDown.isChecked = sp.getBoolean("chkSlowDown", true)
        txtFlingAngle.setText(sp.getFloat("flingAngle", 0f).toString())
        txtQuickTapTime.setText(sp.getLong("quickTapTime", 120L).toString())
        if(chkPrecision.isChecked) {
            txtPrecision.visibility = View.VISIBLE
            txtPrecision.requestLayout()
        }
        else if(!chkPrecision.isChecked) {
            txtPrecision.visibility = View.GONE
            txtPrecision.requestLayout()
        }
        if(chkUseVariableDial.isChecked) {
            lLayoutUseVariableDial.visibility = View.VISIBLE
            lLayoutUseVariableDial.requestLayout()
        }
        else if(!chkUseVariableDial.isChecked) {
            lLayoutUseVariableDial.visibility = View.GONE
            lLayoutUseVariableDial.requestLayout()
        }
        if(chkUseVariableCurve.isChecked) {
            chkUsePositiveCurve.visibility = View.VISIBLE
            chkUsePositiveCurve.requestLayout()
        }
        else if(!chkUseVariableCurve.isChecked) {
            chkUsePositiveCurve.visibility = View.GONE
            chkUsePositiveCurve.requestLayout()
        }
        if(chkAngleSnap.isChecked) {
            lLayoutSnappingContainer.visibility = View.VISIBLE
            lLayoutSnappingContainer.requestLayout()
        }
        else if(!chkAngleSnap.isChecked) {
            lLayoutSnappingContainer.visibility = View.GONE
            lLayoutSnappingContainer.requestLayout()
        }
        if(chkEnableMinMax.isChecked) {
            lLayoutMinMaxContainer.visibility = View.VISIBLE
            lLayoutMinMaxContainer.requestLayout()
        }
        else if(!chkEnableMinMax.isChecked) {
            lLayoutMinMaxContainer.visibility = View.GONE
            lLayoutMinMaxContainer.requestLayout()
        }
        if(chkEnableFling.isChecked) {
            lLayoutFlingContainer.visibility = View.VISIBLE
            lLayoutFlingContainer.requestLayout()
        }
        else if(!chkEnableFling.isChecked) {
            lLayoutFlingContainer.visibility = View.GONE
            lLayoutFlingContainer.requestLayout()
        }

        //chkEnableQuickTap
        if(chkEnableQuickTap.isChecked) {
            txtQuickTapTime.visibility = View.VISIBLE
            txtQuickTapTime.requestLayout()
        }
        else if(!chkEnableQuickTap.isChecked) {
            txtQuickTapTime.visibility = View.GONE
            txtQuickTapTime.requestLayout()
        }
        chkPrecision.setOnClickListener{
            sp.edit()?.putBoolean("usePrecision", chkPrecision.isChecked)?.apply()
            if(chkPrecision.isChecked) {
                txtPrecision.visibility = View.VISIBLE
                txtPrecision.startAnimation(txtPrecisionFadeIn)
            }
            else if(!chkPrecision.isChecked) {
                txtPrecision.startAnimation(precisionFadeOut)
            }
        }
        chkUseVariableDial.setOnClickListener{
            sp.edit()?.putBoolean("useVariableDial", chkUseVariableDial.isChecked)?.apply()
            if(chkUseVariableDial.isChecked) {
                lLayoutUseVariableDial.visibility = View.VISIBLE
                lLayoutUseVariableDial.requestLayout()
                lLayoutUseVariableDial.startAnimation(useVariableDialFadeIn)
            }
            else if(!chkUseVariableDial.isChecked) {
                lLayoutUseVariableDial.startAnimation(useVariableDialFadeOut)
            }
        }
        chkUseVariableCurve.setOnClickListener {
            sp.edit()?.putBoolean("useVariableCurve", chkUseVariableCurve.isChecked)?.apply()
            if(chkUseVariableCurve.isChecked) {
                chkUsePositiveCurve.visibility = View.VISIBLE
                chkUsePositiveCurve.startAnimation(chkUsePositiveCurveFadeIn)
            }
            else if(!chkUseVariableCurve.isChecked) {
                chkUsePositiveCurve.startAnimation(chkUsePositiveCurveFadeOut)
            }
        }
        chkUsePositiveCurve.setOnClickListener {
            sp.edit()?.putBoolean("usePositiveCurve", chkUsePositiveCurve.isChecked)?.apply()
        }
        chkCumulative.setOnClickListener {
            sp.edit()?.putBoolean("cumulativeDial", chkCumulative.isChecked)?.apply()
        }
        chkSingleFinger.setOnClickListener {
            sp.edit()?.putBoolean("isSingleFinger", chkSingleFinger.isChecked)?.apply()
        }
        chkAngleSnap.setOnClickListener {
            sp.edit()?.putBoolean("useAngleSnap", chkAngleSnap.isChecked)?.apply()
            if(chkAngleSnap.isChecked) {
                lLayoutSnappingContainer.visibility = View.VISIBLE
                lLayoutSnappingContainer.requestLayout()
                lLayoutSnappingContainer.startAnimation(snappingContainerFadeIn)
            }
            else if(!chkAngleSnap.isChecked) {
                lLayoutSnappingContainer.startAnimation(snappingContainerFadeOut)
            }
        }
        chkEnableMinMax.setOnClickListener {
            sp.edit()?.putBoolean("useMinMax", chkEnableMinMax.isChecked)?.apply()
            if(chkEnableMinMax.isChecked) {
                lLayoutMinMaxContainer.visibility = View.VISIBLE
                lLayoutMinMaxContainer.requestLayout()
                lLayoutMinMaxContainer.startAnimation(enableMinMaxFadeIn)
            }
            else if(!chkEnableMinMax.isChecked) {
                lLayoutMinMaxContainer.startAnimation(enableMinMaxFadeout)
            }
        }
        chkEnableFling.setOnClickListener {
            sp.edit()?.putBoolean("enableFling", chkEnableFling.isChecked)?.apply()
            if(chkEnableFling.isChecked) {
                lLayoutFlingContainer.visibility = View.VISIBLE
                lLayoutFlingContainer.requestLayout()
                lLayoutFlingContainer.startAnimation(flingContainerFadeIn)
            }
            else if(!chkEnableFling.isChecked) {
                lLayoutFlingContainer.startAnimation(flingContainerFadeOut)
            }
        }
        chkEnableQuickTap.setOnClickListener {
            sp.edit()?.putBoolean("enableQuickTap", chkEnableQuickTap.isChecked)?.apply()
            if(chkEnableQuickTap.isChecked) {
                txtQuickTapTime.visibility = View.VISIBLE
                txtQuickTapTime.startAnimation(txtQuickTapTimeFadeIn)
            }
            else if(!chkEnableQuickTap.isChecked) {
                txtQuickTapTime.startAnimation(quickTapTimeFadeOut)
            }
        }
        chkSlowDown.setOnClickListener {
            sp.edit()?.putBoolean("chkSlowDown", chkSlowDown.isChecked)?.apply()
        }
        btnCloseSettings.setOnClickListener {
            setPrefsOnExit()
            if(mainDemoFragment != null) {
                mainDemoFragment?.setupBehaviours()
            }
            dismiss()
        }
        return rootView
    }

    override fun onShow(dialog: DialogInterface?) {
        if(mainDemoFragment == null) {
            mainDemoFragment = activity?.supportFragmentManager?.findFragmentByTag("MainDemoFragment") as MainDemoFragment?
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setPrefsOnExit() {
        if(txtPrecision.text.toString().isEmpty()) {
            txtPrecision.setText((sp.getFloat("precision", 0.5f).toString()))
        }
        if(txtInnerPrecision.text.toString().isEmpty()) {
            txtInnerPrecision.setText(sp.getFloat("innerPrecision", 5.2f).toString())
        }
        if(txtOuterPrecision.text.toString().isEmpty()) {
            txtOuterPrecision.setText(sp.getFloat("outerPrecision", 0.2f).toString())
        }
        if(txtAngleSnap.text.toString().isEmpty()) {
            txtAngleSnap.setText(sp.getFloat("angleSnap", 0.125f).toString())
        }
        if(txtProximity.text.toString().isEmpty()) {
            txtProximity.setText(sp.getFloat("snapProximity", 0.03125f).toString())
        }
        if(txtMinDial.text.toString().isEmpty()) {
            txtMinDial.setText(sp.getFloat("minRotation", -2.45f).toString())
        }
        if(txtMaxDial.text.toString().isEmpty()) {
            txtMaxDial.setText((sp.getFloat("maxRotation", 2.88f).toString()))
        }
        if(txtFlingDistance.text.toString().isEmpty()) {
            txtFlingDistance.setText(sp.getInt("flingDistance", 150).toString())
        }
        if(txtFlingTime.text.toString().isEmpty()) {
            txtFlingTime.setText(sp.getLong("flingTime", 200L).toString())
        }
        if(txtFlingStartSpeed.text.toString().isEmpty()) {
            txtFlingStartSpeed.setText(sp.getFloat("spinStartSpeed", 0f).toString())
        }
        if(txtFlingEndSpeed.text.toString().isEmpty()) {
            txtFlingEndSpeed.setText(sp.getFloat("spinEndSpeed", 0f).toString())
        }
        if(txtSlowFactor.text.toString().isEmpty()) {
            txtSlowFactor.setText(sp.getFloat("slowFactor", 0f).toString())
        }
        if(txtSpinDuration.text.toString().isEmpty()) {
            txtSpinDuration.setText(sp.getLong("spinDuration", 8000L).toString())
        }
        if(txtQuickTapTime.text.toString().isEmpty()) {
            txtQuickTapTime.setText(sp.getLong("quickTapTime", 120L).toString())
        }
        if(txtFlingAngle.text.toString().isEmpty()) {
            txtFlingAngle.setText((sp.getFloat("flingAngle", 0f).toString()))
        }
        sp.edit()?.putFloat("precision", txtPrecision.text.toString().toFloat())?.apply()
        sp.edit()?.putFloat("innerPrecision", txtInnerPrecision.text.toString().toFloat())?.apply()
        sp.edit()?.putFloat("outerPrecision", txtOuterPrecision.text.toString().toFloat())?.apply()
        sp.edit()?.putFloat("angleSnap", txtAngleSnap.text.toString().toFloat())?.apply()
        sp.edit()?.putFloat("snapProximity", txtProximity.text.toString().toFloat())?.apply()
        sp.edit()?.putFloat("minRotation", txtMinDial.text.toString().toFloat())?.apply()
        sp.edit()?.putFloat("maxRotation", txtMaxDial.text.toString().toFloat())?.apply()
        sp.edit()?.putInt("flingDistance", txtFlingDistance.text.toString().toInt())?.apply()
        sp.edit()?.putLong("flingTime", txtFlingTime.text.toString().toLong())?.apply()
        sp.edit()?.putFloat("spinStartSpeed", txtFlingStartSpeed.text.toString().toFloat())?.apply()
        sp.edit()?.putFloat("spinEndSpeed", txtFlingEndSpeed.text.toString().toFloat())?.apply()
        sp.edit()?.putFloat("slowFactor", txtSlowFactor.text.toString().toFloat())?.apply()
        sp.edit()?.putLong("spinDuration", txtSpinDuration.text.toString().toLong())?.apply()
        sp.edit()?.putLong("quickTapTime", txtQuickTapTime.text.toString().toLong())?.apply()
        sp.edit()?.putFloat("flingAngle", txtFlingAngle.text.toString().toFloat())?.apply()
    }

    override fun onAnimationStart(animation: Animation) {}
    override fun onAnimationEnd(animation: Animation) {
        if(animation == snappingContainerFadeOut) {
            lLayoutSnappingContainer.visibility = View.GONE
            lLayoutSnappingContainer.requestLayout()
        }
        else if(animation == useVariableDialFadeOut) {
            lLayoutUseVariableDial.visibility = View.GONE
            lLayoutUseVariableDial.requestLayout()
        }
        else if(animation == useVariableDialFadeIn) {
            lLayoutUseVariableDial.visibility = View.VISIBLE
            lLayoutUseVariableDial.requestLayout()
            if(chkPrecision.isChecked) {
                chkPrecision.performClick()
            }
        }
        else if(animation == precisionFadeOut) {
            txtPrecision.visibility = View.GONE
            txtPrecision.requestLayout()
        }
        else if(animation == txtPrecisionFadeIn) {
            txtPrecision.visibility = View.VISIBLE
            txtPrecision.requestLayout()
            if(chkUseVariableDial.isChecked) {
                chkUseVariableDial.performClick()
            }
        }
        else if(animation == chkUsePositiveCurveFadeOut) {
            chkUsePositiveCurve.visibility = View.GONE
            chkUsePositiveCurve.requestLayout()
        }
        else if(animation == enableMinMaxFadeout) {
            lLayoutMinMaxContainer.visibility = View.GONE
            lLayoutMinMaxContainer.requestLayout()
        }
        else if(animation == flingContainerFadeOut) {
            lLayoutFlingContainer.visibility = View.GONE
            lLayoutFlingContainer.requestLayout()
        }
        else if(animation == quickTapTimeFadeOut) {
            txtQuickTapTime.visibility = View.GONE
            txtQuickTapTime.requestLayout()
        }
    }

    override fun onAnimationRepeat(animation: Animation) {}
}