/*
Though this is an open source distribution only the files in the HGDialV2 package are protected by open source license.

All of the files in this package (HGDialV2Demo) are free source. Any developer may refactor/re-purpose or reuse the contents
of the files in the HGDialV2Demo package without any obligations to the developer of the HGDialV2 library. The files in this
package (HGDialV2Demo) are intended to give developer/users of the HGDialV2 library a head start in understanding how to use the
diverse, open usage HGDialV2 library.
*/
package com.WarwickWestonWright.HGDialV2Demo

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.WarwickWestonWright.HGDialV2Demo.DummyContent.DummyContent

class FastListAdapter(private val dummyContents: List<DummyContent>, private val iFastListFragment: FastListFragment.IFastListFragment?) :
    RecyclerView.Adapter<FastListAdapter.ViewHolder>(), View.OnClickListener {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.list_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.dummyItem = dummyContents[position]
        holder.lblFastListId.text = holder.dummyItem?.idLabel
        holder.detail.text = holder.dummyItem?.details
        holder.lblFastListId.tag = holder.dummyItem?.id
        holder.lblFastListId.setOnClickListener(this)
    }

    override fun getItemCount(): Int { return dummyContents.size }

    override fun onClick(v: View) {
        iFastListFragment?.itemFragmentCallback(null, dummyContents[v.tag.toString().toInt()])
    }

    inner class ViewHolder(mainView: View) : RecyclerView.ViewHolder(mainView) {
        val lblFastListId: TextView = mainView.findViewById(R.id.lblFastListId)
        val detail: TextView = mainView.findViewById(R.id.detail)
        var dummyItem: DummyContent? = null
        override fun toString(): String {
            return super.toString() + " '" + detail.text + "'"
        }
    }
}