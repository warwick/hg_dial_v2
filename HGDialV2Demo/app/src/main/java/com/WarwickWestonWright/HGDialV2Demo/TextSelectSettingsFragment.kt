package com.WarwickWestonWright.HGDialV2Demo

import android.app.Dialog
import android.content.Context
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.CheckBox
import androidx.fragment.app.DialogFragment

class TextSelectSettingsFragment : DialogFragment(), View.OnClickListener {

    interface ITextSelectSettingsFragment {
        fun textSelectSettingsCb(snapping: Int, multiLineControl: Boolean, defaultTextSelect: Boolean)
    }

    private var iTextSelectSettingsFragment: ITextSelectSettingsFragment? = null
    private lateinit var rootView: View
    private lateinit var sp: SharedPreferences

    private lateinit var chkLetterSnapping: CheckBox
    private lateinit var chkWordSnapping: CheckBox
    private lateinit var chkSentenceSnapping: CheckBox
    private lateinit var chkParagraphSnapping: CheckBox
    private lateinit var chkMultiLineControl: CheckBox
    private lateinit var chkDisableForTest: CheckBox
    private lateinit var btnTextSelectSettingsClose: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        iTextSelectSettingsFragment = targetFragment as TextSelectFragment
        sp = activity?.getSharedPreferences(getString(R.string.shared_pref_filename), Context.MODE_PRIVATE)!!
        if(!sp.getBoolean("textSelectIsSetup", false)) {
            sp.edit().putInt("textSnapping", SENTENCE_SNAPPING).apply()
            sp.edit().putBoolean("multiLineControl", true).apply()
            sp.edit().putBoolean("disableForTest", false).apply()
            sp.edit().putBoolean("textSelectIsSetup", true).apply()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        rootView = inflater.inflate(R.layout.text_select_settings_fragment, container, false)
        chkLetterSnapping = rootView.findViewById(R.id.chkLetterSnapping)
        chkWordSnapping = rootView.findViewById(R.id.chkWordSnapping)
        chkSentenceSnapping = rootView.findViewById(R.id.chkSentenceSnapping)
        chkParagraphSnapping = rootView.findViewById(R.id.chkParagraphSnapping)
        chkMultiLineControl = rootView.findViewById(R.id.chkMultiLineControl)
        chkDisableForTest = rootView.findViewById(R.id.chkDisableForTest)
        btnTextSelectSettingsClose = rootView.findViewById(R.id.btnTextSelectSettingsClose)

        chkLetterSnapping.setOnClickListener(this)
        chkWordSnapping.setOnClickListener(this)
        chkSentenceSnapping.setOnClickListener(this)
        chkParagraphSnapping.setOnClickListener(this)

        when(sp.getInt("textSnapping", SENTENCE_SNAPPING)) {
            LETTER_SNAPPING -> {
                chkLetterSnapping.isChecked = true
            }
            WORD_SNAPPING -> {
                chkWordSnapping.isChecked = true
            }
            SENTENCE_SNAPPING -> {
                chkSentenceSnapping.isChecked = true
            }
            PARAGRAPH_SNAPPING -> {
                chkParagraphSnapping.isChecked = true
            }
        }

        chkMultiLineControl.isChecked = sp.getBoolean("multiLineControl", true)
        chkDisableForTest.isChecked = sp.getBoolean("disableForTest", false)

        chkDisableForTest.setOnClickListener {
            sp.edit().putBoolean("disableForTest", chkDisableForTest.isChecked).commit()
        }

        chkMultiLineControl.setOnClickListener {
            sp.edit().putBoolean("multiLineControl", chkMultiLineControl.isChecked).commit()
        }

        btnTextSelectSettingsClose.setOnClickListener {
            iTextSelectSettingsFragment?.textSelectSettingsCb(sp.getInt("textSnapping", SENTENCE_SNAPPING), chkMultiLineControl.isChecked, chkDisableForTest.isChecked)
        }

        return rootView
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = Dialog(requireActivity())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        return dialog
    }

    override fun onClick(v: View?) {
        when((v as CheckBox).id) {
            R.id.chkLetterSnapping -> {
                chkLetterSnapping.isChecked = true
                chkWordSnapping.isChecked = false
                chkSentenceSnapping.isChecked = false
                chkParagraphSnapping.isChecked = false
                sp.edit().putInt("textSnapping", LETTER_SNAPPING).apply()
            }
            R.id.chkWordSnapping -> {
                chkLetterSnapping.isChecked = false
                chkWordSnapping.isChecked = true
                chkSentenceSnapping.isChecked = false
                chkParagraphSnapping.isChecked = false
                sp.edit().putInt("textSnapping", WORD_SNAPPING).apply()
            }
            R.id.chkSentenceSnapping -> {
                chkLetterSnapping.isChecked = false
                chkWordSnapping.isChecked = false
                chkSentenceSnapping.isChecked = true
                chkParagraphSnapping.isChecked = false
                sp.edit().putInt("textSnapping", SENTENCE_SNAPPING).apply()
            }
            R.id.chkParagraphSnapping -> {
                chkLetterSnapping.isChecked = false
                chkWordSnapping.isChecked = false
                chkSentenceSnapping.isChecked = false
                chkParagraphSnapping.isChecked = true
                sp.edit().putInt("textSnapping", PARAGRAPH_SNAPPING).apply()
            }
        }
    }
}