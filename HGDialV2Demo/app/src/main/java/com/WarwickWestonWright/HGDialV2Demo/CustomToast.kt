/*
Though this is an open source distribution only the files in the HGDialV2 package are protected by open source license.

All of the files in this package (HGDialV2Demo) are free source. Any developer may refactor/re-purpose or reuse the contents
of the files in the HGDialV2Demo package without any obligations to the developer of the HGDialV2 library. The files in this
package (HGDialV2Demo) are intended to give developer/users of the HGDialV2 library a head start in understanding how to use the
diverse, open usage HGDialV2 library.
*/
package com.WarwickWestonWright.HGDialV2Demo

import android.content.res.Resources
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

internal class CustomToast(val context: AppCompatActivity) {
    private var toast: Toast = Toast(context)
    private var customToastLayout: View = LayoutInflater.from(context).inflate(R.layout.toast_layout, null)
    private var lblCustomToast: TextView = customToastLayout.findViewById(R.id.LblCustomToast)

    fun showCustomToast(message: String?, messageDuration: Int) {
        lblCustomToast.text = message
        toast.setGravity(Gravity.CENTER_VERTICAL or Gravity.BOTTOM, 0, getPxFromDP(160, context.resources))
        toast.duration = messageDuration
        toast.view = customToastLayout
        if(toast.view!!.isShown) {
            toast.cancel()
        }
        toast.show()
    }

    private fun getPxFromDP(dimensionDp: Int, resources: Resources): Int {
        val density = resources.displayMetrics.density
        return (dimensionDp * density + 0.5f).toInt()
    }

}