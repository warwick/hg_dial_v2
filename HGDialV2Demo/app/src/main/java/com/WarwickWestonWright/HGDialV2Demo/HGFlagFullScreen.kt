/*
Though this is an open source distribution only the files in the HGDialV2 package are protected by open source license.

All of the files in this package (HGDialV2Demo) are free source. Any developer may refactor/re-purpose or reuse the contents
of the files in the HGDialV2Demo package without any obligations to the developer of the HGDialV2 library. The files in this
package (HGDialV2Demo) are intended to give developer/users of the HGDialV2 library a head start in understanding how to use the
diverse, open usage HGDialV2 library.
*/
package com.WarwickWestonWright.HGDialV2Demo

import android.view.View
import android.view.Window

internal class HGFlagFullScreen(private val window: Window) {
    fun flagFullScreen() {
        val uiOptions: Int = View.SYSTEM_UI_FLAG_FULLSCREEN or
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY or
                View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
        window.decorView.systemUiVisibility = uiOptions
    }
}