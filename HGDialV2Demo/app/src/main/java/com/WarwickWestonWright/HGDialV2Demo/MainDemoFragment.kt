/*
Though this is an open source distribution only the files in the HGDialV2 package are protected by open source license.

All of the files in this package (HGDialV2Demo) are free source. Any developer may refactor/re-purpose or reuse the contents
of the files in the HGDialV2Demo package without any obligations to the developer of the HGDialV2 library. The files in this
package (HGDialV2Demo) are intended to give developer/users of the HGDialV2 library a head start in understanding how to use the
diverse, open usage HGDialV2 library.
*/
package com.WarwickWestonWright.HGDialV2Demo

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.DialogInterface.OnShowListener
import android.content.SharedPreferences
import android.content.res.Configuration
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import com.WarwickWestonWright.HGDialV2.HGDialInfo
import com.WarwickWestonWright.HGDialV2.HGDialV2
import com.WarwickWestonWright.HGDialV2.HGState

class MainDemoFragment : DialogFragment(), OnShowListener {
    interface IMainDemoFragment {
        fun mainDemoFragmentCallback(hgState: HGState?, dismissDialog: Boolean)
    }

    private var iMainDemoFragment: IMainDemoFragment? = null
    private var ihgDial: HGDialV2.IHGDial? = null
    private lateinit var rootView: View
    private lateinit var hgDialV2: HGDialV2
    private lateinit var lblMainDemoStatus: TextView
    private var status: String = ""
    private var mainSettingsFragment = MainSettingsFragment()
    private lateinit var sp: SharedPreferences
    //Saves/Restores State for all settings
    private var hgStateMainDemo: HGState? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if(iMainDemoFragment == null) {
            iMainDemoFragment = activity as IMainDemoFragment
        }
        status = ""
        sp = activity?.getSharedPreferences(getString(R.string.shared_pref_filename), Context.MODE_PRIVATE)!!
        hgStateMainDemo = activity?.intent?.getParcelableExtra("hgStateMainDemo")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        rootView = inflater.inflate(R.layout.main_demo_fragment, container, false)
        hgDialV2 = rootView.findViewById(R.id.hgDialV2)!!
        lblMainDemoStatus = rootView.findViewById(R.id.lblMainDemoStatus)!!
        ihgDial = object : HGDialV2.IHGDial {
            override fun onDown(hgDialInfo: HGDialInfo) {
                status =
                    """
                    TextureAngle: ${hgDialInfo.textureAngle}
                    TextureDirection: ${hgDialInfo.textureDirection}
                    GestureAngle: ${hgDialInfo.gestureAngle}
                    GestureDirection: ${hgDialInfo.gestureDirection}
                    SpinSpeed: ${hgDialInfo.spinCurrentSpeed}
                    SpinTriggered: ${hgDialInfo.isSpinTriggered}
                    rotateSnapped: ${hgDialInfo.rotateSnapped}
                    """.trimIndent()
                lblMainDemoStatus.text = status
            }

            override fun onPointerDown(hgDialInfo: HGDialInfo?) {}
            override fun onMove(hgDialInfo: HGDialInfo) {
                //TextureAngle: ${hgDialInfo.textureAngle} replace with TextureAngle:${hgDialV2.fullTextureAngle} for old behaviour
                status =
                    """
                    TextureAngle: ${hgDialInfo.textureAngle}
                    TextureDirection: ${hgDialInfo.textureDirection}
                    GestureAngle: ${hgDialInfo.gestureAngle}
                    GestureDirection: ${hgDialInfo.gestureDirection}
                    SpinSpeed: ${hgDialInfo.spinCurrentSpeed}
                    SpinTriggered: ${hgDialInfo.isSpinTriggered}
                    rotateSnapped: ${hgDialInfo.rotateSnapped}
                    """.trimIndent()
                lblMainDemoStatus.text = status
            }

            override fun onPointerUp(hgDialInfo: HGDialInfo?) {}
            override fun onUp(hgDialInfo: HGDialInfo) {
                status =
                    """
                    TextureAngle: ${hgDialInfo.textureAngle}
                    TextureDirection: ${hgDialInfo.textureDirection}
                    GestureAngle: ${hgDialInfo.gestureAngle}
                    GestureDirection: ${hgDialInfo.gestureDirection}
                    SpinSpeed: ${hgDialInfo.spinCurrentSpeed}
                    SpinTriggered: ${hgDialInfo.isSpinTriggered}
                    rotateSnapped: ${hgDialInfo.rotateSnapped}
                    """.trimIndent()
                lblMainDemoStatus.text = status
                hgStateMainDemo = hgDialV2.saveState()
                if(hgDialInfo.quickTap) {
                    if(!mainSettingsFragment.isAdded) {
                        val bundle = Bundle()
                        bundle.putString("OpenedFrom", "MainDemoFragment")
                        mainSettingsFragment.arguments = bundle
                        iMainDemoFragment?.mainDemoFragmentCallback(hgDialV2.saveState(), false)
                        mainSettingsFragment.show(requireActivity().supportFragmentManager, "MainSettingsFragment")
                    }
                }
            }
        }
        return rootView
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = Dialog(requireActivity())
        val root = RelativeLayout(activity)
        root.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(root)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.BLACK))
        dialog.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        dialog.setOnShowListener(this)
        dialog.setOnKeyListener { dialog, keyCode, event ->
            if(event.action == MotionEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                iMainDemoFragment?.mainDemoFragmentCallback(null, true)
                true
            }
            else {
                false
            }
        }
        return dialog
    }

    override fun onShow(dialogInterface: DialogInterface) {
        setupBehaviours()
    }

    fun setupBehaviours() {

        //Enable/Disable precision dial behaviour
        if(sp.getBoolean("usePrecision", false)) {
            hgDialV2.precisionRotation = sp.getFloat("precision", 0.5F).toDouble()
        }
        else if(!sp.getBoolean("usePrecision", false)) {
            hgDialV2.precisionRotation = 1.0
        }

        //Enable/Disable variable dial behaviour
        if(sp.getBoolean("useVariableDial", false)) {
            hgDialV2.setVariableDial(
                sp.getFloat("innerPrecision", 5.2F).toDouble(),
                sp.getFloat("outerPrecision", 0.2F).toDouble(),
                true
            )
            hgDialV2.setUseVariableDialCurve(
                sp.getBoolean("useVariableCurve", false),
                sp.getBoolean("usePositiveCurve", false)
            )
        }
        else if(!sp.getBoolean("useVariableDial", false)) {
            hgDialV2.setVariableDial(0.0, 0.0, false)
        }

        //Enable/Disable cumulative dial behaviour
        hgDialV2.cumulativeRotate = sp.getBoolean("cumulativeDial", true)

        //Enable/Disable single finger behaviour
        hgDialV2.useSingleFinger = sp.getBoolean("isSingleFinger", true)

        //Setup angle snapping behaviour
        if(sp.getBoolean("useAngleSnap", false)) {
            hgDialV2.setAngleSnap(
                sp.getFloat("angleSnap", 0.125F).toDouble(),
                sp.getFloat("snapProximity", 0.03125F).toDouble()
            )
        }
        else if(!sp.getBoolean("useAngleSnap", false)) {
            hgDialV2.setAngleSnap(0.0, 0.0)
        }

        //Enable/Disable min/max behaviour
        if(sp.getBoolean("useMinMax", false)) {
            hgDialV2.setMinMaxDial(
                sp.getFloat("minRotation", -2.45F).toDouble(),
                sp.getFloat("maxRotation", 2.88F).toDouble(),
                true
            )
        }
        else if(!sp.getBoolean("useMinMax", false)) {
            hgDialV2.setMinMaxDial(0.0, 0.0, false)
        }

        //Enable/Disable fling-to-spin behaviour
        if(sp.getBoolean("enableFling", false)) {
            hgDialV2.setFlingTolerance(
                sp.getInt("flingDistance", 150),
                sp.getLong("flingTime", 200L)
            )
            hgDialV2.setSpinAnimation(
                sp.getFloat("spinStartSpeed", 0f),
                sp.getFloat("spinEndSpeed", 0f),
                sp.getLong("spinDuration", 200L)
            )
            hgDialV2.slowFactor = sp.getFloat("slowFactor", 0f)
            hgDialV2.slowDown = sp.getBoolean("chkSlowDown", true)
            hgDialV2.flingAngleTolerance = sp.getFloat("flingAngle", 0f).toDouble()
        }
        else if(!sp.getBoolean("enableFling", false)) {
            hgDialV2.setFlingTolerance(0, 0L)
            hgDialV2.setSpinAnimation(0f, 0f, 0L)
            hgDialV2.slowFactor = 0f
            hgDialV2.slowDown = true
            hgDialV2.flingAngleTolerance = 0.0
        }

        //Enable/Disable QuickTap
        if(sp.getBoolean("enableQuickTap", false)) {
            hgDialV2.quickTapTime = sp.getLong("quickTapTime", 200L)
        }
        else if(!sp.getBoolean("enableQuickTap", false)) {
            hgDialV2.quickTapTime = 0L
        }
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        hgDialV2.cancelSpin()
        if(hgStateMainDemo != null) {
            hgDialV2.restoreState(hgStateMainDemo, true)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        iMainDemoFragment = if (context is IMainDemoFragment) {
            context
        }
        else {
            throw RuntimeException("$context must implement IMainDemoFragment")
        }
    }

    override fun onStart() {
        super.onStart()
        hgDialV2.registerCallback(ihgDial)
    }

    override fun onStop() {
        super.onStop()
        hgDialV2.cancelSpin()
        hgDialV2.unRegisterCallback()
    }

    override fun onDetach() {
        super.onDetach()
        iMainDemoFragment = null
    }
}