/*
Though this is an open source distribution only the files in the HGDialV2 package are protected by open source license.

All of the files in this package (HGDialV2Demo) are free source. Any developer may refactor/re-purpose or reuse the contents
of the files in the HGDialV2Demo package without any obligations to the developer of the HGDialV2 library. The files in this
package (HGDialV2Demo) are intended to give developer/users of the HGDialV2 library a head start in understanding how to use the
diverse, open usage HGDialV2 library.
*/
package com.WarwickWestonWright.HGDialV2Demo

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.DialogInterface.OnShowListener
import android.content.SharedPreferences
import android.content.res.Configuration
import android.graphics.Color
import android.graphics.Point
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.WarwickWestonWright.HGDialV2.HGDialInfo
import com.WarwickWestonWright.HGDialV2.HGDialV2
import com.WarwickWestonWright.HGDialV2.HGDialV2.IHGDial
import com.WarwickWestonWright.HGDialV2Demo.DummyContent.DummyContent
import com.WarwickWestonWright.HGDialV2Demo.FastListFragment.IFastListFragment

class FastListHolderFragment : DialogFragment(),
    IFastListFragment,
    IHGDial,
    OnShowListener {

    interface IFastListFragment {
        fun fastListFragmentCallback()
    }

    private var iFastListFragment: IFastListFragment? = null
    private lateinit var rootView: View
    private lateinit var lblFastListTitle: TextView
    private lateinit var txtNumberOfItems: EditText
    private lateinit var btnSelect: Button
    private lateinit var fLayoutHGDialContainer: RelativeLayout
    private lateinit var customToast: CustomToast
    private lateinit var sp: SharedPreferences
    private lateinit var fm: FragmentManager
    private lateinit var fastListFragment: FastListFragment
    private lateinit var recyclerView: RecyclerView
    private lateinit var hgFastListController: HGDialV2
    private lateinit var hgContainerDimens: Point
    private var numberOfItems = 0
    private var listTotalHeight = 0.0
    private var itemHeight = 0.0
    private var itemsPerPage = 0.0
    private var pageHeight = 0.0
    private var currentYPosition = 0
    private var spinCapturedOnDown = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if(iFastListFragment == null) {
            iFastListFragment = (requireActivity() as IFastListFragment?)!!
        }
        customToast = CustomToast(requireActivity() as AppCompatActivity)
        sp = requireActivity().getSharedPreferences(getString(R.string.shared_pref_filename), Context.MODE_PRIVATE)!!
        numberOfItems = sp.getInt("listLength", 1000)
        fm = requireActivity().supportFragmentManager
        spinCapturedOnDown = false
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = Dialog(requireActivity() as MainActivity)
        dialog.setOnShowListener(this)
        val root = RelativeLayout(requireActivity())
        root.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(root)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.BLACK))
        dialog.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        dialog.setOnKeyListener { dialog, keyCode, event ->
            if(event.action == MotionEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                iFastListFragment?.fastListFragmentCallback()
                true
            }
            else {
                false
            }
        }
        return dialog
    }

    @SuppressLint("SetTextI18n")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        rootView = inflater.inflate(R.layout.fast_list_holder_fragment, container, false)
        fLayoutHGDialContainer = rootView.findViewById(R.id.fLayoutHGDialContainer)
        numberOfItems = sp.getInt("listLength", 1000)
        lblFastListTitle = rootView.findViewById(R.id.lblFastListTitle)
        txtNumberOfItems = rootView.findViewById(R.id.txtNumberOfItems)
        hgFastListController = rootView.findViewById(R.id.hgFastListController)
        persistSettings()
        hgFastListController.visibility = View.GONE
        btnSelect = rootView.findViewById(R.id.btnSelect)
        txtNumberOfItems.setText(sp.getInt("listLength", 1000).toString())
        btnSelect.setOnClickListener {
            numberOfItems = txtNumberOfItems.text.toString().toInt()
            fastListFragment.refreshList(resources.configuration, numberOfItems)
            recyclerView = fastListFragment.recyclerView
            hgFastListController.doManualTextureDial(0.0)
            recyclerView = fastListFragment.recyclerView
            hgFastListController.setMinMaxDial(
                0.0,
                numberOfItems.toDouble() / itemsPerPage,
                true
            )
            positionDial()
            hgFastListController.visibility = View.VISIBLE
        }
        return rootView
    }

    override fun onShow(dialog: DialogInterface) {
        recyclerView = fastListFragment.recyclerView
        hgFastListController.doManualTextureDial(0.0)
        itemHeight = recyclerView.getChildAt(0).height.toDouble()
        listTotalHeight = recyclerView.computeVerticalScrollRange().toDouble()
        pageHeight = recyclerView.height.toDouble()
        itemsPerPage = pageHeight / itemHeight
    }

    private fun persistSettings() {

        //Enable/Disable precision dial behaviour
        if(sp.getBoolean("usePrecision", false)) {
            hgFastListController.precisionRotation = sp.getFloat("precision", 0.5f).toDouble()
        }
        else if(!sp.getBoolean("usePrecision", false)) {
            hgFastListController.precisionRotation = 1.0
        }

        //Enable/Disable variable dial behaviour
        if(sp.getBoolean("useVariableDial", false)) {
            hgFastListController.setVariableDial(
                sp.getFloat("innerPrecision", 5.2f).toDouble(),
                sp.getFloat("outerPrecision", 0.2f).toDouble(),
                true
            )
            hgFastListController.setUseVariableDialCurve(
                sp.getBoolean("useVariableCurve", false),
                sp.getBoolean("usePositiveCurve", false)
            )
        }
        else if(!sp.getBoolean("useVariableDial", false)) {
            hgFastListController.setVariableDial(0.0, 0.0, false)
        }

        //Enable/Disable fling-to-spin behaviour
        if(sp.getBoolean("enableFling", false)) {
            hgFastListController.setFlingTolerance(
                sp.getInt("flingDistance", 150),
                sp.getLong("flingTime", 200L)
            )
            hgFastListController.setSpinAnimation(
                sp.getFloat("spinStartSpeed", 0f),
                sp.getFloat("spinEndSpeed", 0f),
                sp.getLong("spinDuration", 200L)
            )
            hgFastListController.slowFactor = sp.getFloat("slowFactor", 0f)
            hgFastListController.slowDown = sp.getBoolean("chkSlowDown", true)
            hgFastListController.flingAngleTolerance = sp.getFloat("flingAngle", 0f).toDouble()
        }
        else if(!sp.getBoolean("enableFling", false)) {
            hgFastListController.setFlingTolerance(0, 0L)
            hgFastListController.setSpinAnimation(0f, 0f, 0L)
            hgFastListController.slowFactor = 0f
            hgFastListController.slowDown = true
            hgFastListController.flingAngleTolerance = sp.getFloat("flingAngle", 0f).toDouble()
        }
    }

    override fun onDown(hgDialInfo: HGDialInfo) {
        spinCapturedOnDown = hgDialInfo.isSpinTriggered == true
    }

    override fun onPointerDown(hgDialInfo: HGDialInfo) {}
    override fun onMove(hgDialInfo: HGDialInfo) {
        try {
            currentYPosition = (hgDialInfo.textureAngle * (pageHeight / itemHeight)).toInt()
            //Dev note: LSmoothScroller not working in Kotlin need to fix
            //recyclerView?.layoutManager?.smoothScrollToPosition(recyclerView, null, currentYPosition)
            recyclerView.layoutManager?.scrollToPosition(currentYPosition)
            recyclerView.invalidate()
            lblFastListTitle.text = currentYPosition.toString()
        }
        catch(e: IllegalArgumentException) {
            if(currentYPosition < 0) {
                currentYPosition = 0
            }
            else if(currentYPosition < recyclerView.adapter!!.itemCount - 1) {
                currentYPosition = recyclerView.adapter!!.itemCount - 1
            }
            recyclerView.layoutManager?.smoothScrollToPosition(recyclerView, null, currentYPosition)
            lblFastListTitle.text = currentYPosition.toString()
        }
    }

    override fun onPointerUp(hgDialInfo: HGDialInfo) {}
    override fun onUp(hgDialInfo: HGDialInfo) {
        if(hgDialInfo.isSpinTriggered) {
            hgFastListController.visibility = View.VISIBLE
        }
        else if(!hgDialInfo.isSpinTriggered && !spinCapturedOnDown) {
            hgFastListController.visibility = View.GONE
        }
        if(!hgDialInfo.isSpinTriggered || hgDialInfo.minMaxReached != 0) {
            if(hgDialInfo.minMaxReached != 0) {
                if(hgDialInfo.minMaxReached == 1) {
                    currentYPosition = recyclerView.adapter!!.itemCount - 1
                    hgFastListController.doManualTextureDial(hgFastListController.maximumRotation)
                    lblFastListTitle.text = currentYPosition.toString()
                    customToast.showCustomToast(getString(R.string.toast_fast_list_bounds_reached_picked) + currentYPosition.toString(), Toast.LENGTH_LONG)
                }
                else if(hgDialInfo.minMaxReached == -1) {
                    currentYPosition = 0
                    hgFastListController.doManualTextureDial(hgFastListController.minimumRotation)
                    lblFastListTitle.text = currentYPosition.toString()
                    customToast.showCustomToast(getString(R.string.toast_fast_list_bounds_reached_picked) + currentYPosition.toString(), Toast.LENGTH_LONG)
                }
            }
            else if(hgDialInfo.minMaxReached == 0) {
                customToast.showCustomToast(getString(R.string.toast_fast_list_you_picked) + currentYPosition.toString(), Toast.LENGTH_LONG)
            }
        }
        spinCapturedOnDown = false
    }

    private fun positionDial() {
        hgContainerDimens = Point(fLayoutHGDialContainer.width, fLayoutHGDialContainer.height)
        if(hgContainerDimens.x <= hgContainerDimens.y) {
            (hgFastListController.parent as FrameLayout).layoutParams.width = hgContainerDimens.x
            (hgFastListController.parent as FrameLayout).layoutParams.height = hgContainerDimens.x
        }
        else if(hgContainerDimens.x > hgContainerDimens.y) {
            (hgFastListController.parent as FrameLayout).layoutParams.width = hgContainerDimens.y
            (hgFastListController.parent as FrameLayout).layoutParams.height = hgContainerDimens.y
        }
    }

    override fun onStart() {
        super.onStart()
        hgFastListController.registerCallback(this)
    }

    override fun onStop() {
        super.onStop()

        sp.edit().putInt("listLength", txtNumberOfItems.text.toString().toInt()).apply()
        hgFastListController.unRegisterCallback()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        hgFastListController.cancelSpin()
        super.onConfigurationChanged(newConfig)
        hgFastListController.cancelSpin()
        hgFastListController.doManualTextureDial(0.0)
        hgFastListController.visibility = View.GONE
    }

    override fun itemFragmentCallback(fastListFragment: FastListFragment?, dummyContent: DummyContent?) {
        if (fastListFragment != null) {
            this.fastListFragment = fastListFragment
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        iFastListFragment = if (context is IFastListFragment) {
            context
        }
        else {
            throw RuntimeException("$context must implement IFastListFragment")
        }
    }

    override fun onDetach() {
        super.onDetach()
        iFastListFragment = null
    }
}