/*
Though this is an open source distribution only the files in the HGDialV2 package are protected by open source license.

All of the files in this package (HGDialV2Demo) are free source. Any developer may refactor/re-purpose or reuse the contents
of the files in the HGDialV2Demo package without any obligations to the developer of the HGDialV2 library. The files in this
package (HGDialV2Demo) are intended to give developer/users of the HGDialV2 library a head start in understanding how to use the
diverse, open usage HGDialV2 library.
*/
package com.WarwickWestonWright.HGDialV2Demo

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.DialogInterface.OnShowListener
import android.content.SharedPreferences
import android.content.res.Configuration
import android.graphics.Color
import android.graphics.Point
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.CheckBox
import android.widget.FrameLayout
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import com.WarwickWestonWright.HGDialV2.HGDialInfo
import com.WarwickWestonWright.HGDialV2.HGDialV2
import com.WarwickWestonWright.HGDialV2.HGDialV2.IHGDial
import com.WarwickWestonWright.HGDialV2.HGGeometry
import com.WarwickWestonWright.HGDialV2.HGState
import com.WarwickWestonWright.HGDialV2.HGViewContainer

class CogDemoFragment : DialogFragment(), OnShowListener {
    private var actingDials: Boolean = false
    private var iCogDemoFragment: ICogDemoFragment? = null
    private lateinit var ihgBigDial: IHGDial
    private lateinit var ihgSmallDial: IHGDial
    private lateinit var rootView: View
    private lateinit var hgBigCogViewContainer: HGViewContainer
    private lateinit var hgSmallCogViewContainer: HGViewContainer
    private lateinit var hgDialV2Big: HGDialV2
    private lateinit var hgDialV2Small: HGDialV2
    private lateinit var hgBigCogV2FrameLayout: FrameLayout
    private lateinit var hgSmallCogV2FrameLayout: FrameLayout
    private lateinit var parentDimens: Point
    private lateinit var hgGeometry: HGGeometry
    private lateinit var sp: SharedPreferences
    private lateinit var chkActingDials: CheckBox

    private lateinit var customToast: CustomToast

    @Volatile//Saves/Restores State for all settings plus is used to demonstrate restoring of  spin state in case you need to nullify a fragment
    private var hgStateBigCog: HGState? = null

    @Volatile//Saves/Restores State for all settings plus is used to demonstrate restoring of  spin state in case you need to nullify a fragment
    private var hgStateSmallCog: HGState? = null

    interface ICogDemoFragment {
        fun cogDemoFragmentCallback()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if(iCogDemoFragment == null) {
            iCogDemoFragment = activity as ICogDemoFragment
        }
        parentDimens = Point(resources.displayMetrics.widthPixels, resources.displayMetrics.heightPixels)
        hgGeometry = HGGeometry()
        retainInstance = true
        sp = activity?.getSharedPreferences(getString(R.string.shared_pref_filename), Context.MODE_PRIVATE)!!
        customToast = CustomToast(activity as MainActivity)
        hgStateBigCog = activity?.intent?.getParcelableExtra("hgStateBigCog")
        hgStateSmallCog = activity?.intent?.getParcelableExtra("hgStateSmallCog")
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = Dialog(activity as MainActivity)
        dialog.setOnShowListener(this)
        val root = RelativeLayout(activity)
        root.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(root)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.BLACK))
        dialog.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        dialog.setOnKeyListener { dialog, keyCode, event ->
            if(event.action == MotionEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                hgDialV2Big.cancelSpin()
                hgDialV2Big.saveState("hgStateBigCog")
                hgDialV2Big.saveStateFlush()
                hgDialV2Small.cancelSpin()
                hgDialV2Small.saveState("hgStateSmallCog")
                hgDialV2Small.saveStateFlush()
                iCogDemoFragment?.cogDemoFragmentCallback()
                true
            }
            else {
                false
            }
        }
        return dialog
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        rootView = inflater.inflate(R.layout.cog_demo_fragment, container, false)
        hgBigCogV2FrameLayout = rootView.findViewById(R.id.hgBigCogV2FrameLayout)
        hgSmallCogV2FrameLayout = rootView.findViewById(R.id.hgSmallCogV2FrameLayout)
        hgBigCogViewContainer = HGViewContainer(R.drawable.hg_big_cog, hgBigCogV2FrameLayout)
        hgSmallCogViewContainer = HGViewContainer(R.drawable.hg_small_cog, hgSmallCogV2FrameLayout)
        hgDialV2Big = hgBigCogViewContainer.hgDialV2Active
        hgDialV2Small = hgSmallCogViewContainer.hgDialV2Active
        chkActingDials = rootView.findViewById(R.id.chkActingDials)
        actingDials = activity?.intent?.getBooleanExtra("actingDials", true)!!
        chkActingDials.isChecked = actingDials
        ihgBigDial = object : IHGDial {
            override fun onDown(hgDialInfo: HGDialInfo) {
                if(actingDials) {
                    hgDialV2Small.cancelSpin()
                }
            }

            override fun onPointerDown(hgDialInfo: HGDialInfo) {}
            override fun onMove(hgDialInfo: HGDialInfo) {
                if(actingDials) {
                    //doRapidDial is a substitute for doManualObjectDial but uses less processor overhead
                    hgDialV2Small.doRapidDial(hgDialV2Big.fullTextureAngle * -2.392307692307692)
                    //Following line of code will do exactly the same as the above but use more processor overhead
                    //hgSmallCogViewContainer.getHgDialV2().doManualObjectDial(hgDialInfo.getTextureAngle() * -2.392307692307692d);
                }
            }

            override fun onPointerUp(hgDialInfo: HGDialInfo) {}
            override fun onUp(hgDialInfo: HGDialInfo) {}
        }
        ihgSmallDial = object : IHGDial {
            override fun onDown(hgDialInfo: HGDialInfo) {
                if(actingDials) {
                    hgDialV2Big.cancelSpin()
                }
            }

            override fun onPointerDown(hgDialInfo: HGDialInfo) {}
            override fun onMove(hgDialInfo: HGDialInfo) {
                if(actingDials) {
                    //doRapidDial is a substitute for doManualObjectDial but uses less processor overhead
                    hgDialV2Big.doRapidDial(hgDialV2Small.fullTextureAngle * -0.4180064308681672)
                    //Following line of code will do exactly the same as the above but use more processor overhead
                    //hgBigCogViewContainer.getHgDialV2().doManualObjectDial(hgDialInfo.getTextureAngle() * -0.4180064308681672d);
                }
            }

            override fun onPointerUp(hgDialInfo: HGDialInfo) {}
            override fun onUp(hgDialInfo: HGDialInfo) {
                if(actingDials) {

                    //Using this line of code is necessary to synch properly in cumulative rotate if you have used doRapidDial in the onMove
                    hgDialV2Big.doManualTextureDial(hgDialInfo.textureAngle * -0.4180064308681672)
                }
            }
        }
        chkActingDials.setOnClickListener {
            showActingDialsMessage(chkActingDials.isChecked)
        }
        persistBehaviours()
        return rootView
    }

    private fun showActingDialsMessage(actingDials: Boolean) {
        this.actingDials = actingDials
        activity?.intent?.putExtra("actingDials", actingDials)
        if(actingDials) {
            customToast.showCustomToast(getString(R.string.toast_acting_dials), Toast.LENGTH_LONG)
        }
        else if(!actingDials) {
            customToast.showCustomToast(getString(R.string.toast_no_acting_dials), Toast.LENGTH_LONG)
        }
    }

    private fun persistBehaviours() {
        hgDialV2Big.quickTapTime = 0
        hgDialV2Small.quickTapTime = 0

        //Enable/Disable fling-to-spin behaviour
        if(sp.getBoolean("enableFling", false)) {
            hgDialV2Big.setFlingTolerance(
                sp.getInt("flingDistance", 100),
                sp.getLong("flingTime", 200L)
            )
            hgDialV2Big.setSpinAnimation(
                sp.getFloat("spinStartSpeed", 0f),
                sp.getFloat("spinEndSpeed", 0f),
                sp.getLong("spinDuration", 8000L)
            )
            hgDialV2Big.slowFactor = sp.getFloat("slowFactor", 0f)
            hgDialV2Big.slowDown = sp.getBoolean("chkSlowDown", true)
            hgDialV2Big.flingAngleTolerance = sp.getFloat("flingAngle", 0f).toDouble()
            hgDialV2Small.setFlingTolerance(
                sp.getInt("flingDistance", 100),
                sp.getLong("flingTime", 200L)
            )
            hgDialV2Small.setSpinAnimation(
                sp.getFloat("spinStartSpeed", 0f),
                sp.getFloat("spinEndSpeed", 0f),
                sp.getLong("spinDuration", 8000L)
            )
            hgDialV2Small.slowFactor = sp.getFloat("slowFactor", 0f)
            hgDialV2Small.slowDown = sp.getBoolean("chkSlowDown", true)
            hgDialV2Small.flingAngleTolerance = sp.getFloat("flingAngle", 0f).toDouble()
        }
        else if (!sp.getBoolean("enableFling", false)) {
            hgDialV2Big.setFlingTolerance(0, 0L)
            hgDialV2Big.setSpinAnimation(0f, 0f, 0L)
            hgDialV2Big.slowFactor = 0f
            hgDialV2Big.slowDown = true
            hgDialV2Big.flingAngleTolerance = 0.0
            hgDialV2Small.setFlingTolerance(0, 0L)
            hgDialV2Small.setSpinAnimation(0f, 0f, 0L)
            hgDialV2Small.slowFactor = 0f
            hgDialV2Small.slowDown = true
            hgDialV2Small.flingAngleTolerance = 0.0
        }
    }

    override fun onShow(dialog: DialogInterface) {
        setMetrics(resources.configuration)
        if(hgStateBigCog != null && hgStateBigCog?.spinTriggered!!) {
            if(!hgStateBigCog?.slowDown!!) {
                hgDialV2Big.setSpinAnimation(
                    hgStateBigCog?.spinStartSpeed!!,
                    hgStateBigCog?.spinEndSpeed!!,
                    Long.MAX_VALUE
                )
            }
            else if(hgStateBigCog?.slowDown!!) {
                hgDialV2Big.setSpinAnimation(
                    hgStateBigCog?.spinStartSpeed!!,
                    hgStateBigCog?.spinEndSpeed!!,
                    hgStateBigCog?.spinDuration!!
                )
            }
            hgDialV2Big.triggerSpin(
                hgStateBigCog?.spinCurrentSpeed!!,
                0f,
                hgStateBigCog?.spinTimeRemaining!!,
                hgStateBigCog?.lastTextureDirection!!
            )
        }
        if(hgStateSmallCog != null && hgStateSmallCog?.spinTriggered!!) {
            if(!hgStateSmallCog?.slowDown!!) {
                hgDialV2Small.setSpinAnimation(
                    hgStateSmallCog?.spinStartSpeed!!,
                    hgStateSmallCog?.spinEndSpeed!!,
                    Long.MAX_VALUE
                )
            }
            else if(hgStateSmallCog?.slowDown!!) {
                hgDialV2Small.setSpinAnimation(
                    hgStateSmallCog?.spinStartSpeed!!,
                    hgStateSmallCog?.spinEndSpeed!!,
                    hgStateSmallCog?.spinDuration!!
                )
            }
            hgDialV2Small.triggerSpin(
                hgStateSmallCog?.spinCurrentSpeed!!,
                0f,
                hgStateSmallCog?.spinTimeRemaining!!,
                hgStateSmallCog?.lastTextureDirection!!
            )
        }
    }

    private fun setMetrics(newConfig: Configuration) {

        /*
		Large cog is 311 px, small is 130 px totalling: 441
		Large cog represents 70.52154195011338% and small represents 29.47845804988662%
		Both Diameters together = 236.5 px. (representing the diagonal distance that they must be apart)

		Though the HGGeometry class is protected by open source license you can get a free source copy here:
		http://stackoverflow.com/questions/622140/calculate-bounding-box-coordinates-from-a-rotated-rectangle/32663341#32663341
		*/
        val bcLp = hgBigCogV2FrameLayout.layoutParams as RelativeLayout.LayoutParams
        val scLp = hgSmallCogV2FrameLayout.layoutParams as RelativeLayout.LayoutParams
        parentDimens = Point((hgBigCogV2FrameLayout.parent as RelativeLayout).width, (hgBigCogV2FrameLayout.parent as RelativeLayout).height)
        val bigGocAngle = 0.877
        val smallGocAngle = 0.377
        if(newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            val bigCogLT = IntArray(2)
            val smallCogLT = IntArray(2)
            val newContainerLayoutWidth = (parentDimens.x.toDouble() * 0.8).toInt()
            val newBigCogDimen = (newContainerLayoutWidth.toDouble() * 0.7052154195011338).toInt()
            val newSmallCogDimen = (newContainerLayoutWidth.toDouble() * 0.2947845804988662).toInt()
            val lm = (parentDimens.x.toDouble() * 0.1).toInt()
            val tm = (parentDimens.y - newBigCogDimen) / 2 - newBigCogDimen / 2 + lm
            val collectiveWidth = newBigCogDimen + newSmallCogDimen
            val bigCogXRange: Int = (hgGeometry.getPointFromAngle(0.25, collectiveWidth.toDouble()) as Point).x - newBigCogDimen
            val smallCogXRange: Int = (hgGeometry.getPointFromAngle(0.25, collectiveWidth.toDouble()) as Point).x - newSmallCogDimen
            bigCogLT[0] = (hgGeometry.getPointFromAngle(bigGocAngle, bigCogXRange.toDouble() / 2) as Point).x + bigCogXRange / 2 + lm
            bigCogLT[1] = (hgGeometry.getPointFromAngle(bigGocAngle, bigCogXRange.toDouble() / 2) as Point).y + bigCogXRange / 2 + tm
            smallCogLT[0] = (hgGeometry.getPointFromAngle(smallGocAngle, smallCogXRange.toDouble() / 2) as Point).x + smallCogXRange / 2 + lm
            smallCogLT[1] = (hgGeometry.getPointFromAngle(smallGocAngle, smallCogXRange.toDouble() / 2) as Point).y + smallCogXRange / 2 + tm
            bcLp.width = newBigCogDimen
            bcLp.height = newBigCogDimen
            scLp.width = newSmallCogDimen
            scLp.height = newSmallCogDimen
            bcLp.setMargins(bigCogLT[0], bigCogLT[1], 0, 0)
            scLp.setMargins(smallCogLT[0], smallCogLT[1], 0, 0)
            hgBigCogV2FrameLayout.layoutParams = bcLp
            hgSmallCogV2FrameLayout.layoutParams = scLp
        }
        else if(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            val bigCogLT = IntArray(2)
            val smallCogLT = IntArray(2)
            val newContainerLayoutWidth = (parentDimens.y.toDouble() * 0.8).toInt()
            val newBigCogDimen = (newContainerLayoutWidth.toDouble() * 0.7052154195011338).toInt()
            val newSmallCogDimen = (newContainerLayoutWidth.toDouble() * 0.2947845804988662).toInt()
            val tm = (parentDimens.y.toDouble() * 0.1).toInt()
            val lm = (parentDimens.x - newBigCogDimen) / 2 - newBigCogDimen / 2 + tm
            val collectiveWidth = newBigCogDimen + newSmallCogDimen
            val bigCogXRange: Int = hgGeometry.getPointFromAngle(0.25, collectiveWidth.toDouble())!!.x - newBigCogDimen
            val smallCogXRange: Int = hgGeometry.getPointFromAngle(0.25, collectiveWidth.toDouble())!!.x - newSmallCogDimen
            bigCogLT[0] = hgGeometry.getPointFromAngle(bigGocAngle, bigCogXRange.toDouble() / 2)!!.x + bigCogXRange / 2 + lm
            bigCogLT[1] = hgGeometry.getPointFromAngle(bigGocAngle, bigCogXRange.toDouble() / 2)!!.y + bigCogXRange / 2 + tm
            smallCogLT[0] = hgGeometry.getPointFromAngle(smallGocAngle, smallCogXRange.toDouble() / 2)!!.x + smallCogXRange / 2 + lm
            smallCogLT[1] = hgGeometry.getPointFromAngle(smallGocAngle, smallCogXRange.toDouble() / 2)!!.y + smallCogXRange / 2 + tm
            bcLp.width = newBigCogDimen
            bcLp.height = newBigCogDimen
            scLp.width = newSmallCogDimen
            scLp.height = newSmallCogDimen
            bcLp.setMargins(bigCogLT[0], bigCogLT[1], 0, 0)
            scLp.setMargins(smallCogLT[0], smallCogLT[1], 0, 0)
            hgBigCogV2FrameLayout.layoutParams = bcLp
            hgSmallCogV2FrameLayout.layoutParams = scLp
        }
    }

    fun getActingDials(): Boolean {
        return this.actingDials
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        iCogDemoFragment = if (context is ICogDemoFragment) {
            context
        }
        else {
            throw RuntimeException("$context must implement ICogDemoFragment")
        }
    }

    override fun onStart() {
        super.onStart()
        hgDialV2Small.registerCallback(ihgSmallDial)
        hgDialV2Big.registerCallback(ihgBigDial)
    }

    override fun onStop() {
        super.onStop()
        hgDialV2Small.unRegisterCallback()
        hgDialV2Big.unRegisterCallback()
    }

    override fun onDetach() {
        super.onDetach()
        iCogDemoFragment = null
    }

    fun getHgStateBigCog(): HGState? {
        if(!hgDialV2Big.spinTriggered) {
            hgStateBigCog = null
        }
        else if(hgDialV2Big.spinTriggered) {
            hgStateSmallCog = null
            hgStateBigCog = hgDialV2Big.saveState("hgStateBigCog")
        }
        return hgStateBigCog
    }

    fun getHgStateSmallCog(): HGState? {
        if(!hgDialV2Small.spinTriggered) {
            hgStateSmallCog = null
        }
        else if(hgDialV2Small.spinTriggered) {
            hgStateBigCog = null
            hgStateSmallCog = hgDialV2Small.saveState("hgStateSmallCog")
        }
        return hgStateSmallCog
    }
}