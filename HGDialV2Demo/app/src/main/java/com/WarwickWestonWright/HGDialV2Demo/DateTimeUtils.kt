/*
Though this is an open source distribution only the files in the HGDialV2 package are protected by open source license.

All of the files in this package (HGDialV2Demo) are free source. Any developer may refactor/re-purpose or reuse the contents
of the files in the HGDialV2Demo package without any obligations to the developer of the HGDialV2 library. The files in this
package (HGDialV2Demo) are intended to give developer/users of the HGDialV2 library a head start in understanding how to use the
diverse, open usage HGDialV2 library.
*/
package com.WarwickWestonWright.HGDialV2Demo

import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.GregorianCalendar
import java.util.Locale

internal class DateTimeUtils {
    private val sdf: SimpleDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault())
    private var date: Date
    private val calendar: Calendar = GregorianCalendar(Locale.getDefault())
    private var yearVal = 0
    private var monthVal = 0
    private var dateVal = 0
    fun getDate(): String {
        return dateTime.split("T").toTypedArray()[0]
    }

    val time: String
        get() = dateTime.split("T").toTypedArray()[1]

    private val dateTime: String
        get() {
            date = Date()
            return sdf.format(date)
        }

    fun getDayOfWeek(year: Int, month: Int, date: Int): String {
        calendar[year, month - 1] = date
        when (calendar[Calendar.DAY_OF_WEEK]) {
            GregorianCalendar.SUNDAY -> {
                return "Sunday"
            }
            GregorianCalendar.MONDAY -> {
                return "Monday"
            }
            GregorianCalendar.TUESDAY -> {
                return "Tuesday"
            }
            GregorianCalendar.WEDNESDAY -> {
                return "Wednesday"
            }
            GregorianCalendar.THURSDAY -> {
                return "Thursday"
            }
            GregorianCalendar.FRIDAY -> {
                return "Friday"
            }
            GregorianCalendar.SATURDAY -> {
                return "Saturday"
            }
            else -> return "Error"
        }
    }

    fun isRealDatePart(datePart: String): Boolean {
        var datePartLocal = datePart
        datePartLocal = datePartLocal.replace("\\s".toRegex(), "")
        val timeParts = datePartLocal.split("-").toTypedArray()
        if(timeParts.size != 3) {
            return false
        }
        else {
            try {
                yearVal = timeParts[0].toInt()
                monthVal = timeParts[1].toInt()
                dateVal = timeParts[2].toInt()
            }
            catch(e: NumberFormatException) {
                return false
            }
            if(dateVal < 1) {
                return false
            }
            if(monthVal > 12 || monthVal < 1) {
                return false
            }
            if(monthVal == 4 || monthVal == 6 || monthVal == 9 || monthVal == 11) {
                if (dateVal > 30) {
                    return false
                }
            }
            if(monthVal == 2) {
                if (yearVal % 4 != 0) {
                    if (dateVal > 28) {
                        return false
                    }
                }
                else if (yearVal % 4 == 0) {
                    if (dateVal > 29) {
                        return false
                    }
                }
            }
            if(dateVal > 31) {
                return false
            }
        }
        return true
    }

    val yearPartsYYYYMMDD: IntArray
        get() = intArrayOf(yearVal, monthVal, dateVal)

    val currentTime: IntArray
        get() {
            val timeParts = time.split(":").toTypedArray()
            return intArrayOf(
                timeParts[0].toInt(),
                timeParts[1].toInt(),
                timeParts[2].toInt()
            )
        }

    init {
        date = Date()
    }
}