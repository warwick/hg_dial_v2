/*
Though this is an open source distribution only the files in the HGDialV2 package are protected by open source license.

All of the files in this package (HGDialV2Demo) are free source. Any developer may refactor/re-purpose or reuse the contents
of the files in the HGDialV2Demo package without any obligations to the developer of the HGDialV2 library. The files in this
package (HGDialV2Demo) are intended to give developer/users of the HGDialV2 library a head start in understanding how to use the
diverse, open usage HGDialV2 library.
*/
package com.WarwickWestonWright.HGDialV2Demo

import android.content.Context
import android.content.SharedPreferences
import android.content.res.Configuration
import android.graphics.Point
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.WarwickWestonWright.HGDialV2.HGState
import java.lang.ref.WeakReference

class MainActivity : AppCompatActivity(),
    MainDemoFragment.IMainDemoFragment,
    CogDemoFragment.ICogDemoFragment,
    TimePickerFragment.ITimePickerFragment,
    FastListHolderFragment.IFastListFragment,
    DatePickerFragment.IDatePickerFragment,
    Runnable {

    private val weakRef: WeakReference<MainActivity> = WeakReference(this)
    private val mainActivityHandler = MainActivityHandler(weakRef)
    private var thread: Thread? = Thread(this)
    private var theSecret: String? = ""
    private var mainDemoFragment: MainDemoFragment? = null
    private var cogDemoFragment: CogDemoFragment? = null
    private var mainSettingsFragment: MainSettingsFragment? = null
    private var timePickerFragment: TimePickerFragment? = null
    private var datePickerFragment: DatePickerFragment? = null
    private var fastListHolderFragment: FastListHolderFragment? = null
    private var textSelectFragment: TextSelectFragment? = null
    private var aboutDialogFragment: AboutDialogFragment? = null
    private lateinit var btnOpenMainDemo: Button
    private lateinit var btnOpenCogDemo: Button
    private lateinit var btnSettings: Button
    private lateinit var btnOpenTimeFragment: Button
    private lateinit var btnOpenDateFragment: Button
    private lateinit var btnOpenFastListFragment: Button
    private lateinit var btnOpenTextSelector: Button
    private lateinit var btnAbout: Button
    private lateinit var sp: SharedPreferences
    //Saves/Restores State for all settings
    private var hgStateMainDemo: HGState? = null
    //Saves/Restores State for all settings plus is used to demonstrate restoring of  spin state in case you need to nullify a fragment
    private var hgStateBigCog: HGState? = null
    //Saves/Restores State for all settings plus is used to demonstrate restoring of  spin state in case you need to nullify a fragment
    private var hgStateSmallCog: HGState? = null
    //Same as CogDemo above but shows that it works with dialog fragments.
    private var hgStateDatePickerDemo: HGState? = null
    private val screenDimens: Point = Point(0, 0)
    private var hgFlagFullScreen: HGFlagFullScreen? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        if(hgFlagFullScreen == null) {
            hgFlagFullScreen = HGFlagFullScreen(window)
        }
        hgFlagFullScreen?.flagFullScreen()

        screenDimens.x = resources.displayMetrics.widthPixels
        screenDimens.y = resources.displayMetrics.heightPixels
        sp = getSharedPreferences(getString(R.string.shared_pref_filename), Context.MODE_PRIVATE)
        btnOpenMainDemo = findViewById(R.id.btnOpenMainDemo)
        btnOpenCogDemo = findViewById(R.id.btnOpenCogDemo)
        btnSettings = findViewById(R.id.btnSettings)
        btnOpenTimeFragment = findViewById(R.id.btnOpenTimeFragment)
        btnOpenDateFragment = findViewById(R.id.btnOpenDateFragment)
        btnOpenFastListFragment = findViewById(R.id.btnOpenFastListFragment)
        btnOpenTextSelector = findViewById(R.id.btnOpenTextSelector)
        btnAbout = findViewById(R.id.btnAbout)

        if(!sp.getBoolean("arePrefsSetup", false)) {
            sp.edit().putBoolean("usePrecision", false).apply()
            sp.edit().putFloat("precision", 0.5f).apply()
            sp.edit().putBoolean("useVariableDial", false).apply()
            sp.edit().putFloat("innerPrecision", 5.2f).apply()
            sp.edit().putFloat("outerPrecision", 0.2f).apply()
            sp.edit().putBoolean("useVariableCurve", false).apply()
            sp.edit().putBoolean("usePositiveCurve", false).apply()
            sp.edit().putBoolean("cumulativeDial", true).apply()
            sp.edit().putBoolean("isSingleFinger", true).apply()
            sp.edit().putBoolean("useAngleSnap", false).apply()
            sp.edit().putFloat("angleSnap", 0.125f).apply()
            sp.edit().putFloat("snapProximity", 0.03125f).apply()
            sp.edit().putBoolean("useMinMax", false).apply()
            sp.edit().putFloat("minRotation", -2.45f).apply()
            sp.edit().putFloat("maxRotation", 2.88f).apply()
            sp.edit().putBoolean("enableFling", false).apply()
            sp.edit().putInt("flingDistance", 150).apply()
            sp.edit().putLong("flingTime", 200L).apply()
            sp.edit().putFloat("spinStartSpeed", 0f).apply()
            sp.edit().putFloat("spinEndSpeed", 0f).apply()
            sp.edit().putFloat("slowFactor", 0f).apply()
            sp.edit().putLong("spinDuration", 8000L).apply()
            sp.edit().putBoolean("chkSlowDown", true).apply()
            sp.edit().putFloat("flingAngle", 0f).apply()
            sp.edit().putBoolean("enableQuickTap", true).apply()
            sp.edit().putLong("quickTapTime", 120L).apply()

            //FastList Prefs
            sp.edit().putInt("listLength", 1000).apply()
            sp.edit().putBoolean("arePrefsSetup", true).apply()
        }

        btnOpenMainDemo.setOnClickListener {
            mainDemoFragment = null
            mainDemoFragment = MainDemoFragment()
            intent?.putExtra("hgStateMainDemo", hgStateMainDemo)
            mainDemoFragment?.show(supportFragmentManager, "MainDemoFragment")
        }

        btnOpenCogDemo.setOnClickListener {
            intent?.putExtra("hgStateBigCog", hgStateBigCog)
            intent?.putExtra("hgStateSmallCog", hgStateSmallCog)
            cogDemoFragment = null
            cogDemoFragment = CogDemoFragment()
            cogDemoFragment?.show(supportFragmentManager, "CogDemoFragment")
        }

        btnSettings.setOnClickListener {
            val bundle = Bundle()
            mainSettingsFragment = null
            mainSettingsFragment = MainSettingsFragment()
            mainSettingsFragment?.arguments = bundle
            mainSettingsFragment?.show(supportFragmentManager, "MainSettingsFragment")
        }

        btnOpenTimeFragment.setOnClickListener {
            timePickerFragment = null
            timePickerFragment = TimePickerFragment()
            timePickerFragment?.show(supportFragmentManager, "TimePickerFragment")
        }

        btnOpenDateFragment.setOnClickListener {
            intent.putExtra("hgStateDatePickerDemo", hgStateDatePickerDemo)
            datePickerFragment = null
            datePickerFragment = DatePickerFragment()
            datePickerFragment?.show(supportFragmentManager, "DatePickerFragment")
        }

        btnOpenFastListFragment.setOnClickListener {
            intent.putExtra("listLength", sp.getInt("listLength", 1000))
            intent.putExtra("listLength", sp.getInt("listLength", 1000))
            fastListHolderFragment = null
            fastListHolderFragment = FastListHolderFragment()
            fastListHolderFragment?.show(supportFragmentManager, "FastListHolderFragment")
        }

        btnOpenTextSelector.setOnClickListener {
            if(thread == null) { thread = Thread(this) }
            thread?.start()
        }

        btnAbout.setOnClickListener {
            aboutDialogFragment = null
            aboutDialogFragment = AboutDialogFragment()
            aboutDialogFragment?.show(supportFragmentManager, "AboutDialogFragment")
        }
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        if(hgFlagFullScreen == null) {hgFlagFullScreen = HGFlagFullScreen(window)}
        hgFlagFullScreen?.flagFullScreen()

        screenDimens.x = resources.displayMetrics.widthPixels
        screenDimens.y = resources.displayMetrics.heightPixels

        if(cogDemoFragment != null && cogDemoFragment?.isAdded!!) {
            hgStateBigCog = cogDemoFragment?.getHgStateBigCog()
            hgStateSmallCog = cogDemoFragment?.getHgStateSmallCog()
            intent.putExtra("actingDials", cogDemoFragment?.getActingDials())
            cogDemoFragment?.dismiss()
            cogDemoFragment = null
            cogDemoFragment = CogDemoFragment()
            intent.putExtra("hgStateBigCog", hgStateBigCog)
            intent.putExtra("hgStateSmallCog", hgStateSmallCog)
            cogDemoFragment?.show(supportFragmentManager, "CogDemoFragment")
        }
        else if(datePickerFragment != null && datePickerFragment?.dialog != null && datePickerFragment?.dialog?.isShowing!!) {
            hgStateDatePickerDemo = datePickerFragment?.getHgStateDatePickerDemo()
            datePickerFragment?.dismiss()
            datePickerFragment = null
            datePickerFragment = DatePickerFragment()
            intent?.putExtra("hgStateDatePickerDemo", hgStateDatePickerDemo)
            datePickerFragment?.show(supportFragmentManager, "DatePickerFragment")
        }
    }

    override fun mainDemoFragmentCallback(hgState: HGState?, dismissDialog: Boolean) {
        if(dismissDialog) {
            if(hgFlagFullScreen == null) {hgFlagFullScreen = HGFlagFullScreen(window)}
            hgFlagFullScreen?.flagFullScreen()
            mainDemoFragment?.dismiss()
        }
        else {
            //Restores state after opening settings fragment from main demo fragment
            this.hgStateMainDemo = hgState
            intent.putExtra("hgStateMainDemo", hgState)
        }
    }

    override fun cogDemoFragmentCallback() {
        if(hgFlagFullScreen == null) {hgFlagFullScreen = HGFlagFullScreen(window)}
        hgFlagFullScreen?.flagFullScreen()
        hgStateBigCog = null
        hgStateSmallCog = null
        cogDemoFragment?.dismiss()
        cogDemoFragment = null
    }

    override fun timePickerFragmentCallback() {
        if(hgFlagFullScreen == null) {hgFlagFullScreen = HGFlagFullScreen(window)}
        hgFlagFullScreen?.flagFullScreen()
        timePickerFragment?.dismiss()
        timePickerFragment = null
    }

    override fun datePickerFragmentCallback() {
        if(hgFlagFullScreen == null) {hgFlagFullScreen = HGFlagFullScreen(window)}
        hgFlagFullScreen?.flagFullScreen()
        datePickerFragment?.dismiss()
        datePickerFragment = null
        hgStateDatePickerDemo = null
    }

    override fun fastListFragmentCallback() {
        if(hgFlagFullScreen == null) {hgFlagFullScreen = HGFlagFullScreen(window)}
        hgFlagFullScreen?.flagFullScreen()
        fastListHolderFragment?.dismiss()
        fastListHolderFragment = null
    }

    override fun onDestroy() {
        super.onDestroy()
        mainDemoFragment = null
        cogDemoFragment = null
        mainSettingsFragment = null
        timePickerFragment = null
        datePickerFragment = null
        fastListHolderFragment = null
        hgStateMainDemo = null
        screenDimens.x = 0
        screenDimens.y = 0
    }

    override fun run() {
        getTheSecret()
    }

    private fun getTheSecret() {
        val assetFileHandler = AssetFileHandler()
        theSecret = assetFileHandler.getAssetText(this, "the_secret_to_discernment.html")
        mainActivityHandler.sendEmptyMessage(0)
    }

    private fun showTextSelectFragment(theSecret: String?) {
        textSelectFragment = TextSelectFragment()
        val bundle = Bundle()
        bundle.putString("theSecret", theSecret)
        textSelectFragment?.arguments = bundle
        textSelectFragment?.show(supportFragmentManager, "TextSelectFragment")
    }

    class MainActivityHandler(private val weakRef: WeakReference<MainActivity>): Handler() {
        override fun handleMessage(msg: Message) {
            val ref = weakRef.get() as MainActivity
            ref.thread = null
            ref.showTextSelectFragment(ref.theSecret)
        }
    }
}