/*
Though this is an open source distribution only the files in the HGDialV2 package are protected by open source license.

All of the files in this package (HGDialV2Demo) are free source. Any developer may refactor/re-purpose or reuse the contents
of the files in the HGDialV2Demo package without any obligations to the developer of the HGDialV2 library. The files in this
package (HGDialV2Demo) are intended to give developer/users of the HGDialV2 library a head start in understanding how to use the
diverse, open usage HGDialV2 library.
*/
package com.WarwickWestonWright.HGDialV2Demo

import android.content.Context
import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.WarwickWestonWright.HGDialV2Demo.DummyContent.DummyContent

class FastListFragment : Fragment() {
    interface IFastListFragment {
        fun itemFragmentCallback(
            fastListFragment: FastListFragment?,
            dummyContent: DummyContent?
        )
    }

    private var columnCount = 2
    private var iFastListFragment: IFastListFragment? = null
    private lateinit var rootView: View
    private lateinit var dummyContents: MutableList<DummyContent>
    lateinit var recyclerView: RecyclerView
        private set
    private var listLength = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if(iFastListFragment == null) {
            iFastListFragment = activity?.supportFragmentManager?.findFragmentByTag("FastListHolderFragment") as FastListHolderFragment?
        }
        //Build Dummy List
        listLength = activity?.intent?.getIntExtra("listLength", 1000)!!
        dummyContents = ArrayList()
        dummyContents.clear()
        for(i in 0 until listLength) {
            dummyContents.add(DummyContent(i, i.toString(), "Contents of: $i"))
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        rootView = inflater.inflate(R.layout.fragment_list_fragment, container, false)
        if(rootView is RecyclerView) {
            recyclerView = rootView as RecyclerView
            changeGridLayout(activity?.resources?.configuration!!)
        }
        iFastListFragment?.itemFragmentCallback(this, null)
        return rootView
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        changeGridLayout(newConfig)
    }

    fun refreshList(newConfig: Configuration, listLength: Int) {
        dummyContents.clear()
        recyclerView.removeAllViews()
        for(i in 0 until listLength) {
            dummyContents.add(DummyContent(i, i.toString(), "Contents of: $i"))
        }
        changeGridLayout(newConfig)
    }

    private fun changeGridLayout(newConfig: Configuration) {
        if(newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            columnCount = 1
        }
        else if(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            columnCount = 2
        }
        setupRecycleView()
    }

    private fun setupRecycleView() {
        if(columnCount < 2) {
            recyclerView.layoutManager = LSmoothScroller(activity)
        }
        else {
            recyclerView.layoutManager = GridLayoutManager(activity, columnCount)
        }
        recyclerView.adapter = FastListAdapter(dummyContents, iFastListFragment)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        iFastListFragment = activity?.supportFragmentManager?.findFragmentByTag("FastListHolderFragment") as FastListHolderFragment
    }

    override fun onDetach() {
        super.onDetach()
        iFastListFragment = null
    }
}