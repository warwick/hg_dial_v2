/*
Though this is an open source distribution only the files in the HGDialV2 package are protected by open source license.

All of the files in this package (HGDialV2Demo) are free source. Any developer may refactor/re-purpose or reuse the contents
of the files in the HGDialV2Demo package without any obligations to the developer of the HGDialV2 library. The files in this
package (HGDialV2Demo) are intended to give developer/users of the HGDialV2 library a head start in understanding how to use the
diverse, open usage HGDialV2 library.
*/
package com.WarwickWestonWright.HGDialV2Demo

import android.app.Dialog
import android.content.Context
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import com.WarwickWestonWright.HGDialV2.HGDialInfo
import com.WarwickWestonWright.HGDialV2.HGDialV2
import com.WarwickWestonWright.HGDialV2.HGDialV2.IHGDial
import com.WarwickWestonWright.HGDialV2.HGState
import com.WarwickWestonWright.HGDialV2.HGViewContainer
import java.lang.ref.WeakReference
import java.util.Formatter
import java.util.Locale
import kotlin.math.abs

class TimePickerFragment : DialogFragment(),
    IHGDial,
    Runnable {
    interface ITimePickerFragment {
        fun timePickerFragmentCallback()
    }

    private val timePickerFragmentRef: WeakReference<TimePickerFragment> = WeakReference(this)
    private val keepTimeHandler = KeepTimeHandler(timePickerFragmentRef)
    private var iTimePickerFragment: ITimePickerFragment? = null
    private lateinit var rootView: View
    private lateinit var btnSetTime: Button
    private lateinit var txtTime: EditText
    private lateinit var lblClockDemoTitle: TextView
    private lateinit var btnCloseTimeDemo: Button
    private lateinit var chkKeepTime: CheckBox
    private lateinit var btnHour: Button
    private lateinit var btnMinute: Button
    private lateinit var btnSecond: Button
    private lateinit var fLayoutHgClockContainer: FrameLayout
    private lateinit var hgViewContainer: HGViewContainer
    private lateinit var dateTimeUtils: DateTimeUtils
    private val clockDrawables = IntArray(3)
    private var currentHourAngle = 0.0
    private var currentMinuteAngle = 0.0
    private var currentSecondAngle = 0.0
    private lateinit var hgDialV2Hour: HGDialV2
    private lateinit var hgDialV2Minute: HGDialV2
    private lateinit var hgDialV2Second: HGDialV2
    private var keepTime = false
    private var timeKeeper: Thread? = null
    private lateinit var formatter: Formatter
    private lateinit var sp: SharedPreferences
    private lateinit var hgState: HGState
    private var spinStartTime: Long = 0
    private var changedDuringSpin = false
    private var spinEndTime: Long = 0
    private var spinStartSpeed = 0f
    private var lastTextureDirection = 0
    private lateinit var customToast: CustomToast
    private lateinit var defaultLocale: Locale
    private val KEEP_TIME = 0
    private val TIME_STOP = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if(iTimePickerFragment == null) {
            iTimePickerFragment = activity as ITimePickerFragment
        }
        sp = activity?.getSharedPreferences(getString(R.string.shared_pref_filename), Context.MODE_PRIVATE)!!
        customToast = CustomToast(activity as MainActivity)
        defaultLocale = Locale.getDefault()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        rootView = inflater.inflate(R.layout.time_picker_fragment, container, false)
        btnSetTime = rootView.findViewById(R.id.btnSetTime)
        txtTime = rootView.findViewById(R.id.txtTime)
        lblClockDemoTitle = rootView.findViewById(R.id.lblClockDemoTitle)
        btnCloseTimeDemo = rootView.findViewById(R.id.btnCloseTimeDemo)
        chkKeepTime = rootView.findViewById(R.id.chkKeepTime)
        btnHour = rootView.findViewById(R.id.btnHour)
        btnMinute = rootView.findViewById(R.id.btnMinute)
        btnSecond = rootView.findViewById(R.id.btnSecond)
        fLayoutHgClockContainer = rootView.findViewById(R.id.fLayoutHgClockContainer)
        clockDrawables[0] = R.drawable.hg_dial_hour_hand
        clockDrawables[1] = R.drawable.hg_dial_minute_hand
        clockDrawables[2] = R.drawable.hg_dial_second_hand
        dateTimeUtils = DateTimeUtils()
        txtTime.setText(dateTimeUtils.time)
        hgViewContainer = HGViewContainer(clockDrawables, fLayoutHgClockContainer)
        hgViewContainer.activeDial = HOUR_HAND

        //These three objects don't need to be assigned they are just here to save long lines of code and illustrate class usage
        hgDialV2Hour = hgViewContainer.hgDialV2s?.get(HOUR_HAND)!!
        hgDialV2Minute = hgViewContainer.hgDialV2s?.get(MINUTE_HAND)!!
        hgDialV2Second = hgViewContainer.hgDialV2s?.get(SECOND_HAND)!!
        btnSetTime.setOnClickListener {
            if(!setTime(txtTime.text.toString())) {
                customToast.showCustomToast(getString(R.string.toast_invalid_time), Toast.LENGTH_LONG)
            }
            else  /* if(setTime(txtTime.getText().toString()) == true) */ {
                hgViewContainer.hgDialV2Active?.cancelSpin()
            }
        }
        btnCloseTimeDemo.setOnClickListener { iTimePickerFragment?.timePickerFragmentCallback() }
        chkKeepTime.setOnClickListener {
            keepTime = chkKeepTime.isChecked
            if(chkKeepTime.isChecked) {
                hgViewContainer.hgDialV2Active?.cancelSpin()
                updateUITime()
                timeKeeper = Thread(this)
                timeKeeper?.start()
            }
        }
        btnHour.setOnClickListener { setActiveClockHand(HOUR_HAND) }
        btnMinute.setOnClickListener { setActiveClockHand(MINUTE_HAND) }
        btnSecond.setOnClickListener { setActiveClockHand(SECOND_HAND) }
        persistBehaviours()
        return rootView
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = Dialog(activity as MainActivity)
        val root = RelativeLayout(activity)
        root.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(root)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.BLACK))
        dialog.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        dialog.setOnKeyListener { dialog, keyCode, event ->
            if(event.action == MotionEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                iTimePickerFragment?.timePickerFragmentCallback()
                true
            }
            else {
                false
            }
        }
        return dialog
    }

    private fun setActiveClockHand(whatHand: Int) {
        changedDuringSpin = true
        if(!hgViewContainer.hgDialV2Active.slowDown) {
            hgViewContainer.activeDial = whatHand
            hgViewContainer.hgDialV2Active.setSpinAnimation(hgViewContainer.hgDialV2Active?.spinStartSpeed!!, 0f, Long.MAX_VALUE)
            hgViewContainer.hgDialV2Active.triggerSpin(spinStartSpeed, 0F, Long.MAX_VALUE, lastTextureDirection)
        }
        else if(hgViewContainer.hgDialV2Active.slowDown) {
            if(spinEndTime - System.currentTimeMillis() > 0) {
                hgState = hgViewContainer.hgDialV2Active.saveState()
                val spinCurrentSpeed: Float = hgState.spinCurrentSpeed
                hgViewContainer.hgDialV2Active.cancelSpin()
                hgViewContainer.activeDial = whatHand
                if (spinCurrentSpeed != null) {
                    hgViewContainer.hgDialV2Active.triggerSpin(spinCurrentSpeed, 0F, spinEndTime - System.currentTimeMillis(), lastTextureDirection)
                }
            }
            else {
                hgViewContainer.activeDial = whatHand
                hgState = hgViewContainer.hgDialV2Active?.saveStateFlush()!!
                spinEndTime = 0L
                spinStartTime = 0L
            }
        }
        setButtonStyles(whatHand)
    }

    private fun updateUITime() {
        val timeParts: IntArray = dateTimeUtils.currentTime
        timeParts[0] = timeParts[0] % 12
        formatter = Formatter(defaultLocale)
        lblClockDemoTitle.text = formatter.format(
            "%1$02d:%2$02d:%3$02d",
            timeParts[0],
            timeParts[1],
            timeParts[2]
        ).toString()
        hgDialV2Hour.doManualTextureDial(timeParts[0] / 12.0 + timeParts[1] / 720.0 + timeParts[2] / 43200.0)
        hgDialV2Minute.doManualTextureDial(
            (hgDialV2Hour.fullTextureAngle * MINUTE_ROTATIONS_PER_12_HOURS).toInt() + timeParts[1] / 60.0 + timeParts[2] / 3600.0
        )
        hgDialV2Second.doManualTextureDial((hgDialV2Hour.fullTextureAngle * SECOND_ROTATIONS_PER_12_HOURS).toInt() + timeParts[2] / 60.0)
    }

    private fun setTime(time: String): Boolean {
        time.replace("\\s".toRegex(), "")
        val timePartsStr = time.split(":").toTypedArray()
        if(timePartsStr.size != 3) {
            return false
        }
        val timePartsInt = IntArray(3)
        try {
            timePartsInt[0] = timePartsStr[0].toInt()
            timePartsInt[1] = timePartsStr[1].toInt()
            timePartsInt[2] = timePartsStr[2].toInt()
            if(timePartsInt[0] < 0 || timePartsInt[0] > 23) {
                return false
            }
            if(timePartsInt[1] < 0 || timePartsInt[1] > 59) {
                return false
            }
            if(timePartsInt[2] < 0 || timePartsInt[2] > 59) {
                return false
            }
        }
        catch (_: NumberFormatException) { }
        hgDialV2Hour.doManualTextureDial(timePartsInt[0] / 12.0 + timePartsInt[1] / 720.0 + timePartsInt[2] / 43200.0)
        hgDialV2Minute.doManualTextureDial((hgDialV2Hour.fullTextureAngle * MINUTE_ROTATIONS_PER_12_HOURS).toInt() + timePartsInt[1] / 60.0 + timePartsInt[2] / 3600.0)
        hgDialV2Second.doManualTextureDial((hgDialV2Hour.fullTextureAngle * SECOND_ROTATIONS_PER_12_HOURS).toInt() + timePartsInt[2] / 60.0)
        return true
    }

    private fun pickTime(hourAngle: Double, minuteAngle: Double, secondAngle: Double): String {
        val timeParts = IntArray(3)
        if(hourAngle < 0) {
            timeParts[0] = abs(hourAngle * 12 % 12 + 12).toInt()
            timeParts[1] = abs(minuteAngle * 60 % 60 + 60).toInt()
            timeParts[2] = abs(secondAngle * 60 % 60 + 60).toInt()
        }
        else {
            timeParts[0] = (hourAngle * 12).toInt() % 12
            timeParts[1] = (minuteAngle * 60).toInt() % 60
            timeParts[2] = (secondAngle * 60).toInt() % 60
        }
        formatter = Formatter(defaultLocale)
        return formatter.format(
            "%1$02d:%2$02d:%3$02d",
            timeParts[0],
            timeParts[1],
            timeParts[2]
        ).toString()
    }

    private fun setButtonStyles(activeDial: Int) {
        when (activeDial) {
            0 -> {
                btnHour.setBackgroundResource(R.drawable.active_date_time_btn_bg)
                btnMinute.setBackgroundResource(R.drawable.inactive_date_time_btn_bg)
                btnSecond.setBackgroundResource(R.drawable.inactive_date_time_btn_bg)
            }
            1 -> {
                btnHour.setBackgroundResource(R.drawable.inactive_date_time_btn_bg)
                btnMinute.setBackgroundResource(R.drawable.active_date_time_btn_bg)
                btnSecond.setBackgroundResource(R.drawable.inactive_date_time_btn_bg)
            }
            2 -> {
                btnHour.setBackgroundResource(R.drawable.inactive_date_time_btn_bg)
                btnMinute.setBackgroundResource(R.drawable.inactive_date_time_btn_bg)
                btnSecond.setBackgroundResource(R.drawable.active_date_time_btn_bg)
            }
        }
    }

    override fun onDown(hgDialInfo: HGDialInfo?) {
        chkKeepTime.isChecked = false
        keepTime = false
        changedDuringSpin = false
        spinEndTime = 0L
        spinStartTime = 0L
    }

    override fun onPointerDown(hgDialInfo: HGDialInfo?) {
        keepTimeHandler.sendEmptyMessage(TIME_STOP)
    }
    override fun onMove(hgDialInfo: HGDialInfo?) {
        when(hgViewContainer.activeDial) {
            HOUR_HAND -> {
                hgDialV2Minute.doRapidDial(hgDialV2Hour.fullTextureAngle * MINUTE_ROTATIONS_PER_12_HOURS)
                hgDialV2Second.doRapidDial(hgDialV2Hour.fullTextureAngle * SECOND_ROTATIONS_PER_12_HOURS)
            }
            MINUTE_HAND -> {
                hgDialV2Hour.doRapidDial(hgDialV2Minute.fullTextureAngle * HOURS_ROTATIONS_PER_MINUTE)
                hgDialV2Second.doRapidDial(hgDialV2Minute.fullTextureAngle * SECOND_ROTATIONS_PER_MINUTE)
            }
            SECOND_HAND -> {
                hgDialV2Minute.doRapidDial(hgDialV2Second.fullTextureAngle * MINUTE_ROTATIONS_PER_SECOND)
                hgDialV2Hour.doRapidDial(hgDialV2Second.fullTextureAngle * HOUR_ROTATIONS_PER_SECOND)
            }
        }
        lblClockDemoTitle.text = pickTime(
            hgDialV2Hour.fullTextureAngle,
            hgDialV2Minute.fullTextureAngle,
            hgDialV2Second.fullTextureAngle
        )
    }

    override fun onPointerUp(hgDialInfo: HGDialInfo?) {}
    override fun onUp(hgDialInfo: HGDialInfo) {
        if(!changedDuringSpin) {
            if(hgDialInfo.isSpinTriggered) {
                spinStartSpeed = hgDialInfo.spinCurrentSpeed
                spinStartTime = System.currentTimeMillis()
                spinEndTime = if (sp.getFloat("slowFactor", 0f) > 0) {
                    spinStartTime + (hgDialInfo.spinCurrentSpeed * sp.getFloat("slowFactor", 0f) * 1000f).toLong()
                }
                else {
                    spinStartTime + sp.getLong("spinDuration", 8000L)
                }
                lastTextureDirection = hgViewContainer.hgDialV2Active?.lastTextureDirection!!
            }
            else {
                spinStartTime = 0L
                spinEndTime = 0L
                changedDuringSpin = false
            }
        }
        if(hgDialInfo.isSpinTriggered) {
            keepTime = false
        }

        //Keep the numbers low to save processor overhead
        currentHourAngle = hgDialV2Hour.fullTextureAngle % 1
        currentMinuteAngle = hgDialV2Minute.fullTextureAngle % 12
        currentSecondAngle = hgDialV2Second.fullTextureAngle % 720

        //Fault tolerance to keep the angles in synch
        hgDialV2Hour.doManualTextureDial(currentHourAngle)
        hgDialV2Minute.doManualTextureDial(currentMinuteAngle)
        hgDialV2Second.doManualTextureDial(currentSecondAngle)
        if(!hgDialInfo.isSpinTriggered) {
            //Display the picked time
            val time = pickTime(currentHourAngle, currentMinuteAngle, currentSecondAngle)
            customToast.showCustomToast(getString(R.string.toast_picked_time) + time, Toast.LENGTH_LONG)
        }
    }

    private fun persistBehaviours() {
        //Enable/Disable fling-to-spin behaviour
        if(sp.getBoolean("enableFling", false)) {
            hgDialV2Hour.setFlingTolerance(
                sp.getInt("flingDistance", 150),
                sp.getLong("flingTime", 200L)
            )
            hgDialV2Hour.setSpinAnimation(
                sp.getFloat("spinStartSpeed", 0f),
                sp.getFloat("spinEndSpeed", 0f),
                sp.getLong("spinDuration", 8000L)
            )
            hgDialV2Hour.slowFactor = sp.getFloat("slowFactor", 0f)
            hgDialV2Hour.slowDown = sp.getBoolean("chkSlowDown", true)
            hgDialV2Hour.flingAngleTolerance = sp.getFloat("flingAngle", 0f).toDouble()
            hgDialV2Minute.setFlingTolerance(
                sp.getInt("flingDistance", 150),
                sp.getLong("flingTime", 200L)
            )
            hgDialV2Minute.setSpinAnimation(
                sp.getFloat("spinStartSpeed", 0f),
                sp.getFloat("spinEndSpeed", 0f),
                sp.getLong("spinDuration", 8000L)
            )
            hgDialV2Minute.slowFactor = sp.getFloat("slowFactor", 0f)
            hgDialV2Minute.slowDown = sp.getBoolean("chkSlowDown", true)
            hgDialV2Minute.flingAngleTolerance = sp.getFloat("flingAngle", 0f).toDouble()
            hgDialV2Second.setFlingTolerance(sp.getInt("flingDistance", 150), sp.getLong("flingTime", 200L))
            hgDialV2Second.setSpinAnimation(
                sp.getFloat("spinStartSpeed", 0f),
                sp.getFloat("spinEndSpeed", 0f),
                sp.getLong("spinDuration", 8000L)
            )
            hgDialV2Second.slowFactor = sp.getFloat("slowFactor", 0f)
            hgDialV2Second.slowDown = sp.getBoolean("chkSlowDown", true)
            hgDialV2Second.flingAngleTolerance = sp.getFloat("flingAngle", 0f).toDouble()
        }
        else if(!sp.getBoolean("enableFling", false)) {
            hgDialV2Hour.setFlingTolerance(0, 0L)
            hgDialV2Hour.setSpinAnimation(0f, 0f, 0L)
            hgDialV2Hour.slowFactor = 0f
            hgDialV2Hour.slowDown = true
            hgDialV2Hour.flingAngleTolerance = 0.0
            hgDialV2Minute.setFlingTolerance(0, 0L)
            hgDialV2Minute.setSpinAnimation(0f, 0f, 0L)
            hgDialV2Minute.slowFactor = 0f
            hgDialV2Minute.slowDown = true
            hgDialV2Minute.flingAngleTolerance = 0.0
            hgDialV2Second.setFlingTolerance(0, 0L)
            hgDialV2Second.setSpinAnimation(0f, 0f, 0L)
            hgDialV2Second.slowFactor = 0f
            hgDialV2Second.slowDown = true
            hgDialV2Second.flingAngleTolerance = 0.0
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        iTimePickerFragment = if (context is ITimePickerFragment) {
            context
        }
        else {
            throw RuntimeException("$context must implement ITimePickerFragment")
        }
    }

    override fun onStart() {
        super.onStart()
        hgViewContainer.registerCallback(this)
    }

    override fun onStop() {
        super.onStop()
        hgViewContainer.cancelSpinThread()
        keepTime = false
        if(timeKeeper != null) {
            keepTimeHandler.sendEmptyMessage(TIME_STOP)
        }
        hgViewContainer.hgDialV2Active.unRegisterCallback()
    }

    override fun onDetach() {
        super.onDetach()
        iTimePickerFragment = null
    }

    override fun run() {
        keepTime()
    }

    private fun keepTime() {
        while(keepTime) {
            val interval = System.currentTimeMillis() + 1000L
            while(System.currentTimeMillis() < interval && keepTime) {}
            keepTimeHandler.sendEmptyMessage(KEEP_TIME)
        }
    }

    private class KeepTimeHandler(private val timePickerFragmentWR: WeakReference<TimePickerFragment>) : Handler() {
        override fun handleMessage(msg: Message) {
            val tpfwr = timePickerFragmentWR.get() as TimePickerFragment
            if(tpfwr.keepTime) {
                if(msg.what == tpfwr.KEEP_TIME) {
                    tpfwr.updateUITime()
                }
                else if(msg.what == tpfwr.TIME_STOP) {
                    tpfwr.timeKeeper = null
                    tpfwr.keepTime = false
                }
            }
        }
    }

    companion object {
        private const val SECOND_ROTATIONS_PER_12_HOURS = 720.0
        private const val MINUTE_ROTATIONS_PER_12_HOURS = 12.0
        private const val HOURS_ROTATIONS_PER_MINUTE = 1.0 / 12.0
        private const val SECOND_ROTATIONS_PER_MINUTE = 60.0
        private const val HOUR_ROTATIONS_PER_SECOND = 1.0 / 720.0
        private const val MINUTE_ROTATIONS_PER_SECOND = 1.0 / 60.0
        private const val HOUR_HAND = 0
        private const val MINUTE_HAND = 1
        private const val SECOND_HAND = 2
    }
}