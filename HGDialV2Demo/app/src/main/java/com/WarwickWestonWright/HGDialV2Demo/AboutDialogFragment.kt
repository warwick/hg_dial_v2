/*
Though this is an open source distribution only the files in the HGDialV2 package are protected by open source license.

All of the files in this package (HGDialV2Demo) are free source. Any developer may refactor/re-purpose or reuse the contents
of the files in the HGDialV2Demo package without any obligations to the developer of the HGDialV2 library. The files in this
package (HGDialV2Demo) are intended to give developer/users of the HGDialV2 library a head start in understanding how to use the
diverse, open usage HGDialV2 library.
*/
package com.WarwickWestonWright.HGDialV2Demo

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ScrollView
import android.widget.TextView
import androidx.fragment.app.DialogFragment

class AboutDialogFragment : DialogFragment() {
    private lateinit var rootView: View
    private lateinit var lblParagraph01: TextView
    private lateinit var lblParagraph03: TextView
    private lateinit var lblParagraph05: TextView
    private lateinit var lblParagraph14: TextView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        rootView = inflater.inflate(R.layout.about_dialog_fragment, container, false)
        lblParagraph01 = rootView.findViewById(R.id.lblParagraph01)
        lblParagraph03 = rootView.findViewById(R.id.lblParagraph03)
        lblParagraph03.movementMethod = LinkMovementMethod.getInstance()
        lblParagraph05 = rootView.findViewById(R.id.lblParagraph05)
        lblParagraph05.movementMethod = LinkMovementMethod.getInstance()
        lblParagraph14 = rootView.findViewById(R.id.lblParagraph14)
        lblParagraph14.movementMethod = LinkMovementMethod.getInstance()

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            lblParagraph01.text = Html.fromHtml(
                "<h3>" + lblParagraph01.text + "</h3>",
                Html.FROM_HTML_OPTION_USE_CSS_COLORS
            )
            lblParagraph03.text = Html.fromHtml(
                "<a href=\"mailto:warwickwestonwright@gmail.com?subject=HGDialV2 Feedback\">warwickwestonwright@gmail.com</a>",
                Html.FROM_HTML_OPTION_USE_CSS_COLORS
            )
            lblParagraph05.text = Html.fromHtml(
                "<a href=\"http://play.google.com/store/apps/details?id=com.WarwickWestonWright.ABPlayListDemo\">AB PlayList Demo</a>",
                Html.FROM_HTML_OPTION_USE_CSS_COLORS
            )
            lblParagraph14.text = Html.fromHtml(
                "<a href=\"https://bitbucket.org/warwick/hg_dial_v2\">https://bitbucket.org/warwick/hg_dial_v2</a>",
                Html.FROM_HTML_OPTION_USE_CSS_COLORS
            )
        }
        else {
            lblParagraph01.text = Html.fromHtml("<h3>" + lblParagraph01.text + "</h3>")
            lblParagraph03.text = Html.fromHtml("<a href=\"mailto:warwickwestonwright@gmail.com?subject=HGDialV2 Feedback\">warwickwestonwright@gmail.com</a>")
            lblParagraph05.text = Html.fromHtml("<a href=\"http://play.google.com/store/apps/details?id=com.WarwickWestonWright.ABPlayListDemo\">AB PlayList Demo</a>")
            lblParagraph14.text = Html.fromHtml("<a href=\"https://bitbucket.org/warwick/hg_dial_v2\">https://bitbucket.org/warwick/hg_dial_v2</a>")
        }
        return rootView
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = Dialog(requireActivity())
        val root = ScrollView(requireActivity())
        root.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(root)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.BLACK))
        dialog.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        return dialog
    }
}