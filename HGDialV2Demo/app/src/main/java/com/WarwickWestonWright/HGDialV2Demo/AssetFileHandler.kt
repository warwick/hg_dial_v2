/*
Though this is an open source distribution only the files in the HGDialV2 package are protected by open source license.

All of the files in this package (HGDialV2Demo) are free source. Any developer may refactor/re-purpose or reuse the contents
of the files in the HGDialV2Demo package without any obligations to the developer of the HGDialV2 library. The files in this
package (HGDialV2Demo) are intended to give developer/users of the HGDialV2 library a head start in understanding how to use the
diverse, open usage HGDialV2 library.
*/
package com.WarwickWestonWright.HGDialV2Demo

import androidx.appcompat.app.AppCompatActivity
import java.nio.charset.Charset

class AssetFileHandler {
    fun getAssetText(context: AppCompatActivity, fileName: String): String {
        val inStream = context.assets.open(fileName)
        return inStream.readBytes().toString(Charset.defaultCharset())
    }
}