/*
This class is concerned with calculations necessary for the library to work. It is also a public utility class to enhance developer usage scenarios.

License. This project is licensed with the 2-clause BSD license. The BSD 2-Clause License [OSI Approved License] The BSD 2-Clause License
In the original BSD license, both occurrences of the phrase "COPYRIGHT HOLDERS AND CONTRIBUTORS" in the disclaimer read "REGENTS AND CONTRIBUTORS".
Copyright (c) 2017, Warwick Weston Wright All rights reserved.
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package com.WarwickWestonWright.HGDialV2;

import static java.lang.Math.PI;
import static java.lang.Math.abs;
import static java.lang.Math.atan2;
import static java.lang.Math.cos;
import static java.lang.Math.pow;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;

import android.graphics.Point;

public class HGGeometry {

    public double getAngleFromPoint(final Point centerPoint, final Point touchPoint) {
        //touchPoint.x = 900; touchPoint.y = 450;
        double returnVal;
        //+0 - 0.5
        if(touchPoint.x > centerPoint.x) {
            returnVal = (atan2((touchPoint.x - centerPoint.x), (centerPoint.y - touchPoint.y)) * 0.5 / PI);

        }
        //+0.5
        else {
            returnVal = (1 - (atan2((centerPoint.x - touchPoint.x), (centerPoint.y - touchPoint.y)) * 0.5 / PI));
        }
        return returnVal;
    }

    public double getRadiansFromPoint(final Point centerPoint, final Point touchPoint) {
        double returnVal;
        returnVal = sin((centerPoint.x * 2 - touchPoint.x) / (float) centerPoint.x * 2f);
        return returnVal;
    }

    public Point getPointFromAngle(final double angle, final double radius) {
        final Point coordinates = new Point();
        coordinates.x = (int) (radius * sin((angle) * 2 * PI));
        coordinates.y = (int) -(radius * cos((angle) * 2 * PI));
        return coordinates;
    }

    public double getTwoFingerDistance(final double firstTouchX, final double firstTouchY, final double secondTouchX, final double secondTouchY) {
        double pinchDistanceX = 0;
        double pinchDistanceY = 0;
        if(firstTouchX > secondTouchX) {
            pinchDistanceX = abs(secondTouchX - firstTouchX);
        }
        else if(firstTouchX < secondTouchX) {
            pinchDistanceX = abs(firstTouchX - secondTouchX);
        }
        if(firstTouchY > secondTouchY) {
            pinchDistanceY = abs(secondTouchY - firstTouchY);
        }
        else if(firstTouchY < secondTouchY) {
            pinchDistanceY = abs(firstTouchY - secondTouchY);
        }
        if(pinchDistanceX == 0 && pinchDistanceY == 0) {
            return  0;
        }
        else {
            pinchDistanceX = (pinchDistanceX * pinchDistanceX);
            pinchDistanceY = (pinchDistanceY * pinchDistanceY);
            return (float) abs(sqrt(pinchDistanceX + pinchDistanceY));
        }
    }

    //This is a nifty little method. This will return a width and height (as an array of doubles) given a diagonal  length (first parameter)
    //and a width and height (last two parameters) used as a width to height ratio. In the return type subscript 0 of the array is the width
    //and 1 the height.
    public double[] pythagoreanTheorem(final double diagonalLength, final double width, final double height) {
        double[] returnVal = {0, 0};
        if(width != 0 && height != 0) {
            double sqrt = sqrt((diagonalLength * diagonalLength) / (pow((width / height), 2) + pow((1.0), 2)));
            if(height > 0) {
                returnVal[1] = (float) sqrt;
            }
            else {
                returnVal[1] = (float) -sqrt;
            }
            returnVal[0] = (returnVal[1] / (height / width));
        }
        else if(width != 0) {
            returnVal[0] = width;
            returnVal[1] = 0;
            return returnVal;
        }
        else if(height != 0) {
            returnVal[0] = 0;
            returnVal[1] = height;
            return returnVal;
        }
        return returnVal;
    }
}