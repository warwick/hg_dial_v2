/*
This class is concerned with: Populating the FrameLayout where the HGDialV2 objects will reside.
It will also handle the touch events and parse back the HGDialInfo object to the client activity/fragment

License. This project is licensed with the 2-clause BSD license. The BSD 2-Clause License [OSI Approved License] The BSD 2-Clause License
In the original BSD license, both occurrences of the phrase "COPYRIGHT HOLDERS AND CONTRIBUTORS" in the disclaimer read "REGENTS AND CONTRIBUTORS".
Copyright (c) 2017, Warwick Weston Wright All rights reserved.
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package com.WarwickWestonWright.HGDialV2;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;

import androidx.core.content.ContextCompat;

import java.util.ArrayList;

public final class HGViewContainer implements View.OnTouchListener {

    private HGDialV2.IHGDial ihgDial;
    private final ArrayList<HGDialV2> hgDialV2s = new ArrayList<>();
    private HGDialV2 hgDialV2Active;
    private final FrameLayout container;
    private int activeDial;
    private final Context context;

    public HGViewContainer(FrameLayout container) {
        this.container = container;
        context = container.getContext();
        hgDialV2s.clear();
        container.setOnTouchListener(this);
    }

    public HGViewContainer(int drawable, FrameLayout container) {
        this.container = container;
        context = container.getContext();
        activeDial = 0;
        hgDialV2s.clear();
        hgDialV2s.add(new HGDialV2(container.getContext()));
        hgDialV2s.get(0).setDrawable(ContextCompat.getDrawable(context, drawable));
        hgDialV2s.get(0).setActive(true);
        hgDialV2Active = hgDialV2s.get(0);
        container.addView(hgDialV2s.get(0));
        container.setOnTouchListener(this);
    }

    public HGViewContainer(int[] drawableIds, FrameLayout container) {
        this.container = container;
        final int drawableIdsLength = drawableIds.length;
        context = container.getContext();
        hgDialV2s.clear();
        //Remove all HGDialV2 objects from container to allow simple recreate.
        for(int i = 0; i < drawableIdsLength; i++) {if(container.getChildAt(i) instanceof HGDialV2) {container.removeView(container.getChildAt(i));}}
        for(int i = 0; i < drawableIdsLength; i++) {
            hgDialV2s.add(i, new HGDialV2(container.getContext()));
            hgDialV2s.get(i).setDrawable(ContextCompat.getDrawable(context, drawableIds[i]));
            hgDialV2s.get(i).setActive(false);
            container.addView(hgDialV2s.get(i));
        }
        activeDial = drawableIdsLength - 1;
        hgDialV2s.get(activeDial).setActive(true);
        hgDialV2Active = hgDialV2s.get(activeDial);
        container.setOnTouchListener(this);
    }

    public void registerCallback(HGDialV2.IHGDial ihgDial) {hgDialV2Active.ihgDial = this.ihgDial = ihgDial;}

    public void addDial(final Bitmap bitmapDial) {
        final HGDialV2 hgDialV2 = new HGDialV2(context);
        final Drawable drawableDial = (new BitmapDrawable(context.getResources(), bitmapDial)).getCurrent();
        hgDialV2.setDrawable(drawableDial);
        hgDialV2.setActive(true);
        hgDialV2s.add(hgDialV2s.size(), hgDialV2);
        hgDialV2Active = hgDialV2;
        hgDialV2Active.ihgDial = this.ihgDial;
        container.addView(hgDialV2);
        if(this.ihgDial != null) {
            hgDialV2Active.ihgDial = this.ihgDial;
        }
        else {
            hgDialV2Active.ihgDial = hgDialV2s.get(activeDial).ihgDial;

        }
    }

    public void addDial(final HGDialV2 hgDialV2) {
        hgDialV2.setActive(true);
        hgDialV2s.add(hgDialV2s.size(), hgDialV2);
        hgDialV2Active = hgDialV2;
        hgDialV2Active.ihgDial = this.ihgDial;
        container.addView(hgDialV2);
        if(this.ihgDial != null) {
            hgDialV2Active.ihgDial = this.ihgDial;
        }
        else {
            hgDialV2Active.ihgDial = hgDialV2s.get(activeDial).ihgDial;
        }
    }

    //Accessors
    public ArrayList<HGDialV2> getHgDialV2s() {return this.hgDialV2s;}
    public HGDialV2 getHgDialV2Active() {return this.hgDialV2Active;}
    public int getActiveDial() {return this.activeDial;}

    public void cancelSpinThread() {
        hgDialV2Active.cancelSpin();
        final int hgDialV2sSize = hgDialV2s.size();
        final int storedActiveDial = activeDial;
        for(int i = 0; i < hgDialV2sSize; i++) {
            hgDialV2s.get(i).cancelSpin();
        }
        hgDialV2Active = hgDialV2s.get(storedActiveDial);
    }

    //Mutators
    public void setActiveDial(final int activeDial) {
        //Optimisation note: Consider scrapping the hgDialV2 objects active property all together and just accessing active dial from this class
        this.hgDialV2Active = hgDialV2s.get(activeDial);
        this.activeDial = activeDial;
        final int hgDialV2sSize = hgDialV2s.size();
        for(int i = 0; i < hgDialV2sSize; i++) {
            hgDialV2s.get(i).setActive(false);
        }
        hgDialV2Active.setActive(true);
        if(this.ihgDial != null) {
            hgDialV2Active.ihgDial = this.ihgDial;
        }
        else{
            hgDialV2Active.ihgDial = hgDialV2s.get(activeDial).ihgDial;
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        hgDialV2Active.sendTouchEvent(event);
        if (event.getAction() == MotionEvent.ACTION_UP) {
            v.performClick();
            return true;
        }
        return true;
    }
}