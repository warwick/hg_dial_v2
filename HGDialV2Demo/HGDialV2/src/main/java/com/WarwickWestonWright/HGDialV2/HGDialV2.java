/*
This class is concerned with: Redrawing canvas based upon data parsed from the DialMetrics class.

License. This project is licensed with the 2-clause BSD license. The BSD 2-Clause License [OSI Approved License] The BSD 2-Clause License
In the original BSD license, both occurrences of the phrase "COPYRIGHT HOLDERS AND CONTRIBUTORS" in the disclaimer read "REGENTS AND CONTRIBUTORS".
Copyright (c) 2017, Warwick Weston Wright All rights reserved.
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package com.WarwickWestonWright.HGDialV2;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;

import java.util.List;

public class HGDialV2 extends View implements View.OnTouchListener {
    public IHGDial ihgDial;
    private final Point centerPoint = new Point(0, 0);
    private Drawable drawable;
    private final AngleWrapper angleWrapper = new AngleWrapper(new Point(0, 0));
    private final DialMetrics dialMetrics = new DialMetrics(this, angleWrapper);
    private final HGDialInfo hgDialInfo = dialMetrics.hgDialInfo;
    //private final Point rotationCenterPoint = new Point(0, 0);
    private boolean isActive;
    private boolean viewIsSetup;
    private boolean suppressInvalidate;
    private int pl;
    private int pt;
    private int pr;
    private int pb;
    private int cw;
    private int ch;
    private int lm;
    private int tm;
    private int rm;
    private int bm;
    private boolean useSingleFinger;
    private boolean tooManyTouches;
    private boolean touchesSwapped;
    private float firstTouchX;
    private float firstTouchY;
    private float secondTouchX;
    private float secondTouchY;
    private float touchOffsetX;
    private float touchOffsetY;
    private int touchPointerCount;
    public double angleSnap;

    public interface IHGDial {
        void onDown(HGDialInfo hgDialInfo);
        void onPointerDown(HGDialInfo hgDialInfo);
        void onMove(HGDialInfo hgDialInfo);
        void onPointerUp(HGDialInfo hgDialInfo);
        void onUp(HGDialInfo hgDialInfo);
    }

    public HGDialV2(Context context) {super(context);init(null, 0);}
    public HGDialV2(Context context, AttributeSet attrs) {super(context, attrs);init(attrs, 0); setOnTouchListener(this);}
    public HGDialV2(Context context, AttributeSet attrs, int defStyle) {super(context, attrs, defStyle);init(attrs, defStyle);}

    private void init(AttributeSet attrs, int defStyle) {
        setFields();
        setFocusable(true);
        setFocusableInTouchMode(true);
        final TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.HGDialV2, defStyle, 0);
        if(a.hasValue(R.styleable.HGDialV2_HGDialV2Drawable)) {
            drawable = a.getDrawable(R.styleable.HGDialV2_HGDialV2Drawable);
            assert drawable != null;
            drawable.setCallback(this);
        }
        a.recycle();
    }

    @Override
    protected void onDraw(@NonNull final Canvas canvas) {
        super.onDraw(canvas);
        if(angleSnap == 0) {
            canvas.rotate((float) (angleWrapper.getTextureAngle() % 1 * 360), centerPoint.x, centerPoint.y);
        }
        else {
            canvas.rotate((float) (dialMetrics.angleSnapNext * 360), centerPoint.x, centerPoint.y);
        }
        drawable.draw(canvas);
    }

    //Register main callback
    public void registerCallback(IHGDial ihgDial) {this.ihgDial = ihgDial;}
    public void unRegisterCallback() {this.ihgDial = null;}

    private void setFields() {
        if(this.dialMetrics.flingAnimationHandler == null) {dialMetrics.flingAnimationHandler = new FlingAnimationHandler(dialMetrics, this);}
        this.isActive = false;
        this.viewIsSetup = false;
        this.suppressInvalidate = dialMetrics.suppressInvalidate;
        this.pl = 0;
        this.pt = 0;
        this.pr = 0;
        this.pb = 0;
        this.cw = 0;
        this.ch = 0;
        this.lm = 0;
        this.tm = 0;
        this.rm = 0;
        this.bm = 0;
        this.useSingleFinger = dialMetrics.useSingleFinger;
        this.tooManyTouches = false;
        this.touchesSwapped = false;
        this.firstTouchX = 0f;
        this.firstTouchY = 0f;
        this.secondTouchX = 0f;
        this.secondTouchY = 0f;
        this.touchOffsetX = 0f;
        this.touchOffsetY = 0f;
        this.touchPointerCount = 0;
        this.angleSnap = dialMetrics.angleSnap;
    }

    /* Top of block touch methods */
    private void setDownTouch(final MotionEvent event) {
        try {
            try {
                if(useSingleFinger) {
                    dialMetrics.flingDownTouchX = firstTouchX = event.getX(event.findPointerIndex(event.getPointerId(0))) + touchOffsetX;
                    dialMetrics.flingDownTouchY = firstTouchY = event.getY(event.findPointerIndex(event.getPointerId(0))) + touchOffsetY;
                    hgDialInfo.setFirstTouchX(firstTouchX);
                    hgDialInfo.setFirstTouchY(firstTouchY);
                    secondTouchX = touchOffsetX;
                    secondTouchY = touchOffsetY;
                }
                else if(touchPointerCount > 1) {
                    firstTouchX = event.getX(event.findPointerIndex(event.getPointerId(0))) + touchOffsetX;
                    firstTouchY = event.getY(event.findPointerIndex(event.getPointerId(0))) + touchOffsetY;
                    secondTouchX = event.getX(event.findPointerIndex(event.getPointerId(1))) + touchOffsetX;
                    secondTouchY = event.getY(event.findPointerIndex(event.getPointerId(1))) + touchOffsetY;
                    hgDialInfo.setFirstTouchX(firstTouchX);
                    hgDialInfo.setFirstTouchY(firstTouchY);
                    hgDialInfo.setSecondTouchX(secondTouchX);
                    hgDialInfo.setSecondTouchY(secondTouchY);
                }
            }
            catch(IndexOutOfBoundsException ignored) {}
        }
        catch(IllegalArgumentException ignored) {}
    }

    private void setMoveTouch(final MotionEvent event) {
        try {
            try {
                if(useSingleFinger) {
                    firstTouchX = event.getX(event.findPointerIndex(event.getPointerId(0))) + touchOffsetX;
                    firstTouchY = event.getY(event.findPointerIndex(event.getPointerId(0))) + touchOffsetY;
                    hgDialInfo.setFirstTouchX(firstTouchX);
                    hgDialInfo.setFirstTouchY(firstTouchY);
                    secondTouchX = touchOffsetX;
                    secondTouchY = touchOffsetY;
                }
                else {
                    firstTouchX = event.getX(event.findPointerIndex(event.getPointerId(0))) + touchOffsetX;
                    firstTouchY = event.getY(event.findPointerIndex(event.getPointerId(0))) + touchOffsetY;
                    secondTouchX = event.getX(event.findPointerIndex(event.getPointerId(1))) + touchOffsetX;
                    secondTouchY = event.getY(event.findPointerIndex(event.getPointerId(1))) + touchOffsetY;
                    hgDialInfo.setFirstTouchX(firstTouchX);
                    hgDialInfo.setFirstTouchY(firstTouchY);
                    hgDialInfo.setSecondTouchX(secondTouchX);
                    hgDialInfo.setSecondTouchY(secondTouchY);
                }
            }
            catch(IndexOutOfBoundsException ignored) {}
        }
        catch(IllegalArgumentException ignored) {}
    }

    private void setUpTouch(final MotionEvent event) {
        try {
            try {
                if(useSingleFinger) {
                    firstTouchX = event.getX(event.findPointerIndex(0)) + touchOffsetX;
                    firstTouchY = event.getY(event.findPointerIndex(0)) + touchOffsetY;
                    secondTouchX = touchOffsetX;
                    secondTouchY = touchOffsetY;
                    hgDialInfo.setFirstTouchX(firstTouchX);
                    hgDialInfo.setFirstTouchY(firstTouchY);
                }
                else if(touchPointerCount == 2) {
                    firstTouchX = event.getX(event.findPointerIndex(event.getPointerId(0))) + touchOffsetX;
                    firstTouchY = event.getY(event.findPointerIndex(event.getPointerId(0))) + touchOffsetY;
                    secondTouchX = event.getX(event.findPointerIndex(event.getPointerId(1))) + touchOffsetX;
                    secondTouchY = event.getY(event.findPointerIndex(event.getPointerId(1))) + touchOffsetY;
                    hgDialInfo.setFirstTouchX(firstTouchX);
                    hgDialInfo.setFirstTouchY(firstTouchY);
                    hgDialInfo.setSecondTouchX(secondTouchX);
                    hgDialInfo.setSecondTouchY(secondTouchY);
                }
            }
            catch(IndexOutOfBoundsException ignored) {}
        }
        catch(IllegalArgumentException ignored) {}
    }
    /* Bottom of block touch methods */


    /* Top of block rotate functions */
    private HGDialInfo doDownDial(final int action) {
        //Prepare touches for single or dual touch
        if(useSingleFinger) {
            angleWrapper.touchPoint.x = (int) firstTouchX;
            angleWrapper.touchPoint.y = (int) firstTouchY;
        }
        else if(touchPointerCount < 2) {
            final Point lastAngle = dialMetrics.hgGeometry.getPointFromAngle(dialMetrics.fullGestureAngle, dialMetrics.imageRadius);
            angleWrapper.touchPoint.x = lastAngle.x + angleWrapper.rotationCenterPoint.x;
            angleWrapper.touchPoint.y = lastAngle.y + angleWrapper.rotationCenterPoint.y;
        }
        else {
            angleWrapper.touchPoint.x = (int) secondTouchX;
            angleWrapper.touchPoint.y = (int) secondTouchY;
        }
        return dialMetrics.getHgDialInfo(action);
    }

    private HGDialInfo doMoveDial(final int action) {
        //Prepare touches for single or dual touch
        if(useSingleFinger) {
            angleWrapper.touchPoint.x = (int) firstTouchX;
            angleWrapper.touchPoint.y = (int) firstTouchY;
        }
        else if(touchPointerCount > 1) {
            angleWrapper.touchPoint.x = (int) secondTouchX;
            angleWrapper.touchPoint.y = (int) secondTouchY;
        }
        else {
            if(!dialMetrics.spinTriggeredProgrammatically) {return hgDialInfo;}
        }
        return dialMetrics.getHgDialInfo(action);
    }

    private HGDialInfo doUpDial(final int action) {
        //Prepare touches for single or dual touch
        if(useSingleFinger) {
            angleWrapper.touchPoint.x = (int) firstTouchX;
            angleWrapper.touchPoint.y = (int) firstTouchY;
        }
        else if(touchPointerCount > 1) {
            angleWrapper.touchPoint.x = (int) secondTouchX;
            angleWrapper.touchPoint.y = (int) secondTouchY;
        }
        return dialMetrics.getHgDialInfo(action);
    }
    /* Bottom of block rotate functions */

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        hgTouchEvent(event);
        return true;
    }

    public void sendTouchEvent(final MotionEvent event) {hgTouchEvent(event);}

    private void hgTouchEvent(final MotionEvent event) {
        synchronizeWithOnDraw(event);
    }

    private void synchronizeWithOnDraw(final MotionEvent event) {
        if(event == null) {return;}
        touchPointerCount = event.getPointerCount();
        final int action = event.getAction() & MotionEvent.ACTION_MASK;
        if(action == MotionEvent.ACTION_MOVE) {
            if(touchesSwapped) {return;}
            setMoveTouch(event);
            ihgDial.onMove(doMoveDial(action));
        }
        else if(action == MotionEvent.ACTION_DOWN) {
            setDownTouch(event);
            ihgDial.onDown(doDownDial(action));
        }
        else if(action == MotionEvent.ACTION_POINTER_DOWN) {
            if((touchPointerCount > 2 && !useSingleFinger) || (touchPointerCount > 1 && useSingleFinger)) {
                tooManyTouches = true;
            }
            else {
                setDownTouch(event);
                ihgDial.onDown(doDownDial(action));
            }
        }
        else if(action == MotionEvent.ACTION_POINTER_UP) {
            if(!tooManyTouches) {
                setUpTouch(event);
                ihgDial.onUp(doUpDial(action));
            }
            else {
                if(!useSingleFinger) {
                    touchesSwapped = event.findPointerIndex(2) != event.getActionIndex();
                }
                else {
                    touchesSwapped = event.findPointerIndex(1) != event.getActionIndex();
                }
            }
        }
        else if(action == MotionEvent.ACTION_UP) {
            tooManyTouches = false;
            touchesSwapped = false;
            setUpTouch(event);
            ihgDial.onUp(doUpDial(action));
            performClick();
        }
    }

    //Accessors
    public Drawable getDrawable() {return this.drawable;}
    public HGDialInfo getHgDialInfo() {return this.hgDialInfo;}
    public boolean getIsActive() {return this.isActive;}
    public double getPrecisionRotation() {return dialMetrics.getPrecisionRotation();}
    public double getFullTextureAngle() {return this.dialMetrics.angleWrapper.getTextureAngle();}
    public double getFullGestureAngle() {return this.dialMetrics.fullGestureAngle;}
    public boolean getSuppressInvalidate() {return this.suppressInvalidate;}
    public boolean getCumulativeRotate() {return dialMetrics.cumulativeRotate;}
    public boolean getUseSingleFinger() {return this.useSingleFinger;}
    public double getVariableDialInner() {return this.dialMetrics.variableDialInner;}
    public double getVariableDialOuter() {return dialMetrics.variableDialOuter;}
    public boolean getUseVariableDial() {return dialMetrics.useVariableDial;}
    public boolean getUseVariableDialCurve() {return dialMetrics.useVariableDialCurve;}
    public boolean getPositiveCurve() {return dialMetrics.positiveCurve;}
    public long getQuickTapTime() {return dialMetrics.quickTapTime;}
    public boolean getSpinTriggered() {return dialMetrics.spinTriggered;}
    public int getFlingDistanceThreshold() {return dialMetrics.flingDistanceThreshold;}
    public long getFlingTimeThreshold() {return dialMetrics.flingTimeThreshold;}
    public float getSpinStartSpeed() {return dialMetrics.spinStartSpeed;}
    public float getSpinEndSpeed() {return dialMetrics.spinEndSpeed;}
    public long getSpinDuration() {return dialMetrics.spinDuration;}
    public int getLastTextureDirection() {return dialMetrics.getLastTextureDirection();}
    public Point getCenterPoint() {return this.dialMetrics.angleWrapper.getCenterPoint();}
    public double getMinimumRotation() {return dialMetrics.minimumRotation;}
    public double getMaximumRotation() {return dialMetrics.maximumRotation;}
    public boolean getUseMinMaxRotation() {return dialMetrics.useMinMaxRotation;}
    public float getSlowFactor() {return dialMetrics.slowFactor;}
    public boolean getSlowDown() {return dialMetrics.slowDown;}
    public double getFlingAngleTolerance() {return dialMetrics.flingAngleTolerance;}
    public double getAngleSnap() {return dialMetrics.angleSnap;}
    private double getAngleSnapProximity() {return dialMetrics.angleSnapProximity;}
    private List<SnapPoint> getSnapPoints() {return dialMetrics.snapPoints;}
    public float getTouchOffsetX() {return touchOffsetX;}
    public float getTouchOffsetY() {return touchOffsetY;}
    public boolean hasAngleSnapped() {return dialMetrics.rotateSnapped;}

    //Mutators
    public void setDrawable(final Drawable drawable) {this.drawable = drawable; viewIsSetup = false; invalidate();}

    public void setActive(final boolean isActive) {
        this.isActive = isActive;
        if(!isActive) {cancelSpin();}
        doManualTextureDial(getFullTextureAngle());
        invalidate();
    }

    public void setPrecisionRotation(final double precisionRotation) {dialMetrics.setPrecisionRotation(precisionRotation);}
    public void setFullTextureAngle(final double fullTextureAngle) {this.dialMetrics.setFullGestureAngle(fullTextureAngle);}
    public void setSuppressInvalidate(final boolean suppressInvalidate) {this.suppressInvalidate = suppressInvalidate; dialMetrics.suppressInvalidate = suppressInvalidate;}
    public void setCumulativeRotate(final boolean cumulativeRotate) {dialMetrics.cumulativeRotate = cumulativeRotate;}
    public void setUseSingleFinger(final boolean useSingleFinger) {dialMetrics.useSingleFinger = useSingleFinger; this.useSingleFinger = useSingleFinger;}
    public void setQuickTapTime(final long quickTapTime) {dialMetrics.quickTapTime = quickTapTime;}
    public void setSlowFactor(final float slowFactor) {dialMetrics.slowFactor  = slowFactor;}
    public void setSlowDown(final boolean slowDown) {dialMetrics.slowDown = slowDown;}
    public void setFlingAngleTolerance(final double flingAngleTolerance) {dialMetrics.flingAngleTolerance = flingAngleTolerance;}

    public void setFlingTolerance(final int flingDistanceThreshold, final long flingTimeThreshold) {
        dialMetrics.flingDistanceThreshold = flingDistanceThreshold;
        dialMetrics.flingTimeThreshold = flingTimeThreshold;
    }

    public void setSpinAnimation(final float spinStartSpeed, final float spinEndSpeed, final long spinDuration) {
        dialMetrics.spinStartSpeedState = dialMetrics.spinStartSpeed = spinStartSpeed;
        dialMetrics.spinEndSpeedState = dialMetrics.spinEndSpeed = spinEndSpeed;
        dialMetrics.spinDurationState = dialMetrics.spinDuration = spinDuration;
    }

    public void setSnapPoints(final List<SnapPoint> snapPoints) {dialMetrics.setSnapPoints(snapPoints);}
    public void setTouchOffset(final float touchOffsetX, final float touchOffsetY) {this.touchOffsetX = touchOffsetX; this.touchOffsetY = touchOffsetY;}

    /* Top of block convenience methods */
    public void doManualTextureDial(final double manualDial) {dialMetrics.doManualTextureDial(manualDial); invalidate();}
    public void doManualGestureDial(final double manualDial) {dialMetrics.doManualGestureDial(manualDial); invalidate();}
    public void cancelSpin() {dialMetrics.cancelSpin();}
    public HGState saveState() {return dialMetrics.saveState();}
    public HGState saveState(String tag) {return dialMetrics.saveState(tag);}
    public HGState saveStateFlush() {return dialMetrics.saveStateFlush();}
    public void restoreState(final HGState hgState, final boolean updateUI) {dialMetrics.restoreState(hgState, updateUI);}
    public void performQuickTap() {dialMetrics.performQuickTap();}

    public void triggerSpin(final float spinStartSpeed, final float spinEndSpeed, final long spinDuration, final int lastTextureDirection) {
        dialMetrics.triggerSpin(spinStartSpeed, spinEndSpeed, spinDuration, lastTextureDirection);
    }
    /* Bottom of block convenience methods */

    /* Top of block behavioural methods */
    public void setMinMaxDial(final double minimumRotation, final double maximumRotation, final boolean useMinMaxRotation) {dialMetrics.setMinMaxDial(minimumRotation, maximumRotation, useMinMaxRotation);}
    public void setVariableDial(final double variableDialInner, final double variableDialOuter, final Boolean useVariableDial) {dialMetrics.setVariableDial(variableDialInner, variableDialOuter, useVariableDial);}
    public void setUseVariableDialCurve(final boolean useVariableDialCurve, final boolean positiveCurve) {dialMetrics.setUseVariableDialCurve(useVariableDialCurve, positiveCurve);}
    public void doRapidDial(final double textureAngle) {angleWrapper.setTextureAngle(textureAngle); invalidate();}
    public void setAngleSnap(final double angleSnap, final double angleSnapProximity) {dialMetrics.setAngleSnap(angleSnap, angleSnapProximity); this.angleSnap = angleSnap;}
    /* Bottom of block behavioural methods */

    @Override
    public void requestLayout() {
        viewIsSetup = false;
        super.requestLayout();
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        viewIsSetup = false;
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        pl = getPaddingLeft();
        pt = getPaddingTop();
        pr = getPaddingRight();
        pb = getPaddingBottom();
        cw = getWidth() - pl - pr;
        ch = getHeight() - pt - pb;
        lm = ((FrameLayout.LayoutParams) getLayoutParams()).leftMargin;
        tm = ((FrameLayout.LayoutParams) getLayoutParams()).topMargin;
        rm = ((FrameLayout.LayoutParams) getLayoutParams()).rightMargin;
        bm = ((FrameLayout.LayoutParams) getLayoutParams()).bottomMargin;
        if(cw < ch) {
            int widgetTop = pt + ((ch - cw) / 2);
            dialMetrics.imageRadius = (int) ((float) cw / 2f);
            drawable.setBounds(pl, pt + widgetTop, pl + cw, pt + ch - widgetTop);
        }
        else {
            int widgetLeft = pl + ((cw - ch) / 2);
            dialMetrics.imageRadius = (int) ((float) ch / 2f);
            drawable.setBounds(pl + widgetLeft, pt, pl + cw - widgetLeft, pt + ch);
        }
        dialMetrics.imageRadiusX2 = dialMetrics.imageRadius * 2;
        centerPoint.x = (cw / 2) + lm;
        centerPoint.y = (ch / 2) + tm;
        angleWrapper.setCenterPoint(centerPoint);
        angleWrapper.rotationCenterPoint = centerPoint;
        viewIsSetup = true;
    }

    @Override
    protected void onConfigurationChanged(final Configuration newConfig) {
        viewIsSetup = false;
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onDetachedFromWindow() {
        if(this.dialMetrics.flingAnimationHandler != null) {
            //Fault Tolerance
            this.dialMetrics.flingAnimationHandler.spinTriggered = false;
        }
        this.isActive = false;
        this.viewIsSetup = false;
        this.suppressInvalidate = false;
        this.pl = 0;
        this.pt = 0;
        this.pr = 0;
        this.pb = 0;
        this.cw = 0;
        this.ch = 0;
        this.lm = 0;
        this.tm = 0;
        this.rm = 0;
        this.bm = 0;
        this.useSingleFinger = true;
        this.firstTouchX = 0f;
        this.firstTouchY = 0f;
        this.secondTouchX = 0f;
        this.secondTouchY = 0f;
        this.touchOffsetX = 0f;
        this.touchOffsetY = 0f;
        this.touchPointerCount = 0;
        this.angleSnap = 0;
        super.onDetachedFromWindow();
    }
}