package com.WarwickWestonWright.HGDialV2;

import java.util.List;

public class SnapPoint {
    private double start;
    private double end;
    private double startProx;
    private double endProx;

    public SnapPoint(final double start, final double end, final double startProx, final double endProx) {
        this.start = start;
        this.end = end;
        this.startProx = startProx;
        this.endProx = endProx;
    }

    public SnapPoint() {}

    //Accessors
    public double getStart() {return start;}
    public double getEnd() {return end;}
    public double getStartProx() {return startProx;}
    public double getEndProx() {return endProx;}

    //Mutators
    public void setStart(final double start) {this.start = start;}
    public void setEnd(final double end) {this.end = end;}
    public void setStartProx(final double startProx) {this.startProx = startProx;}
    public void setEndProx(final double endProx) {this.endProx = endProx;}

    IrregularSnapStatus getSnapPoint(List<SnapPoint> snapPoints, double angle) {
        for(SnapPoint snapPoint : snapPoints) {
            if(angle > snapPoint.start && angle < snapPoint.startProx) {
                return new IrregularSnapStatus(snapPoint.start, true);
            }
            if(angle < snapPoint.end && angle > snapPoint.endProx) {
                return new IrregularSnapStatus(snapPoint.end, true);
            }
        }
        return new IrregularSnapStatus(angle, false);
    }

    static class IrregularSnapStatus {
        final Double angle;
        final boolean snapped;
        public IrregularSnapStatus(final Double angle, final boolean snapped) {
            this.angle = angle;
            this.snapped = snapped;
        }
    }
}