/*
This class is concerned with: Calculating the angles based upon parsed touch event data.

License. This project is licensed with the 2-clause BSD license. The BSD 2-Clause License [OSI Approved License] The BSD 2-Clause License
In the original BSD license, both occurrences of the phrase "COPYRIGHT HOLDERS AND CONTRIBUTORS" in the disclaimer read "REGENTS AND CONTRIBUTORS".
Copyright (c) 2017, Warwick Weston Wright All rights reserved.
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package com.WarwickWestonWright.HGDialV2;

import android.view.MotionEvent;

import java.util.List;

final class DialMetrics {
    final HGDialInfo hgDialInfo;
    final AngleWrapper angleWrapper;
    public final HGGeometry hgGeometry = new HGGeometry();
    final HGState hgState = new HGState();
    final HGDialV2 hgDialV2;
    FlingAnimationHandler flingAnimationHandler;
    //Bottom of block separation of concerns objects

    //Top of block behavioural flags
    public boolean cumulativeRotate;
    public boolean useSingleFinger;
    public boolean useMinMaxRotation;
    public boolean suppressInvalidate;
    public boolean useVariableDial;
    boolean useVariableDialCurve;
    public boolean positiveCurve;
    boolean rotateSnapped;
    //Bottom of block behavioural flags returnable to client

    //Top of block other behavioural values
    double precisionRotation;
    private double onDownAngle;
    private double onUpAngle;
    public double angleSnap;
    public double angleSnapNext;
    double angleSnapProximity;
    public double variableDialInner;
    public double variableDialOuter;
    public double maximumRotation;
    public double minimumRotation;
    private int minMaxRotationOutOfBounds;
    public long quickTapTime;
    List<SnapPoint> snapPoints = null;
    private final SnapPoint snapPoint = new SnapPoint();
    //Bottom of block other behavioural values

    //Top of block Fling Spin Variables
    public float spinStartSpeed;
    private float spinCurrentSpeed;
    public float spinEndSpeed;
    public long spinDuration;
    public float spinStartSpeedState;
    public float spinEndSpeedState;
    public long spinDurationState;
    public float slowFactor;
    public boolean slowDown;
    public double flingAngleTolerance;
    private long spinEndTime;
    private double spinStartAngle;
    private double fullGestureDownAngle;
    private long gestureDownTime;//Also used for quick tap
    public float flingDownTouchX;
    public float flingDownTouchY;
    public int flingDistanceThreshold;
    public long flingTimeThreshold;
    public volatile boolean spinTriggered;
    public boolean spinTriggeredProgrammatically;
    private final boolean slowSpinOnPositiveCurve;
    private int lastTextureDirection;//Also used for rotation direction in dynamic return type (HGDialInfo)
    //Bottom of block Fling Spin Variables

    //Top of block other variables for internal usage
/*    public float touchXLocal;
    public float touchYLocal;*/
    public double imageRadiusX2;
    public double imageRadius;
    private double storedGestureAngle;
    public double fullGestureAngle;
    private double currentGestureAngle;
    //Bottom of block other variables for internal usage

    DialMetrics(HGDialV2 hgDialV2, AngleWrapper angleWrapper) {
        //X-Class objects instantiation
        this.hgDialInfo = new HGDialInfo();
        this.angleWrapper = angleWrapper;
        this.hgDialV2 = hgDialV2;
        this.flingAnimationHandler = new FlingAnimationHandler(this, hgDialV2);

        //Top of block behavioural flags
        this.cumulativeRotate = true;
        this.useSingleFinger = true;
        this.useMinMaxRotation = false;
        this.suppressInvalidate = false;
        this.useVariableDial = false;
        this.useVariableDialCurve = false;
        this.positiveCurve = false;
        this.rotateSnapped = false;
        //Bottom of block behavioural flags returnable to client

        //Top of block other behavioural values
        this.precisionRotation = 1d;
        this.onDownAngle = 0d;
        this.onUpAngle = 0d;
        this.angleSnap = 0d;
        this.angleSnapNext = 0d;
        this.angleSnapProximity = 0d;
        this.variableDialInner = 0d;
        this.variableDialOuter = 0d;
        this.maximumRotation = 0d;
        this.minimumRotation = 0d;
        this.minMaxRotationOutOfBounds = 0;
        this.quickTapTime = 120L;
        //Bottom of block other behavioural values

        //Top of block Fling Spin Variables
        this.spinStartSpeed = 0f;
        this.spinCurrentSpeed = 0f;
        this.spinEndSpeed = 0f;
        this.spinDuration = 0L;
        this.spinStartSpeedState = spinStartSpeed;
        this.spinEndSpeedState = spinEndSpeed;
        this.spinDurationState = spinDuration;
        this.slowFactor = 0f;
        this.slowDown = true;
        this.flingAngleTolerance = 0f;
        this.spinEndTime = 0L;
        this.spinStartAngle = 0d;
        this.gestureDownTime = 0L;//Also used for quick tap
        this.flingDownTouchX = 0f;
        this.flingDownTouchY = 0f;
        this.flingDistanceThreshold = 0;
        this.flingTimeThreshold = 0L;
        this.spinTriggered = false;
        this.spinTriggeredProgrammatically = false;
        this.slowSpinOnPositiveCurve = false;
        this.lastTextureDirection = 0;//Also used for rotation direction in dynamic return type (HGDialInfo)
        //Bottom of block Fling Spin Variables

        //Top of block other variables for internal usage
        this.angleWrapper.touchPoint.x = 0;
        this.angleWrapper.touchPoint.y = 0;
        this.imageRadiusX2 = 0d;
        this.imageRadius = 0d;
        this.storedGestureAngle = 0d;
        this.fullGestureAngle = 0d;
        this.fullGestureDownAngle = 0d;
        this.currentGestureAngle = 0d;
        //Bottom of block other variables for internal usage
    }

    //Top of block main touch Handler
    HGDialInfo getHgDialInfo(final int motionEventAction) {
        if(motionEventAction == MotionEvent.ACTION_MOVE) {
            angleWrapper.setTouchAngle(hgGeometry.getAngleFromPoint(angleWrapper.getCenterPoint(), angleWrapper.touchPoint));
            setReturnType();
        }
        else if(motionEventAction == MotionEvent.ACTION_DOWN || motionEventAction == MotionEvent.ACTION_POINTER_DOWN) {
            fullGestureDownAngle = fullGestureAngle;
            hgDialInfo.setQuickTap(false);
            hgDialInfo.setSpinTriggered(false);
            if(spinTriggered && angleSnap != 0) {
                if(!useVariableDial) {doManualTextureDial(angleSnapNext);}
            }
            if(spinTriggered) {
                flingAnimationHandler.spinTriggered = spinTriggered = false;
                spinCurrentSpeed = (float) flingAnimationHandler.spinCurrentSpeed;
                hgState.setSpinCurrentSpeed(spinCurrentSpeed);
                hgState.setSpinTimeRemaining(spinEndTime - System.currentTimeMillis());
            }
            if(motionEventAction == MotionEvent.ACTION_POINTER_DOWN && useSingleFinger) {return hgDialInfo;}
            gestureDownTime = System.currentTimeMillis();
            angleWrapper.setTouchAngle(hgGeometry.getAngleFromPoint(angleWrapper.getCenterPoint(), angleWrapper.touchPoint));
            if(precisionRotation != 0) {
                if(cumulativeRotate) {
                    onDownAngle = angleWrapper.getTouchAngle() - onUpAngle;
                    if(useVariableDial) {
                        fullGestureAngle = fullGestureAngle * precisionRotation;
                    }
                }
                else {
                    minMaxRotationOutOfBounds = 0;
                    onDownAngle = angleWrapper.getTouchAngle() - onUpAngle;
                    if(useVariableDial) {
                        fullGestureAngle = angleWrapper.getTouchAngle();
                    }
                    else {
                        fullGestureAngle = angleWrapper.getTouchAngle() / precisionRotation;
                    }
                }
                setReturnType();
                spinStartAngle = angleWrapper.getTextureAngle();
            }
        }
        else if(motionEventAction == MotionEvent.ACTION_POINTER_UP || motionEventAction == MotionEvent.ACTION_UP) {
            angleWrapper.setTouchAngle(hgGeometry.getAngleFromPoint(angleWrapper.getCenterPoint(), angleWrapper.touchPoint));
            if(precisionRotation != 0) {
                onUpAngle = (1 - (onDownAngle - angleWrapper.getTouchAngle())) % 1;
                setReturnType();
            }
            setSpinStatus();
            if(spinTriggered) {
                flingAnimationHandler.startFlingAnim();
            }
            else {
                hgDialV2.getHgDialInfo().setTextureDirection(0);
                hgDialV2.getHgDialInfo().setGestureDirection(0);
            }
        }
        if(!suppressInvalidate) {
            hgDialV2.invalidate();
        }
        return hgDialInfo;
    }
    //Bottom of block main touch Handler

    private void setReturnType() {
        if(!useVariableDial) {
            if(!useMinMaxRotation) {
                hgDialInfo.setGestureDirection(setRotationAngleGestureAndReturnDirection());
            }
            else {
                hgDialInfo.setGestureDirection(setRotationAngleGestureAndReturnDirectionForMinMaxBehaviour());
            }
            hgDialInfo.setGestureAngle(angleWrapper.getGestureAngle());
            hgDialInfo.setTextureDirection(lastTextureDirection);
            if(angleSnap != 0 || snapPoints != null) {
                checkAngleSnap();
            }
            else {
                hgDialInfo.setRotateSnapped(false);
                hgDialInfo.setTextureAngle(angleWrapper.getTextureAngle());
            }
            calculateTextureAngle();
        }
        else {
            if(!useMinMaxRotation) {
                angleWrapper.setTextureDirection(setVariableDialAngleAndReturnDirection());
            }
            else {
                angleWrapper.setTextureDirection(setVariableDialAngleAndReturnDirectionForMinMaxBehaviour());
            }
            hgDialInfo.setGestureAngle(angleWrapper.getTouchAngle());
            hgDialInfo.setTextureDirection(lastTextureDirection);
            if(angleSnap != 0 || snapPoints != null) {
                checkAngleSnap();
            }
            else {
                hgDialInfo.setRotateSnapped(false);
                hgDialInfo.setTextureAngle(angleWrapper.getTextureAngle());
            }
        }
        hgDialInfo.setTouchAngle(angleWrapper.getTouchAngle());
    }

    void checkAngleSnap() {
        final double tempAngleSnap = Math.round(angleWrapper.getTextureAngle() / angleSnap);
        if(snapPoints == null) {
            if(angleWrapper.getTextureAngle() > (tempAngleSnap * angleSnap) - angleSnapProximity && angleWrapper.getTextureAngle() < (tempAngleSnap * angleSnap) + angleSnapProximity) {
                angleSnapNext = tempAngleSnap * angleSnap;
                rotateSnapped = true;
                hgDialInfo.setTextureAngle(angleSnapNext);
            }
            else {
                angleSnapNext = angleWrapper.getTextureAngle();
                rotateSnapped = false;
                hgDialInfo.setTextureAngle(angleWrapper.getTextureAngle());
            }
        }
        else {
            final SnapPoint.IrregularSnapStatus irregularSnapStatus = snapPoint.getSnapPoint(snapPoints, angleWrapper.getTextureAngle());
            rotateSnapped = irregularSnapStatus.snapped;
            angleSnapNext = irregularSnapStatus.angle;
            if(rotateSnapped) {
                hgDialInfo.setTextureAngle(angleSnapNext);
            }
            else {
                hgDialInfo.setTextureAngle(angleWrapper.getTextureAngle());
            }
        }
        hgDialInfo.setRotateSnapped(rotateSnapped);
    }

    private void calculateTextureAngle() {angleWrapper.setTextureAngle(fullGestureAngle * precisionRotation);}

    private int setRotationAngleGestureAndReturnDirection() {
        final int[] returnValue = new int[1];
        currentGestureAngle = (1 - (onDownAngle - angleWrapper.getTouchAngle()));
        final float angleDifference = (float) (storedGestureAngle - currentGestureAngle);
        //Detect direction
        if(!(Math.abs(angleDifference) > 0.75d)) {
            if(angleDifference > 0) {
                returnValue[0] = -1;
                fullGestureAngle -= (angleDifference + 1d) % 1d;
            }
            else if(angleDifference < 0) {
                returnValue[0] = 1;
                fullGestureAngle += -angleDifference % 1d;
            }
            if(precisionRotation < 0) {
                angleWrapper.setTextureDirection(-returnValue[0]);
            }
            else if(precisionRotation > 0) {
                angleWrapper.setTextureDirection(returnValue[0]);
            }
            else {
                angleWrapper.setTextureDirection(0);
            }
            if(returnValue[0] != 0) {
                if(precisionRotation < 0) {
                    lastTextureDirection = -returnValue[0];
                    angleWrapper.setTextureDirection(lastTextureDirection);
                }
                else if(precisionRotation > 0) {
                    lastTextureDirection = returnValue[0];
                    angleWrapper.setTextureDirection(lastTextureDirection);
                }
                else {
                    angleWrapper.setTextureDirection(0);
                }
            }
        }
        angleWrapper.setGestureAngle(fullGestureAngle);
        storedGestureAngle = currentGestureAngle;
        return returnValue[0];
    }

    private int setRotationAngleGestureAndReturnDirectionForMinMaxBehaviour() {
        final int[] returnValue = new int[1];
        currentGestureAngle = (1 - (onDownAngle - angleWrapper.getTouchAngle()));
        final double angleDifference = (storedGestureAngle - currentGestureAngle);
        //Detect direction
        if(!(Math.abs(angleDifference) > 0.75d)) {
            if(angleDifference > 0) {
                returnValue[0] = -1;
                fullGestureAngle -= (angleDifference + 1d) % 1d;
            }
            else if(angleDifference < 0) {
                returnValue[0] = 1;
                fullGestureAngle += -angleDifference % 1d;
            }
            if(precisionRotation < 0) {
                angleWrapper.setTextureDirection(-returnValue[0]);
            }
            else if(precisionRotation > 0) {
                angleWrapper.setTextureDirection(returnValue[0]);
            }
            else {
                angleWrapper.setTextureDirection(0);
            }
            if(returnValue[0] != 0) {
                if(precisionRotation < 0) {
                    lastTextureDirection = -returnValue[0];
                    angleWrapper.setTextureDirection(lastTextureDirection);
                }
                else if(precisionRotation > 0) {
                    lastTextureDirection = returnValue[0];
                    angleWrapper.setTextureDirection(lastTextureDirection);
                }
                else {
                    angleWrapper.setTextureDirection(0);
                }
            }
        }
        if(precisionRotation > 0) {
            if(fullGestureAngle < minimumRotation / precisionRotation) {
                minMaxRotationOutOfBounds = -1;
                hgDialInfo.setMinMaxReached(minMaxRotationOutOfBounds);
                fullGestureAngle = minimumRotation / precisionRotation;
            }
            else if(fullGestureAngle > maximumRotation / precisionRotation) {
                minMaxRotationOutOfBounds = 1;
                hgDialInfo.setMinMaxReached(minMaxRotationOutOfBounds);
                fullGestureAngle = maximumRotation / precisionRotation;
            }
            else {
                hgDialInfo.setMinMaxReached(0);
            }
        }
        else if(precisionRotation < 0) {
            if(-fullGestureAngle < -(minimumRotation / precisionRotation)) {
                minMaxRotationOutOfBounds = -1;
                hgDialInfo.setMinMaxReached(minMaxRotationOutOfBounds);
                fullGestureAngle = (minimumRotation / precisionRotation);
            }
            else if(-fullGestureAngle > -(maximumRotation / precisionRotation)) {
                minMaxRotationOutOfBounds = 1;
                hgDialInfo.setMinMaxReached(minMaxRotationOutOfBounds);
                fullGestureAngle = (maximumRotation / precisionRotation);
            }
            else {
                hgDialInfo.setMinMaxReached(0);
            }
        }
        angleWrapper.setGestureAngle(fullGestureAngle);
        storedGestureAngle = currentGestureAngle;
        return returnValue[0];
    }

    private int setVariableDialAngleAndReturnDirection() {
        final int[] returnValue = new int[1];
        currentGestureAngle = (1 - (onDownAngle - angleWrapper.getTouchAngle()));
        final double angleDifference = (storedGestureAngle - currentGestureAngle) % 1;
        final double variablePrecision;
        if(!useVariableDialCurve) {
            //Variable dial acceleration on a straight line.
            variablePrecision = getVariablePrecision();
        }
        else /* if(useVariableDialCurve == true) */ {
            //Variable dial acceleration on a curve.
            variablePrecision = getVariablePrecisionCurve();
        }
        //Detect direction
        if(!(Math.abs(angleDifference) > 0.75d)) {
            if(angleDifference > 0) {
                returnValue[0] = -1;
                if(minMaxRotationOutOfBounds == 0) {
                    fullGestureAngle -= ((angleDifference * variablePrecision));
                }
            }
            else if(angleDifference < 0) {
                returnValue[0] = 1;
                if(minMaxRotationOutOfBounds == 0) {
                    fullGestureAngle -= (angleDifference * variablePrecision);
                }
            }
            if(returnValue[0] != 0) {
                if(variablePrecision > 0) {
                    lastTextureDirection = returnValue[0];
                    angleWrapper.setTextureDirection(lastTextureDirection);
                }
                else if(variablePrecision < 0) {
                    lastTextureDirection = -returnValue[0];
                    angleWrapper.setTextureDirection(-lastTextureDirection);
                }
                hgDialInfo.setTextureDirection(lastTextureDirection);
            }
        }
        angleWrapper.setTextureAngle(fullGestureAngle);
        storedGestureAngle = currentGestureAngle;
        return returnValue[0];
    }

    private int setVariableDialAngleAndReturnDirectionForMinMaxBehaviour() {
        final int[] returnValue = new int[1];
        currentGestureAngle = (1 - (onDownAngle - angleWrapper.getTouchAngle()));
        final double angleDifference = (storedGestureAngle - currentGestureAngle) % 1;
        final double variablePrecision;
        if(!useVariableDialCurve) {
            //Variable dial acceleration on a straight line.
            variablePrecision = getVariablePrecision();
        }
        else /* if(useVariableDialCurve == true) */ {
            //Variable dial acceleration on a curve.
            variablePrecision = getVariablePrecisionCurve();
        }
        //Detect direction
        if(!(Math.abs(angleDifference) > 0.75d)) {
            if(angleDifference > 0) {
                returnValue[0] = -1;
                fullGestureAngle -= ((angleDifference * variablePrecision));
            }
            else if(angleDifference < 0) {
                returnValue[0] = 1;
                fullGestureAngle += -(angleDifference * variablePrecision);
            }
            if(returnValue[0] != 0) {
                if(variablePrecision > 0) {
                    lastTextureDirection = returnValue[0];
                    angleWrapper.setTextureDirection(lastTextureDirection);
                }
                else if(variablePrecision < 0) {
                    lastTextureDirection = -returnValue[0];
                    angleWrapper.setTextureDirection(-lastTextureDirection);
                }
                hgDialInfo.setTextureDirection(lastTextureDirection);
            }
        }
        if(fullGestureAngle < minimumRotation) {
            minMaxRotationOutOfBounds = -1;
            hgDialInfo.setMinMaxReached(minMaxRotationOutOfBounds);
            fullGestureAngle = minimumRotation;
        }
        else if(fullGestureAngle > maximumRotation) {
            minMaxRotationOutOfBounds = 1;
            hgDialInfo.setMinMaxReached(minMaxRotationOutOfBounds);
            fullGestureAngle = maximumRotation;
        }
        if(minMaxRotationOutOfBounds != 0) {
            if(variablePrecision > 0) {
                if(fullGestureAngle == minimumRotation) {
                    if(returnValue[0] == 1) {minMaxRotationOutOfBounds = 0; hgDialInfo.setMinMaxReached(minMaxRotationOutOfBounds);}
                }
                else if(fullGestureAngle == maximumRotation) {
                    if(returnValue[0] == -1) {minMaxRotationOutOfBounds = 0; hgDialInfo.setMinMaxReached(minMaxRotationOutOfBounds);}
                }
                else {
                    hgDialInfo.setMinMaxReached(0);
                }
            }
            else if(variablePrecision < 0) {
                if(fullGestureAngle == minimumRotation) {
                    if(returnValue[0] == -1) {minMaxRotationOutOfBounds = 0; hgDialInfo.setMinMaxReached(minMaxRotationOutOfBounds);}
                }
                else if(fullGestureAngle == maximumRotation) {
                    if(returnValue[0] == 1) {minMaxRotationOutOfBounds = 0; hgDialInfo.setMinMaxReached(minMaxRotationOutOfBounds);}
                }
                else {
                    hgDialInfo.setMinMaxReached(0);
                }
            }
        }
        angleWrapper.setTextureAngle(fullGestureAngle);
        storedGestureAngle = currentGestureAngle;
        return returnValue[0];
    }

    private double getVariablePrecision() {
        double distanceFromCenter = hgGeometry.getTwoFingerDistance(angleWrapper.getCenterPoint().x, angleWrapper.getCenterPoint().y, angleWrapper.touchPoint.x, angleWrapper.touchPoint.y);
        double[] variablePrecision = new double[1];
        if(distanceFromCenter <= imageRadius) {
            variablePrecision[0] = (variableDialInner - (((distanceFromCenter / imageRadius) * (variableDialInner - variableDialOuter)) + variableDialOuter)) + variableDialOuter;
            hgDialInfo.setVariablePrecision(variablePrecision[0]);
        }
        return variablePrecision[0];
    }

    private double getVariablePrecisionCurve() {
        final double distanceFromCenter = hgGeometry.getTwoFingerDistance(angleWrapper.getCenterPoint().x, angleWrapper.getCenterPoint().y, angleWrapper.touchPoint.x, angleWrapper.touchPoint.y);
        double[] variablePrecision = new double[1];
        if(distanceFromCenter <= imageRadius) {
            final double rangeDiff = (variableDialInner - variableDialOuter) * ((distanceFromCenter - imageRadius) / imageRadius);
            if(!positiveCurve) {
                variablePrecision[0] = -rangeDiff * (1d - (Math.sin((distanceFromCenter / imageRadiusX2) * Math.PI))) + variableDialOuter;
            }
            else /* if(positiveCurve == true) */ {
                variablePrecision[0] = -rangeDiff * (Math.cos((distanceFromCenter / imageRadiusX2) * Math.PI)) + variableDialOuter;
            }
            hgDialInfo.setVariablePrecision(variablePrecision[0]);
        }
        return variablePrecision[0];
    }

    private void setSpinStatus() {
        final long currentTime = System.currentTimeMillis();
        final double twoFingerDistance = hgGeometry.getTwoFingerDistance(flingDownTouchX, flingDownTouchY, angleWrapper.touchPoint.x, angleWrapper.touchPoint.y);
        final boolean spinIsDisabled;
        final boolean angleToleranceMet = Math.abs(fullGestureDownAngle - fullGestureAngle) > flingAngleTolerance;
        final boolean distanceToleranceMet = twoFingerDistance > flingDistanceThreshold;
        if((flingAngleTolerance != 0f && angleToleranceMet) || (flingDistanceThreshold != 0 && flingAngleTolerance == 0f && distanceToleranceMet)) {
            spinIsDisabled = false;
        }
        else {
            spinIsDisabled = true;
        }
        //Top of block used for quick tap
        //Note: Fling only works in single finger mode will need to fix. Not priority.
        //flingDistanceThreshold NOT met
        if(spinIsDisabled) {
            //if quickTapTime condition met
            if(currentTime < gestureDownTime + quickTapTime) {
                hgDialInfo.setQuickTap(true);
                return;
            }
        }
        //Bottom of block used for quick tap
        if(!spinIsDisabled && useSingleFinger) {
            //If flingTimeThreshold met
            if(currentTime < gestureDownTime + flingTimeThreshold) {
                if(angleToleranceMet) {
                    if(flingAnimationHandler == null) {flingAnimationHandler = new FlingAnimationHandler(this, this.hgDialV2);}
                    if(spinStartSpeed == 0d && spinEndSpeed == 0d) {
                        flingAnimationHandler.spinStartSpeed = (float) (Math.abs(spinStartAngle - angleWrapper.getTextureAngle()) / ((double) (currentTime - gestureDownTime) / 1000d));
                    }
                    else if(spinStartSpeed != 0d || spinEndSpeed != 0d) {
                        flingAnimationHandler.spinStartSpeed = spinStartSpeed;
                    }
                    flingAnimationHandler.spinEndSpeed = this.spinEndSpeed;
                    if(slowDown) {
                        if(slowFactor == 0) {
                            flingAnimationHandler.spinDuration = this.spinDuration;
                            this.spinEndTime = flingAnimationHandler.spinEndTime = currentTime + spinDuration;
                        }
                        else {
                            if(spinStartSpeed == 0d && spinEndSpeed == 0d) {
                                flingAnimationHandler.spinDuration = (long) (flingAnimationHandler.spinStartSpeed * slowFactor * 1000L);
                            }
                            else if(spinStartSpeed != 0d || spinEndSpeed != 0d) {
                                flingAnimationHandler.spinDuration = (long) (spinStartSpeed * slowFactor * 1000L);
                            }
                            this.spinEndTime = flingAnimationHandler.spinEndTime = currentTime + flingAnimationHandler.spinDuration;
                        }
                    }
                    if(!slowDown) {
                        this.spinEndTime = flingAnimationHandler.spinEndTime = Long.MAX_VALUE;
                        flingAnimationHandler.spinDuration = this.spinEndTime - System.currentTimeMillis();
                    }
                    if(distanceToleranceMet) {
                        hgDialInfo.setSpinTriggered(true);
                        flingAnimationHandler.spinTriggered = spinTriggered = true;
                        flingAnimationHandler.lastTextureDirection = this.lastTextureDirection;
                        hgDialInfo.setSpinCurrentSpeed((float) flingAnimationHandler.spinStartSpeed);
                    }
                }
            }
            else /* if(currentTime >= gestureDownTime + flingTimeThreshold) */ {
                hgDialInfo.setSpinTriggered(false);
                flingAnimationHandler.spinTriggered = spinTriggered = false;
                flingAnimationHandler.lastTextureDirection = this.lastTextureDirection;
                hgDialInfo.setSpinCurrentSpeed(0f);
            }
        }
    }

    /* Top of block convenience methods */
    void doManualTextureDial(final double manualDial) {doManualGestureDial(manualDial / precisionRotation);}

    void doManualGestureDial(final double manualDial) {
        fullGestureAngle = manualDial;
        angleWrapper.setGestureAngle(manualDial);
        angleWrapper.setTextureAngle(angleWrapper.getTextureAngle());
        hgDialInfo.setGestureAngle(angleWrapper.getGestureAngle());
        hgDialInfo.setTextureAngle(angleWrapper.getTextureAngle());
        setReturnType();
    }

    void triggerSpin(final float spinStartSpeed, final float spinEndSpeed, final long spinDuration, final int lastTextureDirection) {
        this.spinStartSpeedState = this.spinStartSpeed;
        this.spinEndSpeedState = this.spinEndSpeed;
        this.spinDurationState = this.spinDuration;
        this.fullGestureDownAngle = this.fullGestureAngle;
        this.spinStartSpeed = spinStartSpeed;
        this.spinEndSpeed = spinEndSpeed;
        this.spinDuration = spinDuration;
        this.spinEndTime = System.currentTimeMillis() + this.spinDuration;
        this.lastTextureDirection = lastTextureDirection;
        this.spinTriggered = true;
        this.spinTriggeredProgrammatically = true;
        flingAnimationHandler.triggerSpin(spinStartSpeed, spinEndSpeed, spinDuration, lastTextureDirection, this.spinEndTime);
    }

    void performQuickTap() {
        hgDialInfo.setQuickTap(true);
        hgDialV2.post(() -> {
            hgDialInfo.setQuickTap(true);
            hgDialV2.ihgDial.onUp(hgDialInfo);
            hgDialInfo.setQuickTap(false);
        });
    }
    /* Bottom of block convenience methods */

    //Top of block Accessors
    double getPrecisionRotation() {return this.precisionRotation;}
    double getFullGestureAngle() {return this.fullGestureAngle;}
    boolean getCumulativeRotate() {return this.cumulativeRotate;}
    double getVariableDialInner() {return this.variableDialInner;}
    double getVariableDialOuter() {return this.variableDialOuter;}
    boolean getUseVariableDial() {return this.useVariableDial;}
    boolean getPositiveCurve() {return this.positiveCurve;}
    boolean getIsSingleFinger() {return this.useSingleFinger;}
    long getQuickTapTime() {return this.quickTapTime;}
    int getFlingDistanceThreshold() {return this.flingDistanceThreshold;}
    long getFlingTimeThreshold() {return this.flingTimeThreshold;}
    float getSpinStartSpeed() {return this.spinStartSpeed;}
    float getSpinEndSpeed() {return this.spinEndSpeed;}
    long getSpinDuration() {return this.spinDuration;}
    boolean getSlowSpinOnPositiveCurve() {return this.slowSpinOnPositiveCurve;}
    boolean getSpinTriggered() {return this.spinTriggered;}
    int getLastTextureDirection() {return this.lastTextureDirection;}
    float getSlowFactor() {return this.slowFactor;}
    //Bottom of block Accessors

    //Top of block Mutators
    void setPrecisionRotation(final double precisionRotation) {
        this.precisionRotation = precisionRotation;
        doManualTextureDial(angleWrapper.getTextureAngle());
    }

    void setFullGestureAngle(final double fullGestureAngle) {this.fullGestureAngle = fullGestureAngle;}
    void setCumulativeRotate(final boolean cumulativeRotate) {this.cumulativeRotate = cumulativeRotate;}
    void setUseSingleFinger(final boolean useSingleFinger) {this.useSingleFinger = useSingleFinger;}
    void setQuickTapTime(final long quickTapTime) {this.quickTapTime = quickTapTime;}
    void setFlingTolerance(final int flingDistanceThreshold, final long flingTimeThreshold) {this.flingDistanceThreshold = flingDistanceThreshold; this.flingTimeThreshold = flingTimeThreshold;}
    void setSpinAnimation(final float spinStartSpeed, final float spinEndSpeed, final long spinDuration) {this.spinStartSpeed = spinStartSpeed; this.spinEndSpeed = spinEndSpeed; this.spinDuration = spinDuration;}
    void setSpinTriggered(final boolean spinTriggered) {this.spinTriggered = spinTriggered;}
    void setSnapPoints(final List<SnapPoint> snapPoints) {this.snapPoints = snapPoints;}
    //Bottom of block Mutators

    /* Top of block behavioural methods */
    void cancelSpin() {flingAnimationHandler.spinTriggered = spinTriggered = false;}

    void setMinMaxDial(final double minimumRotation, final double maximumRotation, final boolean useMinMaxRotation) {
        this.useMinMaxRotation = useMinMaxRotation;
        if(useMinMaxRotation) {
            if(minimumRotation > maximumRotation) {
                this.minimumRotation = maximumRotation;
                this.maximumRotation = minimumRotation;
            }
            else {
                this.minimumRotation = minimumRotation;
                this.maximumRotation = maximumRotation;
            }
        }
        if(!this.useMinMaxRotation) {minMaxRotationOutOfBounds = 0; hgDialInfo.setMinMaxReached(minMaxRotationOutOfBounds);}
    }

    void setVariableDial(final double variableDialInner, final double variableDialOuter, final Boolean useVariableDial) {
        this.variableDialInner = variableDialInner;
        this.variableDialOuter = variableDialOuter;
        this.useVariableDial = useVariableDial;
        if(!useVariableDial) {
            doManualGestureDial(fullGestureAngle);
        }
        else {
            precisionRotation = 1d;
        }
    }

    void setUseVariableDialCurve(final boolean useVariableDialCurve, final boolean positiveCurve) {this.useVariableDialCurve = useVariableDialCurve; this.positiveCurve = positiveCurve;}

    void setAngleSnap(final double angleSnap, final double angleSnapProximity) {
        if(angleSnap == 0d && angleSnapProximity == 0d) {rotateSnapped = false;}
        hgDialV2.angleSnap = this.angleSnap = angleSnap;
        this.angleSnapProximity = Math.min(angleSnapProximity, angleSnap / 2d);
    }
    /* Bottom of block behavioural methods */

    /* Top of block state management */
    HGState saveStateFlush() {return hgState.saveStateFlush();}

    HGState saveState(final String tag) {
        //Developer usage TAG
        hgState.setTag(tag);
        return saveState();
    }

    HGState saveState() {
        //Behavioural State
        hgState.setPrecisionRotation(precisionRotation);
        hgState.setCumulativeRotate(cumulativeRotate);
        hgState.setVariableDialInner(variableDialInner);
        hgState.setVariableDialOuter(variableDialOuter);
        hgState.setUseVariableDial(useVariableDial);
        hgState.setUseVariableDialCurve(useVariableDialCurve);
        hgState.setPositiveCurve(positiveCurve);
        hgState.setUseSingleFinger(useSingleFinger);
        hgState.setQuickTapTime(quickTapTime);
        hgState.setFlingDistanceThreshold(flingDistanceThreshold);
        hgState.setFlingTimeThreshold(flingTimeThreshold);
        hgState.setMinimumRotation(minimumRotation);
        hgState.setMaximumRotation(maximumRotation);
        hgState.setUseMinMaxRotation(useMinMaxRotation);
        hgState.setAngleSnap(angleSnap);
        hgState.setAngleSnapNext(angleSnapNext);
        hgState.setAngleSnapProximity(angleSnapProximity);
        hgState.setSlowFactor(slowFactor);
        hgState.setSlowDown(slowDown);
        hgState.setFlingAngleTolerance(flingAngleTolerance);

        //UI State
        hgState.setFullGestureAngle(fullGestureAngle);
        hgState.setOnDownAngleTextureCumulative(onDownAngle);
        hgState.setOnUpAngleGesture(onUpAngle);
        hgState.setSpinTriggered(spinTriggered);
        hgState.setSpinStartSpeed(spinStartSpeed);
        hgState.setSpinCurrentSpeed((float) flingAnimationHandler.spinCurrentSpeed);

        if(spinTriggered) {
            hgState.setSpinTimeRemaining(spinEndTime - System.currentTimeMillis());
        }

        hgState.setSpinEndSpeed(spinEndSpeed);
        hgState.setSpinDuration(spinDuration);
        hgState.setSpinEndTime(spinEndTime);
        hgState.setLastTextureDirection(lastTextureDirection);
        hgState.setAngleWrapper(angleWrapper);

        return this.hgState;
    }


    void restoreState(final HGState hgState, final boolean updateUI) {
        //Behavioural State
        precisionRotation = hgState.getPrecisionRotation();
        cumulativeRotate = hgState.getCumulativeRotate();
        variableDialInner = hgState.getVariableDialInner();
        variableDialOuter = hgState.getVariableDialOuter();
        useVariableDial = hgState.getUseVariableDial();
        useVariableDialCurve = hgState.useVariableDialCurve();
        positiveCurve = hgState.getPositiveCurve();
        useSingleFinger = hgState.getUseSingleFinger();
        quickTapTime = hgState.getQuickTapTime();
        flingDistanceThreshold = hgState.getFlingDistanceThreshold();
        flingTimeThreshold = hgState.getFlingTimeThreshold();
        minimumRotation = hgState.getMinimumRotation();
        maximumRotation = hgState.getMaximumRotation();
        useMinMaxRotation = hgState.getUseMinMaxRotation();
        angleSnap = hgState.getAngleSnap();
        angleSnapNext = hgState.getAngleSnapNext();
        angleSnapProximity = hgState.getAngleSnapProximity();
        slowFactor = hgState.getSlowFactor();
        slowDown = hgState.getSlowDown();
        flingAngleTolerance = hgState.getFlingAngleTolerance();

        //UI State
        fullGestureAngle = hgState.getFullGestureAngle();
        onDownAngle = hgState.getOnDownAngleTextureCumulative();
        onUpAngle = hgState.getOnUpAngleGesture();
        spinTriggered = hgState.getSpinTriggered();
        spinStartSpeed = hgState.getSpinStartSpeed();
        spinCurrentSpeed = hgState.getSpinCurrentSpeed();
        spinEndSpeed = hgState.getSpinEndSpeed();
        spinDuration = hgState.getSpinDuration();
        spinEndTime = hgState.getSpinEndTime();
        lastTextureDirection = hgState.getLastTextureDirection();
        angleWrapper.setCenterPoint(hgState.getAngleWrapper().getCenterPoint());
        angleWrapper.setGestureAngle(hgState.getAngleWrapper().getGestureAngle());
        angleWrapper.setTextureAngle(hgState.getAngleWrapper().getTextureAngle());
        angleWrapper.setPrecision(hgState.getAngleWrapper().getPrecision());
        angleWrapper.setVariablePrecision(hgState.getAngleWrapper().getVariablePrecision());
        angleWrapper.setTouchAngle(hgState.getAngleWrapper().getTouchAngle());
        angleWrapper.setTextureDirection(hgState.getAngleWrapper().getTextureDirection());
        if(updateUI) {
            hgDialV2.post(hgDialV2::invalidate);
        }
    }
    /* Bottom of block state management */
}