/*
This class is concerned with containing data required by the DialMetrics object.

License. This project is licensed with the 2-clause BSD license. The BSD 2-Clause License [OSI Approved License] The BSD 2-Clause License
In the original BSD license, both occurrences of the phrase "COPYRIGHT HOLDERS AND CONTRIBUTORS" in the disclaimer read "REGENTS AND CONTRIBUTORS".
Copyright (c) 2017, Warwick Weston Wright All rights reserved.
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package com.WarwickWestonWright.HGDialV2;

import android.graphics.Point;
import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

final class AngleWrapper implements Cloneable, Parcelable {
    private Point centerPoint = new Point(0, 0);
    public Point touchPoint = new Point(0, 0);
    private double gestureAngle;
    private double textureAngle;
    private double precision;
    private double variablePrecision;
    private double touchAngle;
    private int textureDirection;

    public Point rotationCenterPoint = new Point();

    AngleWrapper(Point centerPoint) {
        this.centerPoint.x = centerPoint.x;
        this.centerPoint.y = centerPoint.y;
        this.gestureAngle = 0d;
        this.textureAngle = 0d;
        this.precision = 1d;
        this.variablePrecision = 1d;
        this.touchAngle = 0d;
        this.textureDirection = 0;
    }

    //Accessors
    Point getCenterPoint() {return this.centerPoint;}
    double getGestureAngle() {return this.gestureAngle;}
    public double getTextureAngle() {return this.textureAngle;}
    double getPrecision() {return this.precision;}
    double getVariablePrecision() {return this.variablePrecision;}
    double getTouchAngle() {return this.touchAngle;}
    int getTextureDirection() {return this.textureDirection;}

    //Mutators
    void setCenterPoint(final Point centerPoint) {this.centerPoint.x = centerPoint.x; this.centerPoint.y = centerPoint.y;}
    void setGestureAngle(final double gestureAngle) {this.gestureAngle = gestureAngle;}
    void setTextureAngle(final double textureAngle) {
        this.textureAngle = textureAngle;
    }
    void setPrecision(final double precision) {this.precision = precision;}
    void setVariablePrecision(final double variablePrecision) {this.variablePrecision = variablePrecision;}
    void setTouchAngle(final double touchAngle) {this.touchAngle = touchAngle;}
    void setTextureDirection(final int textureDirection) {this.textureDirection = textureDirection;}

    @NonNull
    public Object clone() throws CloneNotSupportedException {return super.clone();}

    @Override
    public int describeContents() {
        return 0;
    }

    private AngleWrapper(Parcel in) {
        centerPoint = in.readParcelable(Point.class.getClassLoader());
        gestureAngle = in.readDouble();
        textureAngle = in.readDouble();
        precision = in.readDouble();
        variablePrecision = in.readDouble();
        touchAngle = in.readDouble();
        textureDirection = in.readInt();
    }

    public static final Creator<AngleWrapper> CREATOR = new Creator<AngleWrapper>() {
        @Override
        public AngleWrapper createFromParcel(Parcel in) {
            return new AngleWrapper(in);
        }

        @Override
        public AngleWrapper[] newArray(int size) {
            return new AngleWrapper[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(centerPoint, flags);
        dest.writeDouble(gestureAngle);
        dest.writeDouble(textureAngle);
        dest.writeDouble(precision);
        dest.writeDouble(variablePrecision);
        dest.writeDouble(touchAngle);
        dest.writeInt(textureDirection);
    }
}