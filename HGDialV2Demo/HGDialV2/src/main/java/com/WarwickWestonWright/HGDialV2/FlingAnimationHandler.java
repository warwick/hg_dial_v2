/*
This class is concerned with: Updating rotation values on the thread that controls the process of fling-to-spin behaviour.

License. This project is licensed with the 2-clause BSD license. The BSD 2-Clause License [OSI Approved License] The BSD 2-Clause License
In the original BSD license, both occurrences of the phrase "COPYRIGHT HOLDERS AND CONTRIBUTORS" in the disclaimer read "REGENTS AND CONTRIBUTORS".
Copyright (c) 2017, Warwick Weston Wright All rights reserved.
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package com.WarwickWestonWright.HGDialV2;

import android.os.Handler;
import android.os.Message;

import androidx.annotation.NonNull;

import java.lang.ref.WeakReference;

final class FlingAnimationHandler implements Runnable {
    private final FlingHandler dialMetricsHandler = new FlingHandler(this);
    private final DialMetrics dialMetrics;
    private final HGDialV2 hgDialV2;
    private Thread spinAnimationThread;
    public volatile boolean spinTriggered;
    double spinStartSpeed;
    double spinCurrentSpeed;
    double spinEndSpeed;
    long spinDuration;
    int lastTextureDirection;
    long spinEndTime;
    private int minMax;

    public FlingAnimationHandler(final DialMetrics dialMetrics, final HGDialV2 hgDialV2) {this.dialMetrics = dialMetrics; this.hgDialV2 = hgDialV2;}

    void startFlingAnim() {
        if(spinAnimationThread == null) {spinAnimationThread = new Thread(this);}
        spinAnimationThread.setUncaughtExceptionHandler((t, e) -> {
            spinTriggered = false;
            spinAnimationThread = null;
        });

        try {
            spinAnimationThread.start();
        }
        catch(IllegalThreadStateException e) {
            spinTriggered = false;
            spinAnimationThread = null;
        }
    }

    void triggerSpin(final float spinStartSpeed, final float spinEndSpeed, final long spinDuration, final int lastTextureDirection, final long spinEndTime) {
        this.spinStartSpeed = spinStartSpeed;
        this.spinEndSpeed = spinEndSpeed;
        this.spinDuration = spinDuration;
        this.lastTextureDirection = lastTextureDirection;
        this.spinEndTime = spinEndTime;
        this.spinTriggered = true;
        if(spinAnimationThread == null) {spinAnimationThread = new Thread(this);}
        try {
            spinAnimationThread.start();
        }
        catch(IllegalThreadStateException e) {
            spinTriggered = false;
            spinAnimationThread = null;
        }
    }

    @Override
    public void run() {
        if(spinDuration == 0) {
            //Used to signal a fling to the callback without any fling animation. This is for developer convenience.
            dialMetrics.hgDialInfo.setSpinTriggered(true);
        }
        else {
            if(dialMetrics.slowDown) {
                doSpinAnimation();
            }
            else {
                doContinuousSpinAnimation();
            }
        }
        dialMetricsHandler.sendEmptyMessage(0);
    }

    /* Top of block Fling Functions called on thread */
    private void doSpinAnimation() {
        if(dialMetrics.precisionRotation < 0) {lastTextureDirection = -lastTextureDirection;}
        this.minMax = 0;
        long currentTime = System.currentTimeMillis();
        final double speedDifference = spinStartSpeed - spinEndSpeed;
        while(spinTriggered) {
            spinCurrentSpeed = (spinStartSpeed + (speedDifference * ((double) (spinEndTime - currentTime) / spinDuration))) - speedDifference;
            if(currentTime < spinEndTime) {
                //Update view every 10 milliseconds
                if(currentTime + 10L < System.currentTimeMillis()) {
                    currentTime = System.currentTimeMillis();
                    final double lastAngle = dialMetrics.fullGestureAngle;
                    dialMetrics.fullGestureAngle += ((0.01f * spinCurrentSpeed) * lastTextureDirection);
                    int direction = lastAngle > dialMetrics.fullGestureAngle ? -1 : 1;
                    if(dialMetrics.precisionRotation < 0) {direction = -direction;}
                    dialMetrics.angleWrapper.setTextureAngle(dialMetrics.fullGestureAngle * dialMetrics.precisionRotation);
                    if(dialMetrics.angleSnap != 0) {dialMetrics.checkAngleSnap();}
                    dialMetrics.hgDialInfo.setTextureAngle(dialMetrics.angleWrapper.getTextureAngle());
                    dialMetrics.hgDialInfo.setTextureDirection(direction);
                    dialMetrics.hgDialInfo.setSpinCurrentSpeed((float) spinCurrentSpeed);
                    final int minMax;
                    if((minMax = checkMinMaxBounds(dialMetrics.angleWrapper.getTextureAngle())) == 0) {
                        hgDialV2.post(() -> {
                            if(hgDialV2.ihgDial != null) {
                                hgDialV2.ihgDial.onMove(dialMetrics.hgDialInfo);
                                hgDialV2.invalidate();
                            }
                        });
                    }
                    else if(checkMinMaxBounds(dialMetrics.angleWrapper.getTextureAngle()) != 0) {
                        this.minMax = minMax;
                        dialMetrics.hgDialInfo.setMinMaxReached(minMax);
                        spinTriggered = false;
                    }
                }
            }
            else {
                spinTriggered = false;
                dialMetrics.hgDialInfo.setSpinTriggered(false);
            }
        }
    }

    private void doContinuousSpinAnimation() {
        this.minMax = 0;
        long currentTime = System.currentTimeMillis();
        final long startTime = currentTime;
        final double startSpinAngle = dialMetrics.fullGestureAngle;
        dialMetrics.hgDialInfo.setSpinCurrentSpeed((float) spinStartSpeed);
        spinCurrentSpeed = spinStartSpeed;
        if(dialMetrics.precisionRotation < 0) {lastTextureDirection = -lastTextureDirection;}
        while(spinTriggered) {
            //Update view every 10 milliseconds
            if(currentTime + 10L < System.currentTimeMillis()) {
                currentTime = System.currentTimeMillis();
                final double animDuration = ((double) (currentTime - startTime) / 1000d);
                dialMetrics.fullGestureAngle = startSpinAngle + (animDuration * spinStartSpeed * lastTextureDirection);
                dialMetrics.angleWrapper.setTextureAngle(dialMetrics.fullGestureAngle * dialMetrics.precisionRotation);
                if(dialMetrics.angleSnap != 0) {dialMetrics.checkAngleSnap();}
                dialMetrics.hgDialInfo.setTextureAngle(dialMetrics.angleWrapper.getTextureAngle());
                final int minMax;
                if((minMax = checkMinMaxBounds(dialMetrics.angleWrapper.getTextureAngle())) == 0) {
                    hgDialV2.post(() -> {
                        if(hgDialV2.ihgDial != null) {
                            hgDialV2.ihgDial.onMove(dialMetrics.hgDialInfo);
                            hgDialV2.invalidate();
                        }
                    });
                }
                else if(checkMinMaxBounds(dialMetrics.angleWrapper.getTextureAngle()) != 0) {
                    this.minMax = minMax;
                    dialMetrics.hgDialInfo.setMinMaxReached(minMax);
                    spinTriggered = false;
                }
            }
        }
    }
    /* Bottom of block Fling Functions called on thread */

    private int checkMinMaxBounds(final double textureAngle) {
        if(!dialMetrics.useMinMaxRotation) {
            return 0;
        }
        else {
            if(textureAngle > dialMetrics.maximumRotation) {
                return 1;
            }
            else if(textureAngle < dialMetrics.minimumRotation) {
                return -1;
            }
        }
        return 0;
    }

    private static class FlingHandler extends Handler {
        private final WeakReference<FlingAnimationHandler> hgDialV2WeakReference;
        FlingHandler(FlingAnimationHandler flingAnimationHandler) {
            hgDialV2WeakReference = new WeakReference<>(flingAnimationHandler);
        }

        @Override
        public void handleMessage(@NonNull Message msg) {
            if(hgDialV2WeakReference != null) {
                final FlingAnimationHandler animationHandler = hgDialV2WeakReference.get();
                if(animationHandler.dialMetrics.spinTriggeredProgrammatically) {
                    animationHandler.dialMetrics.spinTriggeredProgrammatically = false;
                    animationHandler.dialMetrics.spinStartSpeed = animationHandler.dialMetrics.spinStartSpeedState;
                    animationHandler.dialMetrics.spinEndSpeed = animationHandler.dialMetrics.spinEndSpeedState;
                    animationHandler.dialMetrics.spinDuration = animationHandler.dialMetrics.spinDurationState;
                }
                if(!animationHandler.spinTriggered) {
                    animationHandler.dialMetrics.hgDialInfo.setSpinTriggered(false);
                    animationHandler.dialMetrics.hgDialInfo.setGestureDirection(0);
                    animationHandler.dialMetrics.hgDialInfo.setTextureDirection(0);
                    animationHandler.dialMetrics.hgDialInfo.setSpinCurrentSpeed(0f);
                    if(animationHandler.minMax != 0) {
                        if(animationHandler.minMax == 1) {
                            animationHandler.dialMetrics.doManualTextureDial(animationHandler.dialMetrics.maximumRotation);
                            animationHandler.dialMetrics.hgDialInfo.setMinMaxReached(animationHandler.minMax);
                            animationHandler.dialMetrics.hgDialInfo.setTextureAngle(animationHandler.dialMetrics.maximumRotation);
                        }
                        else if(animationHandler.minMax == -1) {
                            animationHandler.dialMetrics.doManualTextureDial(animationHandler.dialMetrics.minimumRotation);
                            animationHandler.dialMetrics.hgDialInfo.setMinMaxReached(animationHandler.minMax);
                            animationHandler.dialMetrics.hgDialInfo.setTextureAngle(animationHandler.dialMetrics.minimumRotation);
                        }
                    }
                    else if(!animationHandler.dialMetrics.useVariableDial) {
                        animationHandler.dialMetrics.doManualTextureDial(animationHandler.dialMetrics.angleWrapper.getTextureAngle());
                        animationHandler.dialMetrics.hgDialInfo.setMinMaxReached(animationHandler.minMax);
                    }
                    if(animationHandler.hgDialV2.ihgDial != null) {
                        animationHandler.hgDialV2.ihgDial.onUp(animationHandler.dialMetrics.hgDialInfo);
                        animationHandler.hgDialV2.invalidate();
                    }
                }
                animationHandler.spinAnimationThread = null;
            }
        }
    }
}