/*
This class is concerned with: Saving/Restoring and flushing the state of the HGDialV2 object

License. This project is licensed with the 2-clause BSD license. The BSD 2-Clause License [OSI Approved License] The BSD 2-Clause License
In the original BSD license, both occurrences of the phrase "COPYRIGHT HOLDERS AND CONTRIBUTORS" in the disclaimer read "REGENTS AND CONTRIBUTORS".
Copyright (c) 2017, Warwick Weston Wright All rights reserved.
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package com.WarwickWestonWright.HGDialV2;
import android.os.Parcel;
import android.os.Parcelable;

public class HGState implements Cloneable, Parcelable {
    //Developer usage TAG
    private String tag;

    //Behavioural State
    private double precisionRotation;
    private boolean cumulativeRotate;
    private double variableDialInner;
    private double variableDialOuter;
    private boolean useVariableDial;
    private boolean useVariableDialCurve;
    private boolean positiveCurve;
    private boolean isSingleFinger;
    private long quickTapTime;
    private int flingDistanceThreshold;
    private long flingTimeThreshold;
    private double minimumRotation;
    private double maximumRotation;
    private boolean useMinMaxRotation;
    private double angleSnap;
    private double angleSnapNext;
    private double angleSnapProximity;
    private float slowFactor;
    private boolean slowDown;
    private double flingAngleTolerance;

    //UI State
    private double fullGestureAngle;
    private double onDownAngleTextureCumulative;
    private double onUpAngleGesture;
    private boolean spinTriggered;
    private float spinStartSpeed;
    private float spinCurrentSpeed;
    private float spinEndSpeed;
    private long spinDuration;
    private long spinEndTime;
    private long spinTimeRemaining;
    private int lastObjectDirection;

    private AngleWrapper angleWrapper;

    HGState() {}

    private HGState(Parcel in) {

        //Developer usage TAG
        this.tag = in.readString();

        //Behavioural State
        this.precisionRotation = in.readDouble();
        this.cumulativeRotate = in.readByte() != 0;
        this.variableDialInner = in.readDouble();
        this.variableDialOuter = in.readDouble();
        this.useVariableDial = in.readByte() != 0;
        this.useVariableDialCurve = in.readByte() != 0;
        this.positiveCurve = in.readByte() != 0;
        this.isSingleFinger = in.readByte() != 0;
        this.quickTapTime = in.readLong();
        this.flingDistanceThreshold = in.readInt();
        this.flingTimeThreshold = in.readLong();
        this.minimumRotation = in.readDouble();
        this.maximumRotation = in.readDouble();
        this.useMinMaxRotation = in.readByte() != 0;
        this.angleSnap = in.readDouble();
        this.angleSnapNext = in.readDouble();
        this.angleSnapProximity = in.readDouble();
        this.slowFactor = in.readFloat();
        this.slowDown = in.readByte() != 0;
        this.flingAngleTolerance = in.readFloat();

        //UI State
        this.fullGestureAngle = in.readDouble();
        this.onDownAngleTextureCumulative = in.readDouble();
        this.onUpAngleGesture = in.readDouble();
        this.spinTriggered = in.readByte() != 0;
        this.spinStartSpeed = in.readFloat();
        this.spinCurrentSpeed = in.readFloat();
        this.spinEndSpeed = in.readFloat();
        this.spinDuration = in.readLong();
        this.spinEndTime = in.readLong();
        this.spinTimeRemaining = in.readLong();
        this.lastObjectDirection = in.readInt();
    }

    HGState saveStateFlush() {

        //Developer usage TAG
        setTag("");

        //Behavioural State
        setPrecisionRotation(1d);
        setCumulativeRotate(true);
        setVariableDialInner(0d);
        setVariableDialOuter(0d);
        setUseVariableDial(false);
        setUseVariableDialCurve(false);
        setPositiveCurve(false);
        setUseSingleFinger(true);
        setQuickTapTime(0L);
        setFlingDistanceThreshold(0);
        setFlingTimeThreshold(0L);
        setMinimumRotation(0d);
        setMaximumRotation(0d);
        setUseMinMaxRotation(false);
        setAngleSnap(0d);
        setAngleSnapNext(0d);
        setAngleSnapProximity(0d);
        setSlowFactor(0f);
        setSlowDown(true);
        setFlingAngleTolerance(0f);

        //UI State
        setFullGestureAngle(0d);
        setOnDownAngleTextureCumulative(0d);
        setOnUpAngleGesture(0d);
        setSpinTriggered(false);
        setSpinStartSpeed(0f);
        setSpinCurrentSpeed(0f);
        setSpinEndSpeed(0f);
        setSpinDuration(0L);
        setSpinEndTime(0L);
        setSpinTimeRemaining(0L);
        setLastTextureDirection(0);
        return this;
    }

    //Mutators
    //Developer usage TAG
    public void setTag(final String tag) {this.tag = tag;}

    //Behavioural State
    void setPrecisionRotation(final double precisionRotation) {this.precisionRotation = precisionRotation;}
    void setCumulativeRotate(final boolean cumulativeRotate) {this.cumulativeRotate = cumulativeRotate;}
    void setVariableDialInner(final double variableDialInner) {this.variableDialInner = variableDialInner;}
    void setVariableDialOuter(final double variableDialOuter) {this.variableDialOuter = variableDialOuter;}
    void setUseVariableDial(final boolean useVariableDial) {this.useVariableDial = useVariableDial;}
    void setUseVariableDialCurve(final boolean useVariableDialCurve) {this.useVariableDialCurve = useVariableDialCurve;}
    void setPositiveCurve(final boolean positiveCurve) {this.positiveCurve = positiveCurve;}
    void setUseSingleFinger(final boolean isSingleFinger) {this.isSingleFinger = isSingleFinger;}
    void setQuickTapTime(final long quickTapTime) {this.quickTapTime = quickTapTime;}
    void setFlingDistanceThreshold(final int flingDistanceThreshold) {this.flingDistanceThreshold = flingDistanceThreshold;}
    void setFlingTimeThreshold(final long flingTimeThreshold) {this.flingTimeThreshold = flingTimeThreshold;}
    void setMinimumRotation(final double minimumRotation) {this.minimumRotation = minimumRotation;}
    void setMaximumRotation(final double maximumRotation) {this.maximumRotation = maximumRotation;}
    void setUseMinMaxRotation(final boolean useMinMaxRotation) {this.useMinMaxRotation = useMinMaxRotation;}
    void setAngleSnap(final double angleSnap) {this.angleSnap = angleSnap;}
    void setAngleSnapNext(final double angleSnapNext) {this.angleSnapNext = angleSnapNext;}
    void setAngleSnapProximity(final double angleSnapProximity) {this.angleSnapProximity = angleSnapProximity;}
    void setSlowFactor(final float slowFactor) {this.slowFactor = slowFactor;}
    void setSlowDown(final boolean slowDown) {this.slowDown = slowDown;}
    void setFlingAngleTolerance(final double flingAngleTolerance) {this.flingAngleTolerance = flingAngleTolerance;}


    //UI State
    void setFullGestureAngle(final double fullGestureAngle) {this.fullGestureAngle = fullGestureAngle;}
    void setOnDownAngleTextureCumulative(final double onDownAngleTextureCumulative) {this.onDownAngleTextureCumulative = onDownAngleTextureCumulative;}
    void setOnUpAngleGesture(final double onUpAngleGesture) {this.onUpAngleGesture = onUpAngleGesture;}
    void setSpinTriggered(final boolean spinTriggered) {this.spinTriggered = spinTriggered;}
    void setSpinStartSpeed(final float spinStartSpeed) {this.spinStartSpeed = spinStartSpeed;}
    void setSpinCurrentSpeed(final float spinCurrentSpeed) {this.spinCurrentSpeed = spinCurrentSpeed;}
    void setSpinEndSpeed(final float spinEndSpeed) {this.spinEndSpeed = spinEndSpeed;}
    void setSpinDuration(final long spinDuration) {this.spinDuration = spinDuration;}
    void setSpinEndTime(final long spinEndTime) {this.spinEndTime = spinEndTime;}
    void setSpinTimeRemaining(final long spinTimeRemaining) {this.spinTimeRemaining = spinTimeRemaining;}
    void setLastTextureDirection(final int lastObjectDirection) {this.lastObjectDirection = lastObjectDirection;}

    void setAngleWrapper(final AngleWrapper angleWrapper) {
        try {
            this.angleWrapper = (AngleWrapper) angleWrapper.clone();
        }
        catch(CloneNotSupportedException c){}
    }

    //Accessors
    //Developer usage TAG
    public String getTag() {return this.tag;}

    //Behavioural State
    public double getPrecisionRotation() {return this.precisionRotation;}
    public boolean getCumulativeRotate() {return this.cumulativeRotate;}
    public double getVariableDialInner() {return this.variableDialInner;}
    public double getVariableDialOuter() {return this.variableDialOuter;}
    public boolean getUseVariableDial() {return this.useVariableDial;}
    public boolean useVariableDialCurve() {return this.useVariableDialCurve;}
    public boolean getPositiveCurve() {return this.positiveCurve;}
    public boolean getUseSingleFinger() {return this.isSingleFinger;}
    public long getQuickTapTime() {return this.quickTapTime;}
    public int getFlingDistanceThreshold() {return this.flingDistanceThreshold;}
    public long getFlingTimeThreshold() {return this.flingTimeThreshold;}
    public double getMinimumRotation() {return this.minimumRotation;}
    public double getMaximumRotation() {return this.maximumRotation;}
    public boolean getUseMinMaxRotation() {return this.useMinMaxRotation;}
    public double getAngleSnap() {return this.angleSnap;}
    public double getAngleSnapNext() {return this.angleSnapNext;}
    public double getAngleSnapProximity() {return this.angleSnapProximity;}
    public float getSlowFactor() {return this.slowFactor;}
    public boolean getSlowDown() {return this.slowDown;}
    public double getFlingAngleTolerance() {return this.flingAngleTolerance;}

    //UI State
    public double getFullGestureAngle() {return this.fullGestureAngle;}
    public double getOnDownAngleTextureCumulative() {return this.onDownAngleTextureCumulative;}
    public double getOnUpAngleGesture() {return this.onUpAngleGesture;}
    public boolean getSpinTriggered() {return this.spinTriggered;}
    public float getSpinStartSpeed() {return this.spinStartSpeed;}
    public float getSpinCurrentSpeed() {return this.spinCurrentSpeed;}
    public float getSpinEndSpeed() {return this.spinEndSpeed;}
    public long getSpinDuration() {return this.spinDuration;}
    public long getSpinEndTime() {return this.spinEndTime;}
    public long getSpinTimeRemaining() {return this.spinTimeRemaining;}
    public int getLastTextureDirection() {return this.lastObjectDirection;}
    AngleWrapper getAngleWrapper() {return this.angleWrapper;}

    public Object clone()throws CloneNotSupportedException {return super.clone();}

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<HGState> CREATOR = new Creator<HGState>() {
        @Override
        public HGState createFromParcel(Parcel in) {
            return new HGState(in);
        }

        @Override
        public HGState[] newArray(int size) {
            return new HGState[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(tag);
        dest.writeDouble(precisionRotation);
        dest.writeByte((byte) (cumulativeRotate ? 1 : 0));
        dest.writeDouble(variableDialInner);
        dest.writeDouble(variableDialOuter);
        dest.writeByte((byte) (useVariableDial ? 1 : 0));
        dest.writeByte((byte) (useVariableDialCurve ? 1 : 0));
        dest.writeByte((byte) (positiveCurve ? 1 : 0));
        dest.writeByte((byte) (isSingleFinger ? 1 : 0));
        dest.writeLong(quickTapTime);
        dest.writeInt(flingDistanceThreshold);
        dest.writeLong(flingTimeThreshold);
        dest.writeDouble(minimumRotation);
        dest.writeDouble(maximumRotation);
        dest.writeByte((byte) (useMinMaxRotation ? 1 : 0));
        dest.writeDouble(angleSnap);
        dest.writeDouble(angleSnapNext);
        dest.writeDouble(angleSnapProximity);
        dest.writeFloat(slowFactor);
        dest.writeByte((byte) (slowDown ? 1 : 0));
        dest.writeDouble(flingAngleTolerance);
        dest.writeDouble(fullGestureAngle);
        dest.writeDouble(onDownAngleTextureCumulative);
        dest.writeDouble(onUpAngleGesture);
        dest.writeByte((byte) (spinTriggered ? 1 : 0));
        dest.writeFloat(spinStartSpeed);
        dest.writeFloat(spinCurrentSpeed);
        dest.writeFloat(spinEndSpeed);
        dest.writeLong(spinDuration);
        dest.writeLong(spinEndTime);
        dest.writeLong(spinTimeRemaining);
        dest.writeInt(lastObjectDirection);
        dest.writeParcelable(angleWrapper, flags);
    }
}